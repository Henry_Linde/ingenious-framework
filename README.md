# Ingenious Game-Playing Framework.

Still under early development - consider it pre-alpha.

All work in here is currently still under copyright and has not been released under any licence - if you want to use any portion of it, please contact the repository owner.

TODO: Improve Readme!!

## Ingenious Framework

The backend of the ingenious framework is basically aiming for three things:

* Adding new games is supposed to be as easy as possible
* Adding new engies to an existing game is supposed to be as easy as possible
* The framework is supposed to be as general as possible, to allow for all different types of games to be added.
 
The framework is built in a server-client manner. This means to play a game, a **GameServer** must be running first, then a lobby with a specific game type must be opened and finally players need to join. A **GameServer** can handle an unlimited amount of different games at the same time. 

One client corresponds to one player running one engine that is specific to a certain game. It connects to the **GameServer**, requests a list of open lobbies, joins one of them and then plays the game.

## Steps to start

1. `git clone https://bitbucket.org/skroon/ingenious-framework.git`
2. `cd ingenious-framework/IngeniousFrame`
3. `gradle eclipse`
4. In Eclipse: Import->Git Project->Import exisiting git project

Now you are able to edit the source in Eclipse. The entry point to the framework is: `core.commandline.main.Driver`. This main method supports starting a game server, creating lobbies on the main server and creating clients to join the lobbies and play the games. Usage examples for this entry point can be seen in the scripts in `IngeniousFrame/scripts`. Please refer to the README in `IngeniousFrame/scripts` to start a game of bomberman.

## Further information

To see how to implement a new game, take a look at the games.tictactoe package. It contains lots of information and comments on how to add a new game.

## Framework structure

The GameServer creates Lobbies, the Lobbies create Referees once enough players have joined, the Referees play games with Engines(=Players).

Next is a writedown of how different classes of the framework (written **bold**) interact with each other.

### On the GameServer's side

The game server can be started by running `core.commandline.main.Driver` with the `server` argument.

**GameServer** constantly waits for connections from Clients on the standard TCP port and creates **ClientHandler** threads for each client that connects. **GameServer** holds a **LobbyManager** instance that is passed to each new **ClientHandler**.

**ClientHandler** first waits for the client to send a unique name (uses **LobbyManager** to check if another player of the same name has connected to the server before). Then waits for one of the possible messages:

* **NewMatchMessage** (contains a **MatchSetting** object which embodies a JSON file containing the specific game settings; on request, a **LobbyHost** object is created with the match settings and added to he **LobbyManager**)

* **LobbyOverviewRequest** (on request, sends the client a list of open lobbies with number of participating players etc.)

* **JoinMessage** (contains the name of the lobby to be joined; on request, the corresponding **LobbyHost** to that name is looked up in the **LobbyManager** and the player added to that lobby).

**LobbyManager** stores HashMaps of **LobbyHost** and **ClientHandler** objects indexed by Strings.

**LobbyHost** accepts joining players until the number of players is reached, then starts the game. Whenever a player joins, a corresponding **ServerToEngineConnection** is created and a handshake with the recently connected player is executed. When the game starts, the lobby is unregistered from the **LobbyManager** and a **Referee** for the game is created by the **RefereeFactory** and started.

**ServerToEngineConnection** implements the **PlayerRepresentation** interface and extends **SocketWrapper**. The socket is initialised with the connection to the player that joined the lobby. 

**PlayerRepresentation** is an interface modelling the view that a **Referee** has on the players of its game. Thus, its methods are: telling the player to init or terminate the game (initGame/terminateGame), telling the player that some move was played by another player (playMove), request player to supply an action it wants to play (genAction). When **PlayerRepresentation** is implemented by **ServerToEngineConnection**, these methods simply take the supplied InitGameMessage/PlayActionMessage/... objects and send them to the connected players via the socket underlying **ServerToEngineConnection**.

**Referee** is created by the **RefereeFactory** with the **MatchSetting** of the game to be played and an array of **PlayerRepresentation** objects. It is then started in a new thread. **Referee** is abstract and is extended e.g. by **PerfectInformationAlternatingPlayReferee** which in turn is extended by e.g. **TTTReferee**.

**PerfectInformationAlternatingPlayReferee** for example is a referee, that

1. Sends InitGameMessages to all players
2. Until the game board is terminal
    1. Asks each player in turn to supply an action
    2. Checks whether the action is valid, then distributes it to the other players
3. Sends GameTerminatedMessages to all players

### On the Client side

The server can be prompted to create a lobby for some game by running `core.commandline.main.Driver` with the `create` argument (you also have to specify game name etc... refer to the help-Message you will get when running `create` without further arguments).

Once a lobby is created, players for the game can be created by running `core.commandline.main.Driver` with the `client` argument (again, refer to the help message given and the scripts in `IngeniousFrame/scripts`).

**LobbyHandler** can ask a GameServer to create a lobby and then joins that lobby, or simply joins an existing lobby. Either way, an **EngineToServerConnection** is returned.

**Engine** gets an **EngineToServerConnection** on construction. It is also directly responsible for handling the PlayedMove and InitGameMessages sent by the **Referee** on the server side.

**EngineToServerConnection** is responsible for handling the GenActionMessages and GameTerminatedMessages sent by the **Referee**. For GenActionMessages, it implements a buffer to give the **Engine** time before it has to supply its move.

## Adding a new game

Check `games.tictactoe` for an example game. Also check scripts/tictactoe for example commands to start a GameServer, create a lobby and start a game.

Currently, when adding a new game/engine, the following steps have to be taken:

1. Add a package and sources in za.ac.sun.cs.ingenious.games . You need at least a Referee and an Engine to start a game.
2. Compile with `gradle jarFat`
3. Start a game server `java -jar build/libs/IngeniousFrame-all-0.0.2.jar server`
4. Create a YOURGAME.json file containing settings for your game. You will at least need to supply the number of players needed to play the game, e.g.:
`{
	"numPlayers": 2
}`
5. Create a lobby for your game on the game server with `java -jar build/libs/IngeniousFrame-all-0.0.2.jar create -config "YOURGAME.json" -game "YOURREFEREENAME" -lobby "mylobby" -players 2`
6. Cretae players to join your lobby by running `java -jar build/libs/IngeniousFrame-all-0.0.2.jar client -username "PLAYERNAME" -engine "za.ac.sun.cs.ingenious.games.YOURPACKAGE.YOURENGINECLASSNAME" -game "YOURREFEREENAME" -hostname localhost -port 61234` for each player. Note that player names must be unique.
7. The game will start once enough players have joined.


## Notes on development workflow

1. No non-trivial work unless there's an issue (trivial work includes correcting spelling in documentation, adding comments, etc., but excludes any changes to actual code).
2. Assign yourself to an issue if you are working on it.
3. Fix / implement the issue/enhancement on a SEPARATE branch, typically branched from develop.  For managing branches, see [this link](http://nvie.com/posts/a-successful-git-branching-model/); you may also find these [git flow extensions](https://github.com/petervanderdoes/gitflow-avh) useful.  Name the branch accordingly, including the issue number in the name -- use underscores ex. fix_nullpointer_10.
4. Submit a pull request to merge the fix branch into develop or master, as appropriate - feel free to use pull requests on feature branches, especially when helping out on a feature. In the pull request commit message, add something like closes #100 or fixes #100, this will close the actual issue (https://confluence.atlassian.com/bitbucket/resolve-issues-automatically-when-users-push-code-221451126.html).  Note the use of the --no-ff flag for the merge.
