package za.ac.sun.cs.ingenious.games.bomberman.gamestate.moves;

import za.ac.sun.cs.ingenious.core.model.Action;

public class PlaceBombAction implements Action {

	private static final long serialVersionUID = 1L;
	
	private int playerID;
	
	public PlaceBombAction(int playerID) {
		this.playerID = playerID;
	}

	public int getPlayerID() {
		return playerID;
	}
	
	public String toString(){
		return "bomb";
	}
}
