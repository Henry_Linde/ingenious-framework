package za.ac.sun.cs.ingenious.games.ingenious;

/**
 * 
 * Collection of constants available in project.
 * 
 * @author steven
 */
public class Constants {
	public static final int RED =0;
	public static final int GREEN =1;
	public static final int BLUE =2;
	public static final int ORANGE =3;
	public static final int YELLOW =4;
	public static final int PURPLE =5;
	public static final int NUM_COLOURS=6;
	public static final int DEFAULT_NUM_TILES=120;
	public static final String[] MessageTypes = {"","=","?"};
	public static final String[] FacilitatorPlayers = {"Open Slot","Random Engine","Minimax Engine","EMM Engine","MCTS Engine","LOA Engine","Carcasonne Engine"};
	
	public static final String[] Colours = {"Red","Blue","Green","Yellow","Purple","Orange","Black"};
}
