package za.ac.sun.cs.ingenious.core.network.lobby;

import za.ac.sun.cs.ingenious.core.network.Message;

/**
 * String message. Basically just a wrapper for a string. Used for backward compatibility.
 * 
 * TODO This should ultimately allow using legacy protocols with the framework, however
 * this functionality is not yet available, see issue #85
 * 
 * @author Stephan Tietz
 */
public class StringMessage extends Message {

	private static final long serialVersionUID = 1L;

	private String s;

	public StringMessage(String s) {
		this.s = s;
	}

	public String getString() {
		return s;
	}

	public String[] asArray() {
		String message = s;
		String messageValues[] = message.split(" ");
		String messageType = messageValues[0];
		String commandParameters[] = new String[1];

		if (messageValues.length == 2) {
			commandParameters = messageValues[1].split(",");
		}
		String returnString[] = new String[commandParameters.length + 1];
		returnString[0] = messageType;
		int count = 1;
		for (String s : commandParameters) {
			returnString[count++] = s;
		}
		for (int i = 0; i < returnString.length; i++) {
			// Log.info("MESSAGE RECEIVED : "+returnString[i]);
		}
		return returnString;
	}

	public String[] splitMessage(String message) {

		String messageValues[] = message.split(" ");
		String messageType = messageValues[0];
		String commandParameters[] = new String[1];

		if (messageValues.length == 2) {
			commandParameters = messageValues[1].split(",");
		}
		String returnString[] = new String[commandParameters.length + 1];
		returnString[0] = messageType;
		int count = 1;
		for (String s : commandParameters) {
			returnString[count++] = s;
		}
		return returnString;
	}

	public static StringMessage reply(String... params) {
		return command("=", params);
	}

	public static StringMessage replyBrute(String reply) {
		return new StringMessage(reply);
	}

	public static StringMessage errorReply(String... params) {
		return command("?", params);
	}
	
	public static StringMessage command(String command, String... params) {
		String message = command+" ";
		
		for(int i =0; i< params.length-1;i++){
			message= message+params[i]+",";
		}
		if(params.length>0){
			message = message + params[params.length-1];
		}
		StringMessage a = new StringMessage(message);
		return a;
		//Log.info("COMMAND : " + command + "MESSAGE : " + message);
				
	}
}
