package za.ac.sun.cs.ingenious.games.domineering.game;

import java.util.ArrayList;
import java.util.List;

import za.ac.sun.cs.ingenious.core.model.GameLogic;
import za.ac.sun.cs.ingenious.core.model.Move;
import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.core.model.TurnBasedGameLogic;
import za.ac.sun.cs.ingenious.core.model.TurnBasedSquareBoard;
import za.ac.sun.cs.ingenious.games.mnk.MNKBoard;

/**
 * Class for the Domineering Board including game logic inherited from BoardController
 * @author Michael Krause
 *
 */
public class DomineeringLogic implements TurnBasedGameLogic<TurnBasedSquareBoard> {

	public boolean isFull(TurnBasedSquareBoard state) {
		for(byte i = 0; i < state.board.length; i++){
			if(state.board[i]==0) return false;
		}
		return true;
	}

	@Override
	public boolean validMove(TurnBasedSquareBoard fromState, Move m) {
		DomineeringAction move = (DomineeringAction) m;
		return  move.getX1()<fromState.getBoardSize() && move.getY1()<fromState.getBoardSize() &&
				fromState.board[fromState.xyToIndex(move.getX1(), move.getY1())] == 0 &&
				move.getX2()<fromState.getBoardSize() && move.getY2()<fromState.getBoardSize() &&
				fromState.board[fromState.xyToIndex(move.getX2(), move.getY2())] == 0;
	}

	@Override
	public boolean makeMove(TurnBasedSquareBoard fromState, Move m) {
		DomineeringAction move = (DomineeringAction) m;
		fromState.board[fromState.xyToIndex(move.getX1(), move.getY1())] = (byte)(move.getPlayer()+1);
		fromState.board[fromState.xyToIndex(move.getX2(), move.getY2())] = (byte)(move.getPlayer()+1);
		nextTurn(fromState, move);
		return true;
	}

	@Override
	public void undoMove(TurnBasedSquareBoard fromState, Move move) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<Action> generateActions(TurnBasedSquareBoard fromState, int forPlayerID) {
		ArrayList<Action> actions = new ArrayList<>();
		for(byte x = 0; x < fromState.getBoardSize(); x++){
			for(byte y = 0; y < fromState.getBoardSize(); y++){
				if(fromState.board[fromState.xyToIndex(x, y)]==0) {
					DomineeringAction m = new DomineeringAction(x, y, forPlayerID);
					if (validMove(fromState, m))
						actions.add(m);
				}
			}
		}
		return actions;
	}

	@Override
	public boolean isTerminal(TurnBasedSquareBoard state) {
		// Important: We need to know who the next Player to play is, so we must make sure
		// that nextMovePlayerID is correctly set before isTerminal gets called
		return generateActions(state, state.nextMovePlayerID).isEmpty();
	}

}
