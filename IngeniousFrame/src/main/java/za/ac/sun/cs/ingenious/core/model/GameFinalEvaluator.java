package za.ac.sun.cs.ingenious.core.model;

/**
 * Used for returning the score for each player once a game has ended. Every
 * game added to the framework should have a class implementing this.
 * 
 * @param <S> The type of GameState that this evaluator an evaluate.
 */
public interface GameFinalEvaluator<S extends GameState> {

	/**
     * Note that the behaviour of this method is not defined if the provided
     * state is not terminal.
     *
     * @param forState The terminal state for which the score is required.
	 * @return An array a where a[i] is player i's score in the provided terminal state.
	 */
	public double[] getScore(S forState);
	
}
