package za.ac.sun.cs.ingenious.search.ismcts;

import za.ac.sun.cs.ingenious.core.model.GameLogic;
import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.core.model.GameState;

public class SOISMCTSNode<S extends GameState> extends ISMCTSNode<S,SOISMCTSNode<S>> {

	public SOISMCTSNode(S currentState, S determinizedState, GameLogic<S> logic, Action toThisMove,
			SOISMCTSNode<S> parent) {
		super(currentState, determinizedState, logic, toThisMove, parent);
	}

	@Override
	public SOISMCTSNode<S> expandAChild(Action action) {
		SOISMCTSNode<S> newNode = new SOISMCTSNode<S>(currentState, determinizedState, logic, action, this);
		expandedChildren.add(newNode);
		unExpandedMoves.remove(action);
		return newNode;
	}

}
