package za.ac.sun.cs.ingenious.games.bomberman;

import com.esotericsoftware.minlog.Log;
import com.google.common.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import za.ac.sun.cs.ingenious.core.GameSystem;
import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.Referee;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.BMBoard;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.BMFinalEvaluator;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.BMLogic;
import za.ac.sun.cs.ingenious.games.bomberman.network.BMGameTerminateMessage;
import za.ac.sun.cs.ingenious.games.bomberman.network.BMGenActionMessage;
import za.ac.sun.cs.ingenious.games.bomberman.network.BMInitGameMessage;
import za.ac.sun.cs.ingenious.games.bomberman.network.NewUpgradesMessage;
import za.ac.sun.cs.ingenious.games.bomberman.network.NextRoundMessage;
import za.ac.sun.cs.ingenious.games.bomberman.network.UpdateBombsMessage;
import za.ac.sun.cs.ingenious.games.bomberman.ui.BombermanFrame;

/**
 * Referee for Bomberman - controls the game logic, and sends updates and move requests to the engines
 * playing in the match.  For an understanding of how the game loop and play proceeds, see the run
 * method, as well as the playRound/playAlternatingRound methods.
*/
public class BMReferee extends Referee {

    private BMBoard board;
	protected BMLogic logic = BMLogic.defaultBMLogic;
	protected BMFinalEvaluator eval = new BMFinalEvaluator();
    private boolean alternating;

    /** Time per move in milliseconds; -1 indicates no limit */
    private static final int TIME_PER_ROUND = 2000;
    /** Time in milliseconds after all players connected before game starts */
    private static final int GAME_START_DELAY = 2000;
    /** Time paused between rounds - useful for observing games */
    private static final int BETWEEN_ROUND_DELAY = 1000;
    /** When requesting moves in parallel, the time between successive checks to see if all players have submitted moves. */
    private static final int CHECK_MOVE_INTERVAL = 1000;

    // TODO: resolve why we need all these constructors - see issue 149
	/**
     * Construct a Bomberman referee for the provided match settings and players. 
     * Assumes a GUI is desired.
     *
     * @param match The settings for the match
     * @param players Players in the match
     */
    public BMReferee(MatchSetting match, PlayerRepresentation[] players) {
		this(match, players, null, true);
	}

    /**
     * Construct a new Bomberman referee.
     *
     * @param match The settings for the match
     * @param players Players in the match
     * @param ui whether the referee should spawn an associated graphical display of the game
     */
    public BMReferee(MatchSetting match, PlayerRepresentation[] players, boolean ui) {
        this(match, players, null, ui);
    }

    /**
     * Do not use this constructor.  For now, assumes UI should not be enabled.
     *
     * @param match
     * @param players
     * @param board
     */
    @Deprecated
    public BMReferee(MatchSetting match, PlayerRepresentation[] players, BMBoard board) {
        this(match, players, board, false);
    }

    /**
     * @param match Match settings - assumes simultaneous play if this is null.
     * @param players Match participants
     * @param board Initial game state
     * @param ui Whether a user interface showing the game should be created or not
     */
    public BMReferee(MatchSetting match, PlayerRepresentation[] players, BMBoard board, boolean ui) {
        super(match, players); // If this succeeds, players is not null
        this.board = board;
        if (this.board == null) {
            int boardSize = BMBoard.DEFAULT_BOARD_SIZE;
            boolean perfect = false; // Default is imperfect information
            if (match != null) {
                boardSize = match.getBoardSize();
                perfect = match.getPerfectInformation();
            }
            this.board = new BMBoard(boardSize, players.length, perfect);
        }
        alternating = (match != null? match.getAlternating(): false);
        if (ui) {
			new BombermanFrame(this);
		}
    }

    /**
     * Called after all the players have connected. Handles the game flow and communication.
     */
    @Override public void run() {
        updateUI();
        // Game begins by sending an InitGameMessage to all players, containing their character and a char reprentation of the map.
        for (int i = 0; i<players.length; i++) {
			sendInitGameMessage(new BMInitGameMessage((char) ('A' + i), i, board), i);
        }
        Log.info("Init game message sent, game will start in "+ GAME_START_DELAY/1000 + " seconds.");
        board.printPretty();
        pause(GAME_START_DELAY);

        boolean gameOver = false;
        // main game loop
        while (!gameOver) {
            Log.info("=============================================");
            Log.info("                  Round " + board.getRound());
            Log.info("=============================================");
            ArrayList<String> output = new ArrayList<>(); // TODO: Why not just log this in-place, without a buffer? See issue 150
            // TODO: merge these two methods - see issue 151
            if (alternating) {
                playAlternatingRound(output);
            } else {
                playRound(output, false);
            }
            board.printPretty();
            for (String s : output) {
                Log.info(s);
            }
            Log.info("Scores:");
            for (int i = 0; i < board.getScores().length; i++) {
                Log.info("	Player " + i + ": " + board.getScores()[i] + "		("
                                           + board.getPlayer(i).getNofBombs() + "/"
                                           + board.getPlayer(i).getMaxNofBombs() + "|"
                                           + board.getPlayer(i).getBlastRadius() + ")");
            }
            updateUI();
            if (logic.isTerminal(board)) {
                gameOver = true;
                continue;
            }
            pause(BETWEEN_ROUND_DELAY);
        }
        Log.info("=============================================");
        Log.info("                  Game terminated");
        Log.info("=============================================");
        board.printPretty();

        // After game ends, a GameTerminatedMessage is sent to all players, including the final score.
        for (int i = 0; i < players.length; i++) {
            sendGameTerminatedMessage(new BMGameTerminateMessage(eval.getScore(board)), i);
        }

        // Post result on event bus for publish-subscribe system.
        // Amongst other things, used for communicating result to tournament engine.
        EventBus bus = GameSystem.getInstance().getEventBus();
        bus.post(new BMGameTerminateMessage(eval.getScore(board)));

        // Log final results
        Log.info();
        Log.info("Game has terminated!");
        Log.info("Final scores:");
        double[] score = eval.getScore(board);
        for (int i = 0; i < score.length; i++) {
            Log.info("Player "+i+": "+score[i]);
        }
        Log.info();
        Log.info("Referee shutting down.");
    }

    /**
     * Performs one game loop for <b>simultaneous</b> play:
     * 1) decreasing bomb timers, exploding bombs, and generating upgrades as necessary
     * 2) asking each player to generate a move in parallel
     * 3) applying the moves selected by the players in random order
     * 4) cleaning up board
     *
     * @param output A buffer for all output, will be logged in a batch when the round is completed.
     * @param skipBombUpdate Set this to true to skip bomb timers decreasing in this round (usually false)
     */
    public void playRound(ArrayList<String> output, boolean skipBombUpdate) {
        // TODO: Get rid of skipbombupdate - only used by the MCTS, I think - see issue 151
        if (!skipBombUpdate) {
            //Update bombs and release resulting upgrades
        	logic.applyPlayedMoveMessage(new UpdateBombsMessage(-1, true), board);
        	distributeAcceptedMove(-1, new UpdateBombsMessage(-1, false));
            distributeAcceptedMove(-1, new NewUpgradesMessage(board.getVisibleBlastUpgrades(), board.getVisibleBombUpgrades()));
        }
        Map<PlayerRepresentation, PlayActionMessage> messages = requestMovesParallel(output);
        // Step through players in random order, and play their moves
        ArrayList<PlayerRepresentation> nextPlayers = new ArrayList<>(messages.keySet());
        Collections.shuffle(nextPlayers);
        for (PlayerRepresentation player: nextPlayers) {
            int playerId = player.getID();
            Log.trace("making move of player " + player.getID());
            PlayActionMessage data = messages.get(player);
            Action generatedMove = (data == null ? null : data.getAction());
            if (generatedMove == null) {
                output.add("Player: " + playerId + "(" + players[playerId].toString() + "): sent no move in.");
            } else {
                output.add("Player: " + playerId + "(" + players[playerId].toString() + "): " + generatedMove.toString());
                if (logic.validMove(generatedMove, playerId, board)) {
					applyAndDistribute(new PlayedMoveMessage(data));
                } else {
                    output.add("Invalid move by " + playerId + " tried to: " + generatedMove.getClass().getName());
                }
            }
        }
        applyAndDistribute(new NextRoundMessage());
    }
    
    /**
     * Performs one game loop for <b>alternating</b> play:
     * For each player:
     * 1) its bombtimers are reduced, bombs with 0 are detonated, and upgrades are revealed as necessary.
     * 2) request the player for a move, and apply the move selected
     * 3) cleaning up board
     * 4) update the UI (note this does not happen for each player with simultaneous play)
     *
     * @param output A buffer for all output for the round, which will be logged in a batch when the round is completed.
     */
    public void playAlternatingRound(ArrayList<String> output) {
        for (PlayerRepresentation engineTurn : players) {
            int playerId = engineTurn.getID();
        	logic.applyPlayedMoveMessage(new UpdateBombsMessage(playerId, true), board);
        	distributeAcceptedMove(-1, new UpdateBombsMessage(playerId, false));
            distributeAcceptedMove(-1, new NewUpgradesMessage(board.getVisibleBlastUpgrades(), board.getVisibleBombUpgrades()));

            //If the player is still alive request a move from him.
            if (board.getPlayer(playerId).isAlive()) {
                // Get move back - if not a {@link BMPlayMoveMessage}, it's not a legal move, but might be a timeout
            	PlayActionMessage moveReceived = engineTurn.genAction(new BMGenActionMessage(playerId, board.getRound(), TIME_PER_ROUND));
				Action generatedMove = moveReceived.getAction();
                if (generatedMove == null) {
                    output.add("Player: " + playerId + "(" + players[playerId].toString() + "): sent no move in.");
                } else {
                    output.add("Player: " + playerId + "(" + players[playerId].toString() + "): " + generatedMove.toString());
                    if (logic.validMove(generatedMove, playerId, board)) {
						applyAndDistribute(new PlayedMoveMessage(moveReceived));
                    } else {
                        output.add("Invalid move by " + playerId + " tried to: " + generatedMove.getClass().getName());
                    }
                }
            } else {
                output.add("Player: " + playerId + "(" + players[playerId].toString() + "): dead.");
            }
            updateUI();
        }
        applyAndDistribute(new NextRoundMessage());
    }

    /** Returns the current game state */
    public BMBoard getBoard() {
        return board;
    }

    /** Uses the game logic to apply the message, then distributes the message to all players */
	private void applyAndDistribute(PlayedMoveMessage p) {
        logic.applyPlayedMoveMessage(p, board);
        distributeAcceptedMove(-1, p);
    }
    
    /** Update GUI, if activated. */
    private void updateUI() {
        setChanged();
        notifyObservers(board.asCharArray());
    }

    /**
     * Pause the given number of milliseconds, or until interrupted (which should not be done).
     */
    private void pause(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            Log.warn("Bomberman Referee", "Unexpected interruption during pause", e);
        }
    }

    /**
     * Creates a new parallelMoveRequester that returns a hashmap of players to move. 
     * <b>If a player returned null, it is not contained in the hashmap.</b> (This is because a concurrent hash map cannot handle null values.)
     *
     * @param output
     *
     * @return Map from players to their selected messages.
     */
    private Map<PlayerRepresentation, PlayActionMessage> requestMovesParallel(ArrayList<String> output){
    	return new ParallelMoveRequester().requestMovesInParallel(output);
    }

    /**
     * Class for retrieving moves from each player in parallel.
     */
    private class ParallelMoveRequester{
    	
    	private int finishedThreads = 0;
    	
    	/**
    	 * Starts one thread for each player. The thread will ask the player to make a move and put it into the hashmap.
    	 * If the player returns null, it will not be added to the map.
    	 * @param output
    	 * @return
    	 */
    	private Map<PlayerRepresentation, PlayActionMessage> requestMovesInParallel(ArrayList<String> output){
    		// Data structure for moves of each player - needs to be thread-safe
            Map<PlayerRepresentation, PlayActionMessage> messages = new ConcurrentHashMap<>();
            // Generate each (alive) player's move
            for (PlayerRepresentation engineTurn : players) {
                int playerId = engineTurn.getID();
                requestMoveAsync(playerId, output, messages);
            }
            while(finishedThreads != players.length){
	            synchronized (this) {
	            	try{
                        // TODO: Figure out this use of a wait interval - see issue 131
	            		this.wait(CHECK_MOVE_INTERVAL);
	            	}catch(InterruptedException e){
	            		// No logic necessary here.
	            	}
				}
	            Log.debug("finished threads: "+finishedThreads);
            }
            return messages;
    	}
    	
    	/**
    	 * Requests one move from the specified player. Increases count of finished threads after move is received.
         *
    	 * @param playerId
    	 * @param output
    	 * @param map
    	 */
    	private void requestMoveAsync(final int playerId, final ArrayList<String> output, final Map<PlayerRepresentation, PlayActionMessage> map){
    		new Thread(){
    			@Override
				public void run() {
    				Log.trace("Starting thread for player "+playerId);
                    //If the player is still alive request a move from him.
    				if (board.getPlayer(playerId).isAlive()) {
                        Log.trace("asking player " + playerId);
                        PlayActionMessage message = players[playerId].genAction(new BMGenActionMessage(playerId, board.getRound(), TIME_PER_ROUND));
                        if (message != null){
                        	map.put(players[playerId], message);
                        }
                    } else {
                        output.add("Player: " + playerId + "(" + players[playerId].toString() + "): dead.");
                    }
    				
    				synchronized (ParallelMoveRequester.this) {
    					finishedThreads++;
    					ParallelMoveRequester.this.notifyAll();
					}
    				Log.trace("Finished thread for player "+playerId);
    			};
    		}.start();
		}
    }
}
