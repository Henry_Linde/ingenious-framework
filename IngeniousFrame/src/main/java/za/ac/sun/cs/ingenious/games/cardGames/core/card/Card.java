package za.ac.sun.cs.ingenious.games.cardGames.core.card;

import java.io.Serializable;

/**
 * 
 * The most basic unit of a card game
 *
 * @param <F1> Generic type of first feature (implements CardFeature).
 * @param <F2> Generic type of second feature (implements CardFeature).
 */
public class Card<F1 extends CardFeature, F2 extends CardFeature> implements Comparable<Card<F1, F2>>, Serializable{

	/** The serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** Static ID-counter. Each card has a unique ID - first id: 0 */
	private static int nextID = 0;
	
	/** First feature. */
	private F1 f1;
	
	/** Second feature. */
	private F2 f2;
	
	/** ID for instantiated card. */
	private int ID;
	
	/**
	 * Instantiates a new card with two features.
	 *
	 * @param f1 First feature
	 * @param f2 Second Feature
	 */
	public Card(F1 f1, F2 f2){
		this.f1 = f1;
		this.f2 = f2;
		this.ID = nextID++;
	}
	
	/**
	 * Instantiates a new card with only one feature.
	 * Second feature is set to null.
	 *
	 * @param f1 the f 1
	 */
	public Card(F1 f1){
		this(f1,null);
	}
	
	
	
	/**
	 * Gets first feature.
	 *
	 * @return f1
	 */
	public F1 getf1() {
		return f1;
	}
	

	/**
	 * Gets second feature2.
	 *
	 * @return f2
	 * @throws NullPointerException if second feature is null.
	 */
	public F2 getf2() {
		if(f2==null){
			throw new NullPointerException("Second feature is null!");
		}
		return f2;
	}
	
	/**
	 * Gets ID.
	 *
	 * @return ID
	 */
	public int getID(){
		return ID;
	}
	
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 * 
	 * @return Returns if the features of two cards are identical. Check ID to verify if it's exactly the same card.
	 */
	// 
	public boolean equals(Object other){
		if(other == null){return false;}
		if(other == this){return true;}
		if(! (other instanceof Card)){return false;}
		Card<F1, F2> otherCard = (Card<F1, F2>) other;
		return this.f2.equals(otherCard.getf2()) && this.f1.equals(otherCard.getf1());
	}
	
	/**
	 * @see java.lang.Object#hashCode()
	 * 
	 * @return Returns some hash code.
	 */
	@Override
	public int hashCode(){
		String s = toString();
		int hash = 3;
		for(int i=0; i<s.length(); i++){
			hash = hash*7 + (int)s.charAt(i);
		}
		return hash;
	}
	
	/**
	 * @see java.lang.Object#toString().
	 * 
	 * Example: king of spades. Syntax might be unsuitable for some games.
	 * 
	 * @return Returns toString() of both features
	 */
	public String toString(){
		if(f2==null){
			return getf1().toString().toLowerCase();
		}
		return getf1().toString().toLowerCase() + " of " + getf2().toString().toLowerCase();
	}

	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 * 
	 * Compares two cards based on the value of their features (CardFeature).
	 * Assumption: first feature is stronger than second feature.
	 */
	@Override
	public int compareTo(Card<F1, F2> other) {
		if(this.f2.equals(other.getf2())){
			return this.getf1().getValue() - other.getf1().getValue();
		}
		else{
			return this.getf2().getValue() - other.getf2().getValue();
		}
	}
}
