package za.ac.sun.cs.ingenious.games.shadowtictactoe;

import com.esotericsoftware.minlog.Log;

import java.util.List;
import java.util.Random;

import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.core.model.Move;
import za.ac.sun.cs.ingenious.core.model.MoveObserver;
import za.ac.sun.cs.ingenious.core.model.TurnBasedGameLogic;
import za.ac.sun.cs.ingenious.core.model.TurnBasedSquareBoard;
import za.ac.sun.cs.ingenious.core.model.UnobservedMove;
import za.ac.sun.cs.ingenious.core.model.XYAction;
import za.ac.sun.cs.ingenious.games.tictactoe.TTTLogic;
import za.ac.sun.cs.ingenious.search.InformationSetDeterminizer;

public class STTTLogic implements TurnBasedGameLogic<STTTGameState>, InformationSetDeterminizer<STTTGameState>, MoveObserver<STTTGameState> {

	private static final Random rand = new Random();
	
	private final TTTLogic tttLogic = new TTTLogic();

	@Override
	public boolean validMove(STTTGameState fromState, Move move) {
		if (move instanceof XYAction) {
			return tttLogic.validMove(fromState.playerBoards[((XYAction)move).getPlayer()], move);
		} else if (move instanceof UnobservedMove) {
			return true;
		} else {
			return false;			
		}
	}

	@Override
	public boolean makeMove(STTTGameState fromState, Move move) {
		if (move instanceof UnobservedMove) {
			fromState.moveHistories.get(((UnobservedMove)move).getPlayerID()).add(move);
			fromState.nextMovePlayerID = fromState.nextMovePlayerID == 0 ? 1 : 0;
			syncNextMovePlayerIDs(fromState);
			return true;
		} else if (move instanceof XYAction) {
			XYAction xyMove = (XYAction) move;
			fromState.moveHistories.get(xyMove.getPlayer()).add(move);
			if (!tttLogic.validMove(fromState.playerBoards[xyMove.getPlayer()==0?1:0],xyMove)) {
				tttLogic.makeMove(fromState.playerBoards[xyMove.getPlayer()], new XYAction(xyMove.getX(),xyMove.getY(),xyMove.getPlayer()==0?1:0));				
			} else {
				tttLogic.makeMove(fromState.playerBoards[xyMove.getPlayer()], xyMove);
			}
			fromState.nextMovePlayerID = fromState.nextMovePlayerID == 0 ? 1 : 0;
			syncNextMovePlayerIDs(fromState);
			return true;
		} else {
			return false;
		}
	}
	
	private void syncNextMovePlayerIDs(STTTGameState forState) {
		forState.playerBoards[0].nextMovePlayerID = forState.nextMovePlayerID;
		forState.playerBoards[1].nextMovePlayerID = forState.nextMovePlayerID;
	}

	@Override
	public void undoMove(STTTGameState fromState, Move move) {
		throw new UnsupportedOperationException();		
	}

	@Override
	public List<Action> generateActions(STTTGameState fromState, int forPlayerID) {
		return tttLogic.generateActions(fromState.playerBoards[forPlayerID], forPlayerID);
	}

	@Override
	public boolean isTerminal(STTTGameState state) {
		return tttLogic.isTerminal(state.playerBoards[0]) || tttLogic.isTerminal(state.playerBoards[1]);
	}
	
	@Override
	public STTTGameState determinizeUnknownInformation(int forPlayerID, STTTGameState forState) {
		// Determine player IDs, we assume the next player to move is the player holding this state
		int myID = forPlayerID;
		int opponentID = (forPlayerID == 0) ? 1 : 0;
		
		// Create the determinized state to return
		STTTGameState newState = (STTTGameState) forState.deepCopy();
		
		// Some shortcuts
		TurnBasedSquareBoard currentState = newState.playerBoards[myID];
		byte[] opponentsBoard = newState.playerBoards[opponentID].board;
		List<Move> myMoveHistory = newState.moveHistories.get(myID);
		
		// Count marks on the board
		int numOpponentMarks = 0;
		int numMyMarks = 0;
		int freeSpots = 0;
		for (int i = 0; i < currentState.board.length; i++) {
			if (currentState.board[i] == 0) {
				freeSpots++;
			} else if (currentState.board[i] == (myID+1)) {
				numMyMarks++;
			} else {
				numOpponentMarks++;
				opponentsBoard[i] = currentState.board[i];
			}
		}
		// The number of moves I made is the number of all marks on the board
		// (marks I made myself and marks of the opponent that I uncovered
		int numMyMoves = numMyMarks + numOpponentMarks;
		if (numMyMoves != myMoveHistory.size()) {
			Log.error("Inconsistency when determinizing STTT information: numMyMoves != myMoveHistory.size()");
		}
		// My opponent has made the same number of moves ...
		int numOpponentMoves = numMyMoves;
		if (myID == 1)
		 {
			numOpponentMoves++; // ... then they made one more
		}
		
		if (numOpponentMoves == 0 || numOpponentMoves-numOpponentMarks == 0) {
			return newState;
		}

		// Now I repeatedly produce possible opponent move sequences until I arrive at a possible
		// determinized state that is not terminal
		TurnBasedSquareBoard testState = null;
		XYAction[] opponentMoveHistory = null;
		do { 
			opponentMoveHistory = new XYAction[numOpponentMoves];			
			testState = new TurnBasedSquareBoard(3,0,2);
			
			// Firstly, consider all the opponent move that I already uncovered
			for (int i = 0; i < numMyMoves; i++) {
				// For all the opponent moves that I uncovered ...
				if (((XYAction)myMoveHistory.get(i)).getPlayer() == opponentID) {
					// .. randomize the time at which my opponent made that move
					if (i==0 && myID!=1) { // Safety check: If I am the first player and uncovered an opponent move in my first action - makes no sense
						System.out.println("Inconsistency when determinizing STTT information: i==0 && myID!=1");
					}
					int posInOppMoveHistory = 0;
					do {
						posInOppMoveHistory = rand.nextInt(i + (myID==1?1:0));
					} while (opponentMoveHistory[posInOppMoveHistory]!=null);
					opponentMoveHistory[posInOppMoveHistory] = (XYAction)myMoveHistory.get(i);
					// Also apply the opponent moves to the testState
					tttLogic.makeMove(testState, opponentMoveHistory[posInOppMoveHistory]);
				}
			}
			
			// Now consider all the move that the opponent made that I don't know about
			// They may have uncovered my own marks or taken other free spots
			for (int i = 0; i < numOpponentMoves; i++) {
				// For all opponent moves that I don't know about ...
				if (opponentMoveHistory[i] == null) {
					// ... apply some domain knowledge or ...
//					if (i==0 && myID==1) {
//						// Assume that my opponent always plays the strategically optimal central 
//						// position if they turn first - this assumption is WRONG when playing
//						// against a random opponent of course
//						opponentMoveHistory[i] = new XYMove(1,1,opponentID);
//						tttLogic.makeMove(opponentMoveHistory[i], testState);
//						continue;
//					}
					
					// ... make a random move
					List<Action> actions = tttLogic.generateActions(testState, opponentID);
					
					if(!actions.isEmpty()){ // If any moves are possible...
						XYAction pickedMove = (XYAction) actions.get(rand.nextInt(actions.size())); //pick a random one
						if (currentState.board[currentState.xyToIndex(pickedMove.getX(), pickedMove.getY())] == myID+1) {
							opponentMoveHistory[i] = new XYAction(pickedMove.getX(),pickedMove.getY(),myID);
						} else { 
							opponentMoveHistory[i] = pickedMove;
						}
						// ... apply the opponent moves to the testState
						tttLogic.makeMove(testState, opponentMoveHistory[i]);
					} else {
						System.out.println("Determinizing STTT, Logic error: opponent can't move");
					}
				}
			}
		} while (tttLogic.isTerminal(testState));
		
		for (XYAction m : opponentMoveHistory) {
			newState.moveHistories.get(opponentID).add(m);
		}
		
		// The test state now contains all moves by the opponent
		newState.playerBoards[opponentID].board = testState.board;
		return newState;
	}

	@Override
	public Move fromPointOfView(Action originalMove, STTTGameState currentState, int pointOfViewPlayerID) {
		if (originalMove instanceof XYAction) {
			if (((XYAction)originalMove).getPlayer()!=pointOfViewPlayerID) {
				return new UnobservedMove(((XYAction)originalMove).getPlayer());
			}
		}
		return originalMove;
	}

}
