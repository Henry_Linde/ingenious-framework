package za.ac.sun.cs.ingenious.search.mcts;

import com.esotericsoftware.minlog.Log;

import java.util.ArrayList;
import java.util.Arrays;

import za.ac.sun.cs.ingenious.core.model.GameState;
import za.ac.sun.cs.ingenious.core.model.Action;

/**
 * Implements MCTS for any game supporting the required interfaces
 */
public final class MCTS {

	private MCTS() {}	

	/**
	 * This method will generate a move by building up a game tree, according to the rules in TreeDescender and TreeUpdater.
	 * 
	 * @param root The root node, holding the current game board and the current player.
	 * @param policy The policy for the playout. Most simple is to use {@link RandomPolicy}, which will just do random playouts.
	 * @param descender The descender that decides which node to expand next
	 * @param updater The updater, that backpropagates the result of a playout through the game tree.
	 * @param turnLength Time in ms to finish generating this move
	 * @param printouts True if information on the search shall be logged
	 *
	 * @return returns the best move found after the specified amount of time.
	 */
	public static <S extends GameState, N extends SearchNode<S,N>> Action generateMove(N root, PlayoutPolicy<N> policy,
			TreeDescender<N> descender , TreeUpdater<N> updater,
			long turnLength, boolean printouts) {
		long timeInit = System.currentTimeMillis();
		long endTime = timeInit + turnLength;
		
		int numberOfPlayouts = 0;
		while(System.currentTimeMillis() < endTime){
			numberOfPlayouts++;
			N bestNode = descender.bestSearchMove(root, numberOfPlayouts);
			N newNode = bestNode;
			if(bestNode.expandable()){
				newNode = bestNode.expandAChild(descender.bestExpandMove(bestNode));
			}
			updater.backupValue(newNode, policy.playout(newNode));
		}	
		Action bestMove = descender.bestPlayMove(root);

		if (printouts) {
			printTreeOverview(root);
			printPossibleMoves(root);
			if (bestMove != null)
				Log.info("MCTS", "Best move: "+ bestMove.toString());
			else
				Log.info("MCTS", "No best move found");
			Log.info("MCTS", "NUMBER OF PLAYOUTS : "+numberOfPlayouts);
		}

		return bestMove;
	}
	
	/**
	 * Helper method to nicely print the first level of the game tree.
	 * @param root The root of the tree
	 */
	public static <S extends GameState, N extends SearchNode<S,N>> void printPossibleMoves(N root){
		Log.info();
		Log.info("MCTS", "===================");
		Log.info("MCTS", "Possible moves:");
		for(N node : root.getExpandedChildren()){
			Log.info("MCTS", node.getMove().toString() + " winrate: "+ Arrays.toString(node.getReward()) + "/" + node.getVisits());
		}
		Log.info("MCTS", "===================");
	}
	

	/**
	 * Print the tree, this generates excessive printout.
	 * @param root The root of the tree
	 */
	public static <S extends GameState, N extends SearchNode<S,N>> void printTree(N root){

		Log.info("MCTS", "===================");
		Log.info("MCTS", "Current  search tree:");
		
		ArrayList<N> list = new ArrayList<>();
		list.add(root);
		while(!list.isEmpty()){
			N n = list.remove(0);
			Log.info("MCTS", n.toString());
			list.addAll(n.getExpandedChildren());
		}
		Log.info("MCTS", "===================");
	}
	
	
	
	/**
	 * Prints an overview over the amount of nodes in the logginsearch tree to a depth of 10.
	 * @param root The root of the tree
	 */
	public static <S extends GameState, N extends SearchNode<S,N>> void printTreeOverview(N root){
		Log.info("MCTS", "===================");
		Log.info("MCTS", "Current  search tree:");
		
		int[] count = new int[10];
		int[] expandableCount = new int[10];
		
		ArrayList<N> list = new ArrayList<>();
		list.add(root);
		while(!list.isEmpty() ){
			N n = list.remove(0);

			if(n.getDepth() >= 10){
				break;
			}

			count[n.getDepth()]++;
			if(n.expandable()){
				expandableCount[n.getDepth()]++;
			}
			list.addAll(n.getExpandedChildren());
		}
		
		for (int i = 0; i < count.length; i++) {
			Log.info("MCTS", "depth " + i + ": "+count[i] + "	("+expandableCount[i]+")");
		}
		

		
		Log.info("MCTS", "===================");
	}
}
