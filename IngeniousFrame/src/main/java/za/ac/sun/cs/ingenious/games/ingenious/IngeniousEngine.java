package za.ac.sun.cs.ingenious.games.ingenious;

import java.net.Socket;
import java.util.ArrayList;

import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.games.ingenious.engines.IMoveController;
import za.ac.sun.cs.ingenious.games.ingenious.engines.IngeniousEvaluator;
import za.ac.sun.cs.ingenious.games.ingenious.engines.IngeniousScoreKeeper;
import za.ac.sun.cs.ingenious.games.ingenious.engines.TrailingColoursEval;

public abstract class IngeniousEngine extends Engine {
	public IMoveController moveController;
	public ArrayList<Tile> rack = new ArrayList<Tile>();
	public ArrayList<IngeniousRack> racks = new ArrayList<IngeniousRack>();
	public IngeniousScoreKeeper scoreKeeper;
	public ArrayList<Tile> expectedBag = new ArrayList<Tile>();
	public IngeniousEvaluator evaluator = new TrailingColoursEval();
	
	public MatchSetting matchSetting;	
	
	
	/**
	 * @deprecated
	 * @param s
	 * @param m
	 * @param id
	 */
	public IngeniousEngine(Socket s, MatchSetting m, int id){
		//TODO this constructor should never be called, the next line will cause errors
		super(new EngineToServerConnection(s, null, null, id));
		this.playerID = id;
	}
	
	
	/**
	 * @deprecated
	 * @param match
	 * @param playerId
	 */
	public IngeniousEngine(MatchSetting match,int playerId){	
		//TODO this constructor should never be called, the next line will cause errors
		super(new EngineToServerConnection(null, null, null, playerId));
		this.matchSetting = match;
		this.playerID = playerId;
		for(int i = 0; i< match.getNumPlayers();i++){
			racks.add(new IngeniousRack(match.getVariants()[i].getRackSize()));
		}
		this.scoreKeeper = new IngeniousScoreKeeper(match.getNumPlayers(),match.getNumColours());
	}
	
	public IngeniousEngine(EngineToServerConnection toServer){
		super(toServer);
		this.playerID = 0;
	}
	
	public void receiveBag(String [] msg){
		Tile next;
		for (int i = 1; i < msg.length; i = i + 2) {
			next = new Tile(Integer.parseInt(msg[i]),
					Integer.parseInt(msg[i + 1]));
			expectedBag.add(next);
			System.out.println("TILE ADDED TO BAG : " + next);
		}
	}
	public void receiveRack(String [] msg){
		Tile next;
		int id = Integer.parseInt(msg[1]);
		for (int i = 2; i < msg.length; i = i + 2) {
			next = new Tile(Integer.parseInt(msg[i]),
					Integer.parseInt(msg[i + 1]));
			racks.get(id).add(next);
			System.out.println("TILE ADDED TO RACK : " + next);
		}
	}
	
	public void receiveDraw(String [] msg){
		int id = Integer.parseInt(msg[1]);
		Tile tile = new Tile(Integer.parseInt(msg[2]),
				Integer.parseInt(msg[3]));
		racks.get(id).add(tile);
	}


	@Override
	public String engineName() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void receiveInitGameMessage(InitGameMessage a) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void receivePlayedMoveMessage(PlayedMoveMessage a) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
		// TODO Auto-generated method stub
		return null;
	}
}
