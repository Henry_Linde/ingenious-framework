package za.ac.sun.cs.ingenious.search.mcts;

import static java.lang.Math.sqrt;

import za.ac.sun.cs.ingenious.core.model.GameState;

/**
 * Extends the SimpleDescender with the UCT search value formula
 * @author steve
 * @author Michael Krause
 */
public class UCTDescender<N extends SearchNode<? extends GameState,N>> extends SimpleDescender<N> {
	
	/**
	 * @param c The constant in the UCT search value formula
	 */
	public UCTDescender(double c) {
		C = c;
	}

	/**
	 * Sets C to a commonly used value near 0.7
	 */
	public UCTDescender() {
		this(1.0/sqrt(2.0));
	}
	
	final double C;
	
	@Override
	public double searchValue(N node, int forPlayerID, int totalNofPlayouts) {
		double parentPlayouts = node.getParent().getVisits();	
		
		double barX = node.getReward()[forPlayerID]/(double)node.getVisits();
		//double C = 1.0/((double)totalNofPlayouts/50.0);
		
		double uncertainty  = 2.0*C*sqrt(2.0*Math.log(parentPlayouts)/(double)node.getVisits());
		
		return barX+uncertainty;
	}
}
