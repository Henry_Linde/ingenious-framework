package za.ac.sun.cs.ingenious.games.shadowtictactoe;

import com.esotericsoftware.minlog.Log;

import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.core.model.Move;
import za.ac.sun.cs.ingenious.core.model.XYAction;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.games.tictactoe.TTTFinalEvaluator;
import za.ac.sun.cs.ingenious.games.tictactoe.TTTLogic;

public abstract class STTTEngine extends Engine {

	protected STTTGameState currentState;
	protected STTTLogic logic;	
	private final TTTLogic tttLogic = new TTTLogic();
	
	/**
	 * The required constructor for the TCP back-end to work
	 * @param toServer
	 */
	public STTTEngine(EngineToServerConnection toServer) {
		super(toServer);
		this.logic = new STTTLogic();
		this.currentState = new STTTGameState();
	}
	
	/**
	 * ============================================================================================================
	 * 													Required Methods
	 *  ============================================================================================================
	 */
	
	/**
	 * Put any code here that needs to be called to initialize game (e.g. setup board, etc).
	 * In case of TicTacToe nothing needs to be initialized, the board always looks the same.
	 */
	@Override
	public void receiveInitGameMessage(InitGameMessage a) {
		Log.info("Started Shadow TicTacToe");
	}
	
	/**
	 * Called after another player made a move. Put logic here to update your local gamestate accordingly.
	 * @param a 
	 */
	@Override
	public void receivePlayedMoveMessage(PlayedMoveMessage a) {
		Move move = a.getMove();
		if (move instanceof OccupiedXYAction) {
			OccupiedXYAction xyMove = (OccupiedXYAction) move;
			int opponentID = playerID==0?1:0;
			// Make a mark on the opponents board, as we know now that they made a move at that spot
			tttLogic.makeMove(currentState.playerBoards[opponentID], new XYAction(xyMove.getX(), xyMove.getY(), opponentID));
			logic.makeMove(currentState, new XYAction(xyMove.getX(), xyMove.getY(), playerID));	
		} else {
			logic.makeMove(currentState, (Action) a.getMove());
		}
		
		Log.info("I am player " + playerID + " and this is what my board looks like:");
		this.currentState.printPretty();
	}
	
	/**
	 * Put any code here that shall be executed when a game terminated message is sent by server, e.g. print the winner and the board.
	 * Finally close connection to release socket on server.
	 */
	@Override
	public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
		TTTFinalEvaluator eval = new TTTFinalEvaluator();
		Log.info("Game terminated.");
        Log.info("Final scores for my terminal position:");
        double[] score = eval.getScore(currentState.playerBoards[this.playerID]);
        for (int i = 0; i < score.length; i++) {
            Log.info("Player "+i+": "+score[i]);
        }
		Log.info("Final state as imagined by player " + this.playerID + ":");
		currentState.printPretty();
		toServer.closeConnection();
	}
	
}
