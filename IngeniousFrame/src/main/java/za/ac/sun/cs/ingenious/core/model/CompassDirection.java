package za.ac.sun.cs.ingenious.core.model;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;

/**
 * An enum for representing points on the compass.  The 16 clasically named points
 * are represented by their traditional abbreviations in clockwise order beginning
 * from north.
 *
 * The enum also provides functionality for dealing with cardinal directions as a subset
 * of this collection.
 *
 * @author Steve Kroon
 */
public enum CompassDirection {
    N("north"),
    NNE("north-northeast"),
    NE("northeast"),
    ENE("east-northeast"),
    E("east"),
    ESE("east-southeast"),
    SE("southeast"),
    SSE("south-southeast"),
    S("south"),
    SSW("south-southwest"),
    SW("southwest"),
    WSW("west-southwest"),
    W("west"),
    WNW("west-northwest"),
    NW("northwest"),
    NNW("north-northwest");

    // Consider an ImmutableEnumSet - see http://stackoverflow.com/questions/3603810/are-there-plans-for-immutableenumset-in-java-7
    private static Set<CompassDirection> cardinals = Collections.unmodifiableSet(EnumSet.of(N,E,S,W));
    
    // Whole name for toString() method.
    private String toStringName;
    
    private CompassDirection(String toStringName){
    	this.toStringName = toStringName;
    }

    /**
     * Returns an unmodifiable set containing the enum elements corresponding to the cardinal directions.
     */
    public static Set<CompassDirection> cardinalDirections() {
        return cardinals;
    }

    /**
     * @return true if the specified direction is a cardinal direction, and false otherwise.
     */
    public boolean isCardinal() {
        return cardinalDirections().contains(this);
    }
    
    /**
     * Returns the full name of a direction
     */
    public String toString(){
    	return this.toStringName;
    }
}
