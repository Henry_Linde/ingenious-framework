package za.ac.sun.cs.ingenious.core.commandline.dte.client;

import com.google.gson.stream.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;

import za.ac.sun.cs.ingenious.core.GameSystem;

/**
 * Created by Chris Coetzee on 2016/07/30.
 * <p/>
 */
public class Config {
    public static final String CLIENT_CONFIG = "dce-client.json";

    private String engineName = ""; //the full classname of your engine

    public String getEngineName() {
        return engineName;
    }

    public static Config loadFromConfigFile() throws URISyntaxException, IOException {
        InputStream is = GameSystem.getInstance().openResourceSeek(CLIENT_CONFIG);
        return GameSystem.getInstance().getGson().fromJson(new JsonReader(new InputStreamReader(
                is)), Config.class);
    }
}