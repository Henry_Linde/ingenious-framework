package za.ac.sun.cs.ingenious.search.pimc;

import za.ac.sun.cs.ingenious.core.model.GameState;
import za.ac.sun.cs.ingenious.search.mcts.SearchNode;

public interface NodeCreator<S extends GameState, N extends SearchNode<S,N>> {
	
	public N createNode(S withState);
	
}
