package za.ac.sun.cs.ingenious.games.tictactoe.engines;

import java.util.List;
import java.util.Random;

import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.core.model.XYAction;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.games.tictactoe.TTTEngine;

/**
 * This engine just chooses a random, valid action when it has to act. 
 */
public class TTTRandomEngine extends TTTEngine {

	public TTTRandomEngine(EngineToServerConnection toServer) {
		super(toServer);
	}
	
	/**
	 * Never use spaces in engine name!
	 */
	@Override
	public String engineName() {
		return "TTTRandomEngine";		
	}
	
	/**
	 * This method is called when the server requests an action by this engine.
	 */
	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
		List<Action> actions = logic.generateActions(currentState, this.playerID);
		if(!actions.isEmpty()){
			Random rand = new Random();
			return new PlayActionMessage((XYAction)actions.get(rand.nextInt(actions.size())));
		} else
			return new PlayActionMessage(null); // Return a null action when none is possible
	}

}
