package za.ac.sun.cs.ingenious.games.cardGames.core.exceptions;

/**
 * The LocationNotFoundException.
 * 
 * Is raised when a certain location could not be found.
 */
public class LocationNotFoundException extends Exception{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 327482937429L;
	
	/**
	 * Instantiates a new location not found exception.
	 *
	 * @param message the message
	 */
	public LocationNotFoundException(String message){
		super(message);
	}
	
}
