package za.ac.sun.cs.ingenious.search.mcts;

import java.util.List;

import za.ac.sun.cs.ingenious.core.model.GameState;
import za.ac.sun.cs.ingenious.core.model.Action;

/**
 * Standard implementation of TreeDescender. Always searches for the node with the
 * best search value until an expandable node is reached.
 * In this implementation, the search value is just the total reward of the player
 * whose turn it is, divided by the number of visits to that node
 * 
 * @author steve
 * @author Michael Krause 
 */
public abstract class SimpleDescender<N extends SearchNode<? extends GameState,N>> implements TreeDescender<N> {

	@Override
	public Action bestExpandMove(N parent) {
		// The simplest implementation just chooses a random move from the parent node to expand
		List<Action> unExpandedMoves = parent.getMovesToExpand();
		int index = (int) (Math.random() * unExpandedMoves.size());
		return unExpandedMoves.get(index);
	}
	
	@Override
	public Action bestPlayMove(N root) {
		// Returns the move leading to the node with the highest visits count.
		// This isn't actually what most MCTS implementations do. 
		// Most implementations look for the highest search value, but in many cases the two will be equivalent
		if (root.getExpandedChildren().isEmpty())
			return null;
		
		int highestVisits = Integer.MIN_VALUE;
		N bestNode = null;
		for (N node : root.getExpandedChildren()) {			
			if (node.getVisits() > highestVisits) {
				highestVisits = node.getVisits();
				bestNode = node;
			}
		}
		return bestNode.getMove();
	}
	
	@Override
	public N bestSearchMove(N root, int totalNofPlayouts) {
		// Takes a greedy path trough the tree by always picking 
		// the node with the highest searchValue until it hits an expandable or terminal node.
		N bestNode = root;
		while(bestNode != null && !bestNode.expandable()){
			N newNode = bestChild(bestNode, totalNofPlayouts);
			if(newNode == null){ //the node is not expandable but at the same time has no children, which means it is terminal.
				return bestNode; //in that case the "playout" is just started from this node.
			}			
			bestNode = newNode;
		}	
		return bestNode;
	}

	/**
	 * Finds the child with the highest search value for the given node
	 */
	protected N bestChild(N node, int totalNofPlayouts){
		if(node.getExpandedChildren().isEmpty()){			
			return null;
		}
		double highestValue = Double.NEGATIVE_INFINITY;
		N bestMove = null;
		for(N child : node.getExpandedChildren()){
			double childValue = this.searchValue(child, node.getCurrentPlayer(), totalNofPlayouts);
			if( childValue > highestValue ){
				bestMove = child;
				highestValue = childValue; 
			}
		}
		return bestMove;	
	}
	
	/**
	 * Calculates the search value of a given node. During descent, the nodes with the 
	 * highest search values will be visited until an expandable or terminal node is reached. 
	 * @param node Node for which to calculate the searchValue
	 * @param forPlayerID Player for which to calculate the search value
	 * @param totalNofPlayouts Number of playouts that have been run before this call
	 * @return Search value of node
	 */
	protected abstract double searchValue(N node, int forPlayerID, int totalNofPlayouts);

}
