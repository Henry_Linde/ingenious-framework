package za.ac.sun.cs.ingenious.core.commandline.main;

import com.beust.jcommander.JCommander;
import com.esotericsoftware.minlog.Log;

import za.ac.sun.cs.ingenious.core.commandline.BaseArguments;
import za.ac.sun.cs.ingenious.core.commandline.client.ClientArguments;
import za.ac.sun.cs.ingenious.core.commandline.gamecreator.GameCreatorArguments;
import za.ac.sun.cs.ingenious.core.commandline.gameserver.GameServerArguments;

/**
 * Created by Chris Coetzee on 2016/07/27.
 */
public class MainArguments extends BaseArguments {
    private String command;

    public String getCommand() {
        return command;
    }

    @Override

    protected JCommander getJcommanderInstance() {
        JCommander m = new JCommander(this);
        m.addCommand("server", new GameServerArguments());
        m.addCommand("client", new ClientArguments());
        m.addCommand("create", new GameCreatorArguments());
        return m;
    }

    @Override public void validateAndPrepareArguments(JCommander commander) {
        this.command = commander.getParsedCommand();
        if (this.command == null) {
            Log.error("Missing command");
            commander.usage();
            throw new IllegalArgumentException();
        }
    }
}
