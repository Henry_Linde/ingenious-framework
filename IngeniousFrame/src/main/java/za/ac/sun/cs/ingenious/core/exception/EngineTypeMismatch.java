package za.ac.sun.cs.ingenious.core.exception;

import za.ac.sun.cs.ingenious.core.configuration.EngineConfig;

/**
 * Created by Chris Coetzee on 2016/07/18.
 */
public class EngineTypeMismatch extends EngineConfigurationError {
    public EngineTypeMismatch(String msg, EngineConfig e) {
        super(msg);
        super.setEngine(e);
    }
}
