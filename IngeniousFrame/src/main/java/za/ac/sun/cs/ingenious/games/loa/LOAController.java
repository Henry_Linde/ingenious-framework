package za.ac.sun.cs.ingenious.games.loa;

import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.Referee;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.model.Coord;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;

public class LOAController extends Referee implements Runnable{
	private LOABoard gameBoard;
	private LOALogic logic;

	public LOAController(MatchSetting match, PlayerRepresentation[] players) {
		super(match, players);
		this.gameBoard = new LOABoard(new int[][] {
			{2,1,1,1,1,1,1,2},
			{0,2,2,2,2,2,2,0},
			{0,2,2,2,2,2,2,0},
			{0,2,2,2,2,2,2,0},
			{0,2,2,2,2,2,2,0},
			{0,2,2,2,2,2,2,0},
			{0,2,2,2,2,2,2,0},
			{2,1,1,1,1,1,1,2}}, 0);
		this.logic = new LOALogic();
	}
	

	@Override
	public void run() {
		boolean gameOver = false;
		System.out.println("CONTROLLER CONNECTION CONCLUDED");
		System.out.println("CONTROLLER SETUP CONCLUDED");

		while (!gameOver) {
			for (PlayerRepresentation engineTurn : players) {
				int playerId = engineTurn.getID();
				System.out.println("CURRENT PLAYER NUMBER : " + playerId);

				
				PlayActionMessage data = engineTurn.genAction(new GenActionMessage());
				LOAAction generatedMove = data == null ? null : (LOAAction) data.getAction();

				if (generatedMove == null) {
					System.out.println("generate move pass.");
					break;
				}

				if (this.logic.makeMove(gameBoard,generatedMove)) {
					distributeAcceptedMove(-1, new PlayedMoveMessage(data));
					System.out.println(gameBoard);
				} else {
					System.out.println("Invalid move");
					gameOver = true;
					break;
				}
				
				System.out.println(gameBoard);
				
				if (this.logic.isTerminal(gameBoard)){
					System.out.println("GAME OVER");
					gameOver = true;
					break;
				}
			}
		}
	}
	
	public LOAAction fromArray(String[] moveReply){
		Coord from = new Coord(Integer.parseInt(moveReply[1]), Integer.parseInt(moveReply[2]));
		Coord to = new Coord(Integer.parseInt(moveReply[3]), Integer.parseInt(moveReply[4]));
		return new LOAAction(from, to);
	}
}
