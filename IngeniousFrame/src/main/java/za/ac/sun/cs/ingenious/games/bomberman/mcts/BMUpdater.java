package za.ac.sun.cs.ingenious.games.bomberman.mcts;

import za.ac.sun.cs.ingenious.search.mcts.SearchNode;
import za.ac.sun.cs.ingenious.search.mcts.SimpleUpdater;

public class BMUpdater extends SimpleUpdater {

	@Override
	public void backupValue(SearchNode node, double[] result) {
		int currentPlayer = node.getCurrentPlayer();
		SearchNode iterator = node;
		while (iterator != null) {
			if((currentPlayer+1)%4  == iterator.getCurrentPlayer())
				iterator.addPlayout(result);
			else
				iterator.addPlayout(new double[result.length]);
			iterator = iterator.getParent();
		}
	}

}
