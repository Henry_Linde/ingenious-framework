package za.ac.sun.cs.ingenious.core.model;

/**
 * Represents the state of a game where players take turns, i.e: per state there is
 * exactly one person that can act.
 * 
 * @author Michael Krause
 */
public abstract class TurnBasedGameState extends GameState {
	
	/**
	 * The ID of the player that will make the next move
	 */
	public int nextMovePlayerID;
	
	public TurnBasedGameState(int firstPlayer, int numPlayers) {
		super(numPlayers);
		this.nextMovePlayerID = firstPlayer;
	}

}
