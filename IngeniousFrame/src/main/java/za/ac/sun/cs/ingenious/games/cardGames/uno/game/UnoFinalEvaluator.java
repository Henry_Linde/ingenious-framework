package za.ac.sun.cs.ingenious.games.cardGames.uno.game;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import za.ac.sun.cs.ingenious.core.model.GameFinalEvaluator;
import za.ac.sun.cs.ingenious.games.cardGames.core.card.Card;
import za.ac.sun.cs.ingenious.games.cardGames.uno.cardFeatures.UnoSuits;
import za.ac.sun.cs.ingenious.games.cardGames.uno.cardFeatures.UnoSymbols;

public class UnoFinalEvaluator implements GameFinalEvaluator<UnoGameState>{
	
	private int winner;
	
	private int getWinner(UnoGameState forState) {
		TreeMap<Card<UnoSymbols, UnoSuits>, ArrayList<UnoLocations>> map = forState.getMap();
		ArrayList<UnoLocations> list = new ArrayList<>();
		
		for (Map.Entry<Card<UnoSymbols, UnoSuits>, ArrayList<UnoLocations>> entry : map.entrySet()) {
			for (UnoLocations location : entry.getValue()) {
				if (!list.contains(location)) {
					list.add(location);
				}
			}
		}
		
		for(int i=0; i<forState.getNumberOfPlayers(); i++){
			if(!list.contains(UnoLocations.values()[i])){
				winner = i;
				return winner;
			}
		}
		winner = -1;
		return winner;
	}

	@Override
	public double[] getScore(UnoGameState forState) {
		double[] score = new double[forState.getNumberOfPlayers()];
		for(int i=0; i<forState.getNumberOfPlayers(); i++){
			score[i] = -1;
		}
		score[winner] = 1;
		return score;
	}
}
