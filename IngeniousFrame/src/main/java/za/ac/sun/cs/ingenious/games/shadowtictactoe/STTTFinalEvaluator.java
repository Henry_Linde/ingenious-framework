package za.ac.sun.cs.ingenious.games.shadowtictactoe;

import za.ac.sun.cs.ingenious.core.model.GameFinalEvaluator;
import za.ac.sun.cs.ingenious.games.tictactoe.TTTFinalEvaluator;
import za.ac.sun.cs.ingenious.games.tictactoe.TTTLogic;

public class STTTFinalEvaluator implements GameFinalEvaluator<STTTGameState> {

	private final TTTLogic tttLogic = new TTTLogic();
	private final TTTFinalEvaluator tttEval = new TTTFinalEvaluator();

	private int getWinner(STTTGameState forState) {
		if (tttLogic.isTerminal(forState.playerBoards[0])) {
			return tttEval.getWinner(forState.playerBoards[0]);
		} else {
			return tttEval.getWinner(forState.playerBoards[1]);
		}
	}

	@Override
	public double[] getScore(STTTGameState forState) {
		double[] scores = new double[2];
		if(getWinner(forState) == -1){
			scores[0] = 0.5;
			scores[1] = 0.5;
		}else{
			scores[getWinner(forState)] = 1;
		}
		return scores;
	}
}
