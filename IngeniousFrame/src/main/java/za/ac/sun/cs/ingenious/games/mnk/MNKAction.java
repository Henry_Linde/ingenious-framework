package za.ac.sun.cs.ingenious.games.mnk;

import za.ac.sun.cs.ingenious.core.model.Action;

public class MNKAction implements Action {
	
	private static final long serialVersionUID = 1L;
	private short h,w;
	private int player;
	
	public MNKAction(short h, short w, int player){
		super();
		this.h = h;
		this.w = w;
		this.player = player;
		
	}
	
	public short getH(){
		return this.h;
	}
	
	public short getW(){
		return this.w;
	}
	
	public int getPlayer(){
		return this.player;
	}
	
	public String[] toArray(){
		return null;
	}
	
	public String toString(){
		return "h: " + this.h + "w: " + this.w;
	}
}
