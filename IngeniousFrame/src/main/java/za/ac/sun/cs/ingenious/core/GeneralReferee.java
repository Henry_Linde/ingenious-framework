package za.ac.sun.cs.ingenious.core;

import com.esotericsoftware.minlog.Log;

import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.model.GameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.model.GameLogic;
import za.ac.sun.cs.ingenious.core.model.GameState;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;

/**
 * A superclass that should work for most referees. The run method sends customizable
 * {@link InitGameMessage}s and {@link GameTerminatedMessage}s at the start and end of a
 * game. Furthermore, at the end of the game the results are logged and displayed on
 * screen.
 */
public abstract class GeneralReferee<S extends GameState, L extends GameLogic<S>, E extends GameFinalEvaluator<S>>
		extends Referee {

	protected S currentState;
	protected L logic;
	protected E eval;

	public GeneralReferee(MatchSetting match, PlayerRepresentation[] players, S currentState, L logic, E eval) {
		super(match, players);
		this.currentState = currentState;
		this.logic = logic;
		this.eval = eval;
	}

	/**
	 * Overwrite this method if your game requieres specific initialisation.
	 * @return The init game message that is to be sent to every player before the game starts.
	 */
	protected InitGameMessage createInitGameMessage(PlayerRepresentation player) {
		return new InitGameMessage();
	}

	/**
	 * Overwrite this method if your game requieres specific termination.
	 * @return The game terminate message that is to be sent to every player after the game ended.
	 */
	protected GameTerminatedMessage createTerminateGameMessage(PlayerRepresentation player) {
		return new GameTerminatedMessage();
	}

	/**
	 * Overwrite this method if your game requieres specific GenMoveMessages, e.g. supplying tiles, updating the deck, etc.
	 * @return The genMove message that is to be sent to the current player.
	 */
	protected GenActionMessage createGenMoveMessage(PlayerRepresentation player) {
		return new GenActionMessage();
	}

	/**
	 * Overwrite this method if you want to do custom things before the game starts. 
	 */
	protected void beforeGameStarts() {
		
	}

	/**
	 * Overwrite this method if you want to do custom things after the game terminated. 
	 */
	protected void afterGameTerminated() {
		Log.info("Game has terminated!");
        Log.info("Final scores:");
		double[] score = eval.getScore(currentState);
        for (int i = 0; i < score.length; i++) {
            Log.info("Player "+i+": "+score[i]);
        }
	}
	
	/**
	 * Run method for a general referee.
	 * At the beginning of the game a InitGameMessage is sent, at the end a GameTerminateMessage is sent. To customize those two messages, overwrite the corresponding methods.
	 */
	@Override
	public void run() {
		Log.info("===================");
		Log.info("Starting "+this.getClass().getSimpleName());
		Log.info("===================");
		
		beforeGameStarts();
		
		// Send init game message to all players before game logic starts. 
		for(int i = 0; i < players.length; i++){
			sendInitGameMessage(createInitGameMessage(players[i]), i);
		}
		
		gameLoop();
		
		// Send init game message to all players before game logic starts. 
		for(int i = 0; i < players.length; i++){
			sendGameTerminatedMessage(createTerminateGameMessage(players[i]), i);
		}
		
		afterGameTerminated();
				
		Log.info("========================");
		Log.info("Shutting down "+this.getClass().getSimpleName());
		Log.info("========================");
	}

	/**
	 * To be overriden by implementing referees
	 */
	protected abstract void gameLoop();
}
