package za.ac.sun.cs.ingenious.games.nim;

import za.ac.sun.cs.ingenious.core.PerfectInformationAlternatingPlayReferee;
import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.model.TurnBasedNimBoard;

/**
 * The referee acts as the "game master". It handles communication with the clients and
 * updates the server's game state.
 */
public class NIMReferee extends PerfectInformationAlternatingPlayReferee<TurnBasedNimBoard, NIMLogic, NIMFinalEvaluator> {

	/**
	 * The required constructor for the RefereeFactory
	 * @param match contains the match settings from the MatchSettingsFile
	 * @param players an array of player representations (either local or TCP)
	 */
	public NIMReferee(MatchSetting match, PlayerRepresentation[] players) {
		super(match, players, new TurnBasedNimBoard(4,0,2), new NIMLogic(), new NIMFinalEvaluator());
	}

}
