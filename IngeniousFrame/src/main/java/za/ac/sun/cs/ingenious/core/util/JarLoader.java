package za.ac.sun.cs.ingenious.core.util;

/**
 * Created by Chris Coetzee on 2016/07/18.
 */

import com.esotericsoftware.minlog.Log;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import za.ac.sun.cs.ingenious.core.exception.InvalidJarException;

/**
 * Provides services for Adapter finding and loading
 *
 * @author Chris Coetzee (chriscz93@gmail.com)
 */
public class JarLoader {
    private static final String EXTENSION_CLASS = ".class";

    private Set<String>    jarPaths;
    private ClassLoader    systemClassLoader;
    private URLClassLoader baseClassLoader;

    public JarLoader() {
        jarPaths = new HashSet<>();
        systemClassLoader = getSystemClassLoader();
        baseClassLoader = new URLClassLoader(new URL[] {}, systemClassLoader);
    }

    /**
     * Attempts to load the class with the given name
     *
     * @param name
     * @return The Class corresponding to the specified name.
     * @throws ClassNotFoundException If the class could not be determined
     */
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        return baseClassLoader.loadClass(name);
    }

    /**
     * Adds all the jars on the given path to the class logginsearch path. Throws a runtime exception
     * if the path does not exist or if the path is not a directory
     *
     * @param path
     * @throws RuntimeException
     */
    public void addJarsOnPath(String path) {
        addJarsOnPathInternal(path);
    }

    /**
     * Adds a single Jar to the logginsearch path.
     * Only throws InvalidJarException
     * if the given path is not a valid JAR file. In the case where the path is a directory no
     * such error will be raised on an invalid JAR file.
     *
     * @param path
     * @throws InvalidJarException
     */
    public void addJarToSearchPath(String path) throws InvalidJarException {
        File file = new File(path);
        if (isValidJar(file)) {
            /* Just adding the file to the loader path anyway */
            String abs = file.getAbsolutePath();
            if (jarPaths.add(abs)) {
                try {
                    addPathToLoader(abs, baseClassLoader);
                    Log.trace(this.getClass().getName(),"Adding to path path " + abs);
                } catch (RuntimeException e) {
                	Log.trace(this.getClass().getName(),"Could not add path to classloader, skipping");
                }
            }
        } else {
            throw new InvalidJarException(String.format("Not a valid jar file %s",
                                                        file.getAbsolutePath()));
        }
    }

    public List<String> findSubClasses(Class<?> cls) {
        return subclassSearchStrategy(cls);
    }

    /* --- Private Internal API ------------------------------------------------------------------*/
    private void addJarsOnPathInternal(String newPath) {
        File file = new File(newPath);
        if (file.isDirectory()) {
            /* apply a filter to all the files */
            File[] files = file.listFiles(new FileFilter() {
                @Override public boolean accept(File pathname) {
                    return isValidJar(pathname);
                }
            });

            /* now add all the jar files */
            for (File jarfile : files) {
                String absolutepath = jarfile.getAbsolutePath();

                if (jarPaths.add(absolutepath)) {
                    addPathToLoader(absolutepath, baseClassLoader);
                    Log.trace(this.getClass().getName(),"Adding to JAR to path" + absolutepath);
                }
            }
        } else {
            throw new RuntimeException(String.format("Expected a directory, got something else: %s",
                                                     newPath));
        }
    }

    /**
     * This logginsearch uses the manifest file to identify classes are to be used
     */
    private List<String> subclassSearchStrategy(Class<?> cls) {
        ArrayList<String> candidateClassnames = new ArrayList<>();
        Log.trace(this.getClass().getName(),"searching jars: " + jarPaths.toString());

        for (String path : jarPaths) {
            File file = new File(path);

            if (!file.exists()) {
                Log.debug(this.getClass().getName(),"File path `" + file.getAbsolutePath() + "` does not exist");
                continue;
            }

            if (file.isFile()) {
                /* Try opening the jar */
                JarFile jarfile = null;
                try {
                    jarfile = new JarFile(file);

                    Enumeration<JarEntry> enumEntries = jarfile.entries();

                    while (enumEntries.hasMoreElements()) {
                        JarEntry e = enumEntries.nextElement();

                        String filename = e.getName();
                        /* we only care about class files */
                        if (filename.endsWith(EXTENSION_CLASS)) {
                            String classname = toClassName(filename);
                            try {
                                Class<?> tmpClass = loadClass(classname);
                                if (cls.isAssignableFrom(tmpClass)) {
                                    candidateClassnames.add(classname);
                                }
                            } catch (Exception exp) {
                                Log.error("Could not test file \"" + filename + "\" for subclassing.");
                                exp.printStackTrace();
                            }

                        }
                    }
                } catch (IOException e) {
                    /* Probably wasn't a JAR-File */
                    Log.trace(this.getClass().getName(),"Couldn't process jar file: " + file.getName(), e);
                    continue;
                } finally {
                    if (jarfile != null) {
                        try {
                            jarfile.close();
                        } catch (IOException e) {
                            Log.trace(this.getClass().getName(),"Couldn't close JAR file");
                        }
                    }
                }
            }
        }
        return candidateClassnames;
    }

    private static boolean isValidJar(File file) {
        JarFile testFile = null;
        try {
            testFile = new JarFile(file);
        } catch (Exception e) {
            return false;
        } finally {
            if (testFile != null) {
                try {
                    testFile.close();
                } catch (IOException e) {
                    /* We already know that its a jar*/
                }
            }
        }
        return true;
    }

    /* --- STATIC HELPERS ------------------------------------------------------------------------*/

    /**
     * Returns the system class loader
     */
    private static ClassLoader getSystemClassLoader() {
        ClassLoader loader = JarLoader.class.getClassLoader();
        return loader;
    }

    /**
     * Adds `path` to the class loader logginsearch path, after it was created.
     * The implementation uses a reflection hack to add the path to the classloader.
     *
     * @param path
     * @param loader
     * @throws RuntimeException
     */
    private static void addPathToLoader(String path, URLClassLoader loader)
            throws RuntimeException {
        try {
            File f = new File(path);
            URI u = f.toURI();
            URLClassLoader urlClassLoader = loader;
            Class<URLClassLoader> urlClass = URLClassLoader.class;
            Method method = urlClass.getDeclaredMethod("addURL", new Class[] { URL.class });
            method.setAccessible(true);
            method.invoke(urlClassLoader, new Object[] { u.toURL() });
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Takes the given file path and converts it to a classname
     *
     * @param path
     * @return String representing the class corresponding to the specified path.
     */
    public static String toClassName(String path) {
        return toClassName(null, path);
    }

    public static String toClassName(String base, String path) {
        if (base != null && path.startsWith(base)) {
            path = base.substring(0, base.length());
            if (path.startsWith("/")) {
                path = path.substring(1);
            }
            return base;
        }

        if (path.endsWith(EXTENSION_CLASS)) {
            path = path.substring(0, path.lastIndexOf(EXTENSION_CLASS));
        }

        path = path.replace('/', '.');

        return path;
    }
}

