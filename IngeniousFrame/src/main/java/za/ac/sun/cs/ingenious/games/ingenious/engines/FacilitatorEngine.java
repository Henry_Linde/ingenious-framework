package za.ac.sun.cs.ingenious.games.ingenious.engines;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousBoard;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousEngine;

public class FacilitatorEngine extends IngeniousEngine {

	private IngeniousBoard gameBoard;
	
	public FacilitatorEngine(String host, int port, MatchSetting match, int position) throws UnknownHostException,
			IOException {
		super(new Socket(host, port), match,position);
		gameBoard = new IngeniousBoard(11,6);
	}

//	public void run() {
//		InputStreamReader ISR = new InputStreamReader(System.in);
//		BufferedReader BR = new BufferedReader(ISR);
//		int counter = 0;
//				
//		while(true){
//			try {
//				String [] msg = messageHandler.receiveMessage();
//				
//				if(msg[0].equals(TCPProtocoll.ID)){
//					messageHandler.reply(""+this.playerId);
//				}else if(msg[0].equals(TCPProtocoll.NAME)){
//					messageHandler.reply("Facilitator_Engine");
//				}else if(msg[0].equals("setrack")){
//					Tile next;
//					for(int i =1;i<msg.length;i= i+2){
//						next = new Tile(Integer.parseInt(msg[i]),Integer.parseInt(msg[i+1]));
//						rack.add(next);
//						System.out.println("TILE ADDED TO RACK : "+next);
//					}
//				}else if(msg[0].equals(TCPProtocoll.GENMOVE)){
//					for(Tile tile : this.rack){
//						System.out.println(tile);
//					}
//					
//					System.out.println("WHAT IS YOUR REPLY?");
//					String[] reply = messageHandler.splitMessage(BR.readLine());
//					
//					
//					Tile tile = new Tile(Integer.parseInt(reply[1]),Integer.parseInt(reply[2]));
//					tile.setRotation(Integer.parseInt(reply[3]),6);
//					Coord coord = new Coord(Integer.parseInt(reply[4]),Integer.parseInt(reply[5]));
//					IngeniousMove move = new IngeniousMove(tile,coord);
//					gameBoard.makeMove(move);
//					rack.remove(tile);
//					System.out.println(gameBoard);
//					messageHandler.reply(reply[1],reply[2],reply[3],reply[4],reply[5]);
//				}else if(msg[0].equals(TCPProtocoll.PLAYMOVE)){
//					System.out.println("playmove received");
//					String[] moveReply = msg;
//					System.out.println(moveReply.length);
//				
//					Tile tile = new Tile(Integer.parseInt(moveReply[1]),
//							Integer.parseInt(moveReply[2]));
//					tile.setRotation(Integer.parseInt(moveReply[3]), 6);
//					Coord coords = new Coord(
//							Integer.parseInt(moveReply[4]),
//							Integer.parseInt(moveReply[5]));
//					IngeniousMove move = new IngeniousMove(tile, coords);
//					gameBoard.makeMove(move);
//					System.out.println(gameBoard);
//				}else if(msg[0].equals("draw")){
//					Tile tile = new Tile(Integer.parseInt(msg[1]),Integer.parseInt(msg[2]));
//					rack.add(tile);
//				}else{
//					System.out.println("WHAT IS YOUR REPLY?");
//					messageHandler.replyBrute(BR.readLine());
//				}
//				
//			} catch (Exception e) {
//				messageHandler.errorReply(""+e.toString());
//				e.printStackTrace();
//			}
//		}
//	}
}
