package za.ac.sun.cs.ingenious.core.network.game.messages;

import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.core.network.Message;

/**
 * Sent by a player to the referee to indicate that they want to execute the contained
 * action
 */
public class PlayActionMessage extends Message {

	private static final long serialVersionUID = 1L;
	
	private Action m;
	
	public PlayActionMessage(Action m) {
		this.m = m;
	}
	
	public Action getAction() {
		return m;
	}

}
