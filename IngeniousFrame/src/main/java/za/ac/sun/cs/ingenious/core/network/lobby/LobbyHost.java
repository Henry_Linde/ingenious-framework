package za.ac.sun.cs.ingenious.core.network.lobby;

import com.esotericsoftware.minlog.Log;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import za.ac.sun.cs.ingenious.core.Referee;
import za.ac.sun.cs.ingenious.core.RefereeFactory;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.network.game.ServerToEngineConnection;
import za.ac.sun.cs.ingenious.core.network.lobby.messages.JoinedLobbyMessage;
import za.ac.sun.cs.ingenious.core.network.lobby.messages.SendNameMessage;

/**
 * Handles all actions specific to a lobby on the server side. Can accept new joining
 * players and will start the game once enough players have joined.
 */
public class LobbyHost {

    protected final    MatchSetting               matchSettings;
    protected volatile int                        numberOfEngines;
    private            int                        maxPlayers;
    protected          ServerToEngineConnection[] engineConns;

    private boolean gameStarted;

    private LobbyManager lobbyManager;

    public LobbyHost(MatchSetting match, LobbyManager lobbyManager) {
        this.matchSettings = match;
        this.numberOfEngines = 0;
        this.lobbyManager = lobbyManager;
        this.maxPlayers = match.getNumPlayers();
        this.engineConns = new ServerToEngineConnection[matchSettings.getNumPlayers()];
        this.gameStarted = false;
    }

    public synchronized int acceptJoiningPlayers(Socket client, ObjectInputStream is,
            ObjectOutputStream os) throws IOException, ClassNotFoundException {
        int id = getFreeID();
        Log.info("Player " + id + " joined lobby, initiating handshake");

        // Perform Handshake with newly connected player
        String playerName = "NO_NAME_RECEIVED";
		os.writeObject(new JoinedLobbyMessage(id));
		playerName = ((SendNameMessage)is.readObject()).getPlayerName();
		Log.info("Player name for id " + id + ": " +playerName);	
        ServerToEngineConnection engine = new ServerToEngineConnection(id, this, client, is, os, playerName);
		addEngine(engine);
        if (numberOfEngines == matchSettings.getNumPlayers()) {
            startGame();
        }
        return id;
    }

    public synchronized void addEngine(ServerToEngineConnection engineConn) {
        if (engineConns[numberOfEngines] == null) {
            engineConns[numberOfEngines] = engineConn;
            numberOfEngines++;
            Log.info(
                    "The number of engines has just been increased to : " + numberOfEngines);
        }
    }

    private int getFreeID() {
        for (int i = 0; i < engineConns.length; i++) {
            if (engineConns[i] == null) {
                return i;
            }
        }
        return -1;
    }

    protected void startGame() {
        Log.info("Starting game");
        if (lobbyManager != null) {
            lobbyManager.unregisterLobby(matchSettings.getLobbyName());
            for (ServerToEngineConnection engine : engineConns) {
            	lobbyManager.removeManager(engine.getPlayerName());
            }
        }
        gameStarted = true;
        Referee controller = RefereeFactory.getInstance().getReferee(matchSettings,
                                                                           engineConns);
        /* FIXME we should keep track of this thread */
        new Thread(controller).start();
    }

    public MatchSetting getMatchSetting() {
        return this.matchSettings;
    }

    /**
     * Called whenever an engine disconnects. Implement this if error handling is required.
     */
    public void engineDisconnected(int id) {
        Log.info("Player " + id + " disconnected.");
        numberOfEngines--;

        if (!isGameStarted()) {

            engineConns[id] = null;
            if (numberOfEngines == 0) {
                lobbyManager.unregisterLobby(matchSettings.getLobbyName());
                Log.info("Lobby " + matchSettings.getLobbyName()
                                           + " unregistered due to no more players");
            }
        }

    }

    public boolean isGameStarted() {
        return gameStarted;
    }

    public int getNumberOfEngines() {
        return numberOfEngines;
    }

    public int getMaxPlayers() {
        return matchSettings.getNumPlayers();
    }

}
