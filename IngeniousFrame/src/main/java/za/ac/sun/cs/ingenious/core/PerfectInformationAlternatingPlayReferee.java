package za.ac.sun.cs.ingenious.core;

import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.model.GameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.model.TurnBasedGameLogic;
import za.ac.sun.cs.ingenious.core.model.TurnBasedGameState;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;

/**
 * DO NOT USE THIS ANYMORE.
 * 
 * Use the FullyObservableMovesReferee, overriding the nextPlayer method with the correct
 * implementation for your game OR just use the one in TurnBasedReferee
 * 
 * TODO This Referee implements a different contract for the distributeAcceptedMove
 * method, where the move always gets distributed to all players. See issue #96
 */
@Deprecated
public abstract class PerfectInformationAlternatingPlayReferee<S extends TurnBasedGameState, L extends TurnBasedGameLogic<S>, E extends GameFinalEvaluator<S>> extends FullyObservableMovesReferee<S,L,E> {

	protected PerfectInformationAlternatingPlayReferee(MatchSetting match, PlayerRepresentation[] players,
			S currentState, L logic, E eval) {
		super(match, players, currentState, logic, eval);
	}

	@Override
	protected int nextPlayer(S currentState, int previousPlayer) {
		return (previousPlayer+1) % matchSettings.getNumPlayers();
	}
	
	@Override
	// TODO Here, moves always get distributed to all players. See issue #96
	protected void distributeAcceptedMove(int playerId, PlayedMoveMessage move) {
		for (int i = 0; i < getMaxPlayers(); i++) {
			players[i].playMove(move);
		}
	}
}
