package za.ac.sun.cs.ingenious.core.model;

import java.util.HashSet;
import java.util.Set;

/**
 * Common operations of any turn based game (games where exactly one player can act in
 * each turn).
 * 
 * @author Michael Krause
 * @param <S> The type of TurnBasedGameState this logic operates on
 */
public interface TurnBasedGameLogic<S extends TurnBasedGameState> extends GameLogic<S> {

	@Override
	public default Set<Integer> getCurrentPlayersToAct(S fromState) {
		Set<Integer> retVal = new HashSet<Integer>();
		retVal.add(fromState.nextMovePlayerID);
		return retVal;
	}

	/**
	 * Set fromState.nextMovePlayerID to the next player to act after the given move has
	 * been applied. Use this e.g. at the end of your makeMove method.
	 * 
	 * The default implementation cycles through players 0,1,2,...,fromState.numPlayers
	 */
	public default void nextTurn(S fromState, Move move) {
		fromState.nextMovePlayerID = (fromState.nextMovePlayerID+1) % fromState.numPlayers;	
		fromState.setCurrPlayer((fromState.nextMovePlayerID+1) % fromState.numPlayers);	
	}
}
