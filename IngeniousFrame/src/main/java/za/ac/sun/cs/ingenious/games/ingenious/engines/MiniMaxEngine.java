package za.ac.sun.cs.ingenious.games.ingenious.engines;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousBoard;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousEngine;
import za.ac.sun.cs.ingenious.games.ingenious.search.minimax.StandardMinimaxMC;

public class MiniMaxEngine extends IngeniousEngine {
	private IngeniousBoard gameBoard;

	public MiniMaxEngine(String host, int port, MatchSetting match, int position)
			throws UnknownHostException, IOException {
		super(new Socket(host, port), match , position);
		gameBoard = new IngeniousBoard(match.getBoardSize(),match.getNumColours());
		this.moveController = new StandardMinimaxMC(2);
	}
	public String engineName(){
		return "Minimax Engine";
	}	
}

