package za.ac.sun.cs.ingenious.core;

/**
 * Various values that are hardcoded into the framework right now
 * 
 * TODO Load these from file or commandline arguments, see issue #218
 * 
 * @author Michael Krause
 */
public final class Constants {
	private Constants() {}
	
	public static final String LogDirectory = "./Logs";
	public static final int DEBUGLEVEL = 0;
	public static final long TURN_LENGTH = 3000;
}