package za.ac.sun.cs.ingenious.search.ismcts;

import za.ac.sun.cs.ingenious.core.model.GameState;
import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.search.InformationSetDeterminizer;
import za.ac.sun.cs.ingenious.search.mcts.MCTS;
import za.ac.sun.cs.ingenious.search.mcts.RandomPolicy;
import za.ac.sun.cs.ingenious.search.mcts.PlayoutPolicy;
import za.ac.sun.cs.ingenious.search.mcts.SearchNode;
import za.ac.sun.cs.ingenious.search.mcts.TreeDescender;
import za.ac.sun.cs.ingenious.search.mcts.TreeUpdater;

public final class SOISMCTS {
	private SOISMCTS(){}
	
	public static <N extends SearchNode<S,N> & DeterminizedSearchNode<S>, S extends GameState> Action generateMove(N root,
			InformationSetDeterminizer<S> det, PlayoutPolicy<N> policy,	TreeDescender<N> descender ,
			TreeUpdater<N> updater, long turnLength) {

		long timeInit = System.currentTimeMillis();
		long endTime = timeInit + turnLength;	
		
		int numberOfPlayouts = 0;
		while(System.currentTimeMillis() < endTime){
			numberOfPlayouts++;
			root.setParentDeterminizedState(det.determinizeUnknownInformation(root.getCurrentPlayer(), root.getGameState()));
			N bestNode = descender.bestSearchMove(root, numberOfPlayouts);
			N newNode = bestNode;
			if(bestNode.expandable()){
				newNode = bestNode.expandAChild(descender.bestExpandMove(bestNode));
			}
			updater.backupValue(newNode, policy.playout(newNode));
		}	
		MCTS.printTreeOverview(root);
		MCTS.printPossibleMoves(root);
		
		Action bestMove = descender.bestPlayMove(root);
		System.out.println("Best move: "+ bestMove.toString());
		
		System.out.println("NUMBER OF PLAYOUTS : "+numberOfPlayouts);
		return bestMove;
	}
}
