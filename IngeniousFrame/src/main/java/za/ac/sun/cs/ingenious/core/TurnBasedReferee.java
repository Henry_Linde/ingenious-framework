package za.ac.sun.cs.ingenious.core;

import com.esotericsoftware.minlog.Log;

import java.util.Set;

import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.model.GameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.model.GameLogic;
import za.ac.sun.cs.ingenious.core.model.GameState;
import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.core.model.TurnBasedGameLogic;
import za.ac.sun.cs.ingenious.core.model.TurnBasedGameState;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.time.Clock;

/**
 * Extend from this class to have a referee that asks each player one after the 
 * other to make a move and distributes this move to all other players.
 * 
 * This referee can be used for: chess, lines of action, TicTacToe, etc.
 * 
 * This referee supports, but doesn't requiere, game specific InitGameMessages and GameTerminateMessages.
 * 
 * It is important to set the numPlayers variable in the match settings file correctly.
 * 
 * @author Stephan Tietz
 * @param <S> type for representing a game state
 * @param <L> type for representing game logic
 */
public abstract class TurnBasedReferee<S extends TurnBasedGameState, L extends TurnBasedGameLogic<S>, E extends GameFinalEvaluator<S>> extends GeneralReferee<S, L, E> {

	protected TurnBasedReferee(MatchSetting match,
			PlayerRepresentation[] players, S currentState, L logic, E eval) {
		super(match, players, currentState, logic, eval);
	}


	/**
	 * Overwrite this method to specify a clock for the player passed as an argument.
	 * The clock specifies how much time a player has before his socket times out.
	 * If not overwritten a clock with infinite timeout will be passed.
	 *
	 * @param player
	 * @return A clock object representing time available to a player.
	 */
	protected Clock getClock(PlayerRepresentation player){
		return new Clock();
	}
	
	
	/**
	 * Overwrite this method if you want to do custom things after a player made his move, e.g. logging the current board state, etc
	 * @param player The player that played this turn
	 * @param action The move that the player made this turn
	 */
	protected void afterPlayersTurn(PlayerRepresentation player, Action action){
		Log.info("Player " + (player.getID()) + " made move " + action);
		Log.info("Game state is now:");
		currentState.printPretty();
		Log.info();		
	}

	/**
	 * Each player is asked in turns to make a move.
	 */
	@Override
	protected void gameLoop() {
		int player = currentState.nextMovePlayerID;
		while(!logic.isTerminal(currentState)){
			int nextPlayer = nextPlayer(currentState, player);
			PlayActionMessage m = players[player].genAction(createGenMoveMessage(players[player])); //ask current player to make a move
			if(m != null && m.getAction() != null) {
				if(logic.validMove(currentState, m.getAction())){ //check if move valid
					logic.makeMove(currentState, m.getAction()); //update server gamestate
					distributeAcceptedMove(player, new PlayedMoveMessage(m)); //then distribute move
					reactToValidMove(player, m);
				}else{
					reactToInvalidMove(player, m);
				}
			}else{
				reactToNullMove(player, m);
			}
			
			afterPlayersTurn(players[player], m != null ? m.getAction() : null);
			
			player = nextPlayer; //change current player
		}
	}


	protected int nextPlayer(S currentState, int previousPlayer) {
		Set<Integer> nextPlayers = logic.getCurrentPlayersToAct(currentState);
		if (nextPlayers.size() != 1) {
			Log.error("TurnBasedReferee only supports games where one player acts at a time");
		}
		return logic.getCurrentPlayersToAct(currentState).iterator().next();
	}

	protected void reactToInvalidMove(int player, PlayActionMessage m) {
		Log.info("Move "+ m.getAction() + " is invalid.");
	}
	
	protected void reactToNullMove(int player, PlayActionMessage m){
		Log.info("Player " +  player + "  didn't send a move in.");
	}
	
	protected void reactToValidMove(int player, PlayActionMessage m){
		
	}

}
