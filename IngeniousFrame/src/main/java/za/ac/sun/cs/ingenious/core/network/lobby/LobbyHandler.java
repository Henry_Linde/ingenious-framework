package za.ac.sun.cs.ingenious.core.network.lobby;

import com.esotericsoftware.minlog.Log;
import com.google.common.base.Preconditions;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.network.TCPProtocoll;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.lobby.messages.JoinMessage;
import za.ac.sun.cs.ingenious.core.network.lobby.messages.JoinedLobbyMessage;
import za.ac.sun.cs.ingenious.core.network.lobby.messages.LobbyOverviewReply;
import za.ac.sun.cs.ingenious.core.network.lobby.messages.LobbyOverviewRequest;
import za.ac.sun.cs.ingenious.core.network.lobby.messages.NewMatchMessage;
import za.ac.sun.cs.ingenious.core.network.lobby.messages.SendNameMessage;

/**
 * Class to handle all lobby stuff on client side.
 * Offers methods to create a new lobby, to getInstance available lobbies or
 * to join into existing lobbies.
 * Communicates with ClientHandler on Serverside.
 *
 * @author Stephan Tietz
 */
public class LobbyHandler {

    private Socket             socket;
    private ObjectInputStream  inputStream;
    private ObjectOutputStream outputStream;
    private String			   playerName;

    /**
     * Creates a new LobbyHandler. Call "connect" after this!
     */
    public LobbyHandler() {
    }

    /**
     * Asks the server to create a new lobby and automatically joins it.
     *
     * @param match Match settings that shall be applied for this lobby. The name of the new lobby is also specified in the match settings.
     * @return returns a connection for the player/enginge that is hosting this match.
     */
    public EngineToServerConnection createGameAndJoin(MatchSetting match) {
        try {
            if (match == null) {
                Log.info("match is null");
            }
            outputStream.writeObject(new NewMatchMessage(match));
            String response = (String) inputStream.readObject();
            if (response.equals(TCPProtocoll.SUCCESS)) {
            } else {
                Log.error("EngineToServerConnection: response=" + response);
            }
            return joinLobby(match.getLobbyName());

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean createGame(MatchSetting match) {
        Preconditions.checkNotNull(match);
        try {
            outputStream.writeObject(new NewMatchMessage(match));
            String response = (String) inputStream.readObject();
            if (!response.equals(TCPProtocoll.SUCCESS)) {
                Log.error("Could not create lobby on server, a lobby with the same name may already exist");
                return false;
            }
            return true;
        } catch (IOException | ClassNotFoundException e) {
            Log.error("Could not create lobby on server", e);
            return false;
        }
    }

    /**
     * Asks the server to send a list containing all available lobbies
     *
     * @return List of available lobbies.
     */
    public LobbyOverviewReply refreshListOfLobbies() {
        try {
            outputStream.writeObject(new LobbyOverviewRequest());
            Log.info("Refresh requested");

            return (LobbyOverviewReply) inputStream.readObject();
        } catch (IOException e) {
            return null;
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

    /**
     * Request the match settings for a specific lobby
     *
     * @param lobbyName name of the lobby that match settings are inquired for
     * @return returns the settings of this lobby
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public MatchSetting requestMatchSetting(String lobbyName) {
        try {
            outputStream.writeObject(TCPProtocoll.MATCHSETTING);
            outputStream.writeObject(lobbyName);
            MatchSetting match = (MatchSetting) inputStream.readObject();
            return match;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Joins into the specified lobby.
     *
     * @param name of the lobby to join into
     * @return returns a handle for the engine to communicate to the server
     */
    public EngineToServerConnection joinLobby(String name) {
    	int playerID = -1;
        try {
            outputStream.writeObject(new JoinMessage(name));
            JoinedLobbyMessage a = (JoinedLobbyMessage)inputStream.readObject();
			playerID = (((JoinedLobbyMessage) a).getPlayerID());
			outputStream.writeObject(new SendNameMessage(this.playerName));
        } catch (IOException | ClassNotFoundException e) {
            Log.error("Error during handshake with server", e);
            return null;
        }

        return new EngineToServerConnection(socket, inputStream, outputStream, playerID);
    }

    public boolean connect(String hostName, int port, String playerName) {
        try {
            Log.info("Connecting to " + hostName + ":" + port);
            socket = new Socket(hostName, port);
            openStreams();
            Log.info("Registering: "+playerName);
            outputStream.writeObject(new String(playerName));

            String ack = (String) inputStream.readObject();

            if (!ack.equals("Acknowledged")) {
                closeConnection();
                Log.error("Player name " + playerName + " already taken");
                return false;
            }
            this.playerName = playerName;
            return true;
        } catch (IOException | ClassNotFoundException e) {
        	Log.error("Error establishing connection with server, closing connection");
            closeConnection();
        	return false;
        }
    }

    private void openStreams() {
        try {
            outputStream = new ObjectOutputStream(this.socket.getOutputStream());
            inputStream = new ObjectInputStream(this.socket.getInputStream());
        } catch (IOException e) {
            Log.info("Input Stream failed to open.");
        }
    }

    /**
     * Disconnects IO-streams and closes socket catches and prints failures
     */
    public void closeConnection() {
        try {
            if (inputStream != null) inputStream.close();
            if (inputStream != null) outputStream.close();
            if (inputStream != null) socket.close();
        } catch (IOException e) {
            Log.info("Socket or Stream failed to close");
        }
    }

}
