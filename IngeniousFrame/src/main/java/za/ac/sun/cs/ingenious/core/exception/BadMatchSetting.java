package za.ac.sun.cs.ingenious.core.exception;

/**
 * Created by Chris Coetzee on 2016/07/18.
 */
public class BadMatchSetting extends Exception {
    public BadMatchSetting(String msg) {
        super(msg);
    }
}
