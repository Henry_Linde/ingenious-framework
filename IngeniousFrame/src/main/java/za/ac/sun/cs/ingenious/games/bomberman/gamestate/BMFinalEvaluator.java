package za.ac.sun.cs.ingenious.games.bomberman.gamestate;

import java.util.LinkedList;
import java.util.List;

import za.ac.sun.cs.ingenious.core.model.GameFinalEvaluator;

/**
 * Class for evaluating terminal states for Bomberman
 */
public class BMFinalEvaluator implements GameFinalEvaluator<BMBoard> {

	@Override
	public double[] getScore(BMBoard forState) {
		double[] winningScore = new double[forState.getPlayers().length];
		int maxScore = Integer.MIN_VALUE;
		List<Integer> winningPlayers = new LinkedList<>();
		for (int i = 0 ; i < forState.getScores().length; i++) {
			if (forState.getScores()[i] > maxScore) {
				winningPlayers.clear();
				winningPlayers.add(i);
				maxScore = forState.getScores()[i];
			} else if (forState.getScores()[i] == maxScore) {
				winningPlayers.add(i);
			}
		}
		for (int i = 0 ; i < forState.getScores().length; i++) {
			if (winningPlayers.contains(i)) {
				winningScore[i] = 1.0/winningPlayers.size();
			} else {
				winningScore[i] = 0;
			}
		}
		return winningScore;
	}
}
