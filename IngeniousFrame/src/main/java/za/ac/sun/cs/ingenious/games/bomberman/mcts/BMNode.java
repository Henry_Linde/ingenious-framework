package za.ac.sun.cs.ingenious.games.bomberman.mcts;

import com.esotericsoftware.minlog.Log;

import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.BMBoard;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.BMLogic;
import za.ac.sun.cs.ingenious.games.bomberman.network.UpdateBombsMessage;
import za.ac.sun.cs.ingenious.search.mcts.SimpleNode;

public class BMNode extends SimpleNode<BMBoard,BMNode> {

    private BMLogic bmlogic;
    private short currentPlayer;

	public BMNode(BMBoard board, short currentPlayer){
		this(board, null, null, currentPlayer);
	}
	
	public BMNode(BMBoard board, Action toThisMove, BMNode parent, short currentPlayer) {
		super(board, BMLogic.defaultBMLogic, toThisMove, parent);
        this.currentPlayer = currentPlayer;
        try {
    		bmlogic = (BMLogic) logic;
        } catch (ClassCastException e) {
            Log.error("Invalid logic used for BMNode, defaulting to default BM logic");
            bmlogic = BMLogic.defaultBMLogic;
        }
	}	
	
	@Override
	public int getCurrentPlayer() {
		return currentPlayer;
	}

	@Override
	public String toString() {
		((BMBoard) currentState).printPretty();
		if (parentToThisMove != null) {
			return depth + " " + parentToThisMove.toString() + "		" + rewards + "/" + visits;
		} else {
			return depth + " null	" + rewards + "/" + visits;
		}
	}

	@Override
	public BMNode expandAChild(Action childMove) {
		BMBoard nextBoard = (BMBoard) ((BMBoard) getGameState()).deepCopy();
		short nextPlayer = (short)((currentPlayer+1)%4);
		if(nextPlayer == 0){
			bmlogic.applyPlayedMoveMessage(new UpdateBombsMessage(-1, false), nextBoard);
		}
		return new BMNode(nextBoard, childMove, this, nextPlayer);
	}
}
