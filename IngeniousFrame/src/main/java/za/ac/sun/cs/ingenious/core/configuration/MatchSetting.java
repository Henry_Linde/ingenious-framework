package za.ac.sun.cs.ingenious.core.configuration;

import com.esotericsoftware.minlog.Log;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import za.ac.sun.cs.ingenious.core.Variant;
import za.ac.sun.cs.ingenious.core.exception.BadMatchSetting;

public class MatchSetting implements Serializable {
    /**
     * Determines if a de-serialized file is compatible with this class.
     */
    private static final long serialVersionUID      = -3834774530442103753L;
    public static final  int  UNSET_POSITIVE_SCALAR = -1;

    private JsonObject data;

    public MatchSetting(JsonObject o) {
        this.data = o;
    }

    public String getLobbyName() {
        if (!data.has("LobbyName")) {
			return "";
		}
        return data.getAsJsonPrimitive("LobbyName").getAsString();
    }

    public int getNumPlayers() {
        if (!data.has("numPlayers")) {
			return UNSET_POSITIVE_SCALAR;
		}
        return data.getAsJsonPrimitive("numPlayers").getAsInt();
    }

    public int getNumColours() {
        if (!data.has("numColours")) {
			return 0;
		}
        return data.getAsJsonPrimitive("numColours").getAsInt();
    }

    public boolean getPerfectInformation() {
        if (!data.has("perfectInformation")) {
			return false;
		}
        return data.getAsJsonPrimitive("perfectInformation").getAsBoolean();
    }

    public boolean getAlternating() {
        if (!data.has("alternatingPlay")) {
			return false;
		}
        return data.getAsJsonPrimitive("alternatingPlay").getAsBoolean();
    }

    public Variant[] getVariants() {
        if (!data.has("variants")) {
			return new Variant[0];
		}
        JsonArray arr = data.get("variants").getAsJsonArray();
        ArrayList<Variant> variants = new ArrayList<>();

        Gson g = new Gson();
        for (JsonElement e : arr) {
            variants.add(g.fromJson(e, Variant.class));
        }

        return variants.toArray(new Variant[0]);
    }

    public int getBoardSize() {
        if (!data.has("boardSize")) {
			return 0;
		}
        return data.getAsJsonPrimitive("boardSize").getAsInt();
    }
    
    public int mnk_getH(){
    	if (!data.has("mnk_height")) {
			return 0;
		}
        return data.getAsJsonPrimitive("mnk_height").getAsInt();
    }
    
    public int mnk_getW(){
    	if (!data.has("mnk_width")) {
			return 0;
		}
        return data.getAsJsonPrimitive("mnk_width").getAsInt();
    }
    
    public int mnk_getK(){
    	if (!data.has("mnk_k")) {
			return 0;
		}
        return data.getAsJsonPrimitive("mnk_k").getAsInt();
    }

    public String[] getEngineNames() {
        if (data.has("engines")) {
            ArrayList<String> names = new ArrayList<>();
            EngineConfig[] engines = getEngines();
            for (EngineConfig e : engines) {
                names.add(e.getName());
            }
            return names.toArray(new String[0]);

        } else if (!data.has("engineNames")) {return new String[0];} else {
            JsonArray arr = data.get("engineNames").getAsJsonArray();
            ArrayList<String> names = new ArrayList<>();

            for (JsonElement e : arr) {
                names.add(e.getAsString());
            }

            return names.toArray(new String[0]);
        }
    }

    public EngineConfig[] getEngines() {
        JsonArray arr;
        if (!data.has("engines")) {
            arr = new JsonArray();
        } else {
            arr = data.get("engines").getAsJsonArray();
        }

        ArrayList<EngineConfig> configs = new ArrayList<>();

        Gson g = new Gson();
        for (JsonElement e : arr) {
            configs.add(g.fromJson(e, EngineConfig.class));
        }

        return configs.toArray(new EngineConfig[0]);
    }

    public ArrayList<Integer> getOpenSlots() {
        JsonArray arr;
        if (!data.has("openSlots")) {
            arr = new JsonArray();
        } else {
            arr = data.get("openSlots").getAsJsonArray();
        }

        ArrayList<Integer> openslots = new ArrayList<>();
        for (JsonElement e : arr) {
            openslots.add(e.getAsInt());
        }
        return openslots;
    }

    public String getGameName() {
        if (!data.has("gameName")) {
			return "";
		}
        return data.get("gameName").getAsString();
    }

    /**
     * TODO ensure we call this everywhere it is constructed
     * Verifies that the loaded match settings are correct
     */

    public void checkConfiguration() throws BadMatchSetting {
        // then give a warning about other options ignored
        if (getEngines().length > 0) {
            if (this.getEngineNames().length > 0) {
                Log.error("[WARN][MatchSettings] `engineNames` attribute will be ignored");
            }
            // Check uniqueness of the engine names
            Map<String, EngineConfig> engineMap = new HashMap<>();
            for (EngineConfig e : getEngines()) {
                if (engineMap.containsKey(e.getClassName())) {
                    throw new BadMatchSetting(String.format("Engine naming conflict: [%s]", e));
                }
            }

        }
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        Gson gson = new Gson();
        String data = gson.toJson(this.data);
        out.writeObject(data);
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        Gson gson = new Gson();
        String data = (String) in.readObject();
        this.data = gson.fromJson(data, JsonElement.class).getAsJsonObject();
    }

}