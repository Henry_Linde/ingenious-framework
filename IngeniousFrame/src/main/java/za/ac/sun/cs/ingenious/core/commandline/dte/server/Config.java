package za.ac.sun.cs.ingenious.core.commandline.dte.server;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;

import za.ac.sun.cs.ingenious.core.GameSystem;
import za.ac.sun.cs.ingenious.core.network.TCPProtocoll;

/**
 * Created by Chris Coetzee on 2016/07/30.
 * <p/>
 * Loads the server configuration located in the same package.
 */
public class Config {
    public static final String SERVER_CONFIG = "dce-server.json";

    private String hostname = "localhost"; // host on which game server will be running
    private int    port     = TCPProtocoll.PORT; // port on which game server wll be running

    private String referee  = "";
    private String gameName = "";

    private JsonElement gameConfig = new JsonObject();

    public JsonElement getGameConfig() {
        return gameConfig;
    }

    public String getHostname() {
        return hostname;
    }

    public int getPort() {
        return port;
    }

    public String getGameName() {
        return gameName;
    }

    public String getReferee() {
        return referee;
    }

    public static Config loadFromConfigFile() throws URISyntaxException, IOException {
        InputStream is = GameSystem.getInstance().openResourceSeek(SERVER_CONFIG);
        return GameSystem.getInstance().getGson().fromJson(new JsonReader(new InputStreamReader(
                is)), Config.class);
    }
}
