package za.ac.sun.cs.ingenious.core;

import java.util.Observable;

import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;

/**
 * Most general representation of a referee. Does not make any assumptions about how a
 * game is played. For that, see the extending classes.
 * 
 * A referee is the entity that plays the game with the players (=engines). It is run by
 * the GameServer and supports receiving {@link PlayActionMessage}s from, as well as
 * sending {@link PlayedMoveMessage}s to its connected clients. It also supports sending
 * {@link InitGameMessage} and {@link GameTerminatedMessage} at the start and end of a
 * game.
 */
public abstract class Referee extends Observable implements Runnable {

	protected final MatchSetting matchSettings;
	protected int numberOfEngines;
	private int maxPlayers;
	protected PlayerRepresentation[] players;

	/**
	 * Creates a new referee, that handles lobby and and game logic.
     * 
     * @param players Array of participants in the game - may not be null
     * @throws IllegalArgumentException if players is null
	 */
	protected Referee(MatchSetting match, PlayerRepresentation[] players){
		this.matchSettings = match;
		this.numberOfEngines = 0;
		if(match != null){
			this.maxPlayers = match.getNumPlayers();
		}
        if (players != null) {
    		this.players = players;
        } else {
            throw new IllegalArgumentException("players argument may not be null");
        }
	}

	protected void sendInitGameMessage(InitGameMessage a, int targetID ){
		players[targetID].initGame(a);
	}

	protected void sendGameTerminatedMessage(GameTerminatedMessage a, int targetID ){
		players[targetID].terminateGame(a);
	}

	/**
	 * Distributes this move to all players except the one specified in playerID
	 * @param playerId If playerID is -1, the move will be sent to all players.
	 * @param move
	 */
	protected void distributeAcceptedMove(int playerId, PlayedMoveMessage move) {
		for (int i = 0; i < maxPlayers; i++) {
            if (i != playerId) {
				players[i].playMove(move);
            }
		}
	}

	public MatchSetting getMatchSetting(){
		return this.matchSettings;
	}

	public int getNumberOfEngines() {
		return numberOfEngines;
	}

	public int getMaxPlayers(){
		return matchSettings.getNumPlayers();
	}

	public PlayerRepresentation[] getEngineConns() {
		return players;
	}

	/**
	 * Implement game control logic here.
	 */
	@Override
	public abstract void run();

}
