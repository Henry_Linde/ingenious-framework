package za.ac.sun.cs.ingenious.search.ismcts;

import java.util.Map;

import za.ac.sun.cs.ingenious.core.model.GameState;
import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.search.mcts.SearchNode;

/**
 * Search Node interface for imperfect information games
 * @author Michael Krause
 */
public interface DeterminizedSearchNode<S extends GameState> {

	/**
	 * Provides availability counts for subset armed bandit algorithms
	 * @return A map containing number of visits to child nodes that are reached via some move from this node
	 */
	public Map<Action, Integer> getVisitsPerChildNodes();
	
	/**
	 * Call this whenever the node is visited with another determinization.
	 * @param determinizedState The parent node's determinized state. This node's determinized state is set to be that state with the move leading to this node applied.
	 */
	public void setParentDeterminizedState(S determinizedState);
	
	/**
	 * @return This node's determinized state.
	 */
	public S getDeterminizedState();
	
}
