package za.ac.sun.cs.ingenious.logging;

import java.io.Closeable;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;

public class Logger extends com.esotericsoftware.minlog.Log.Logger implements Closeable {

	private FileWriter fw;

	public Logger(String directory, String className) {
		super();
		long yourmilliseconds = System.currentTimeMillis();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy-HH:mm");    
		Date resultdate = new Date(yourmilliseconds);
		String dateString = sdf.format(resultdate);
		
		new File(directory).mkdir();
		String fileName = directory + File.separator + dateString +"-"+ className;
		
		int fileCount = 1;
		while ((new File(fileName + "." + fileCount + ".log").isFile()))
			fileCount++;
		fileName += "." + fileCount + ".log";
		
		try {
			fw = new FileWriter(fileName);
		} catch (IOException e) {
			System.err.println("Logger: could not create log file. Will only log to standard out.");
		}
	}
	
	@Override
	protected void print(String message) {
		super.print(message);
		if (fw!=null) {
			try {
				fw.write(message + "\n");
				fw.flush();
			} catch (IOException e) {
				System.err.println("Logger: could not log to file.");
			}
		}
	}

	@Override
	public void close() throws IOException {
		fw.close();
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		fw.close();
	}

}
