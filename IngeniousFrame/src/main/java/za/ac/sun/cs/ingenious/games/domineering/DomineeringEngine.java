package za.ac.sun.cs.ingenious.games.domineering;

import com.esotericsoftware.minlog.Log;

import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.model.TurnBasedSquareBoard;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.games.domineering.game.DomineeringLogic;

public abstract class DomineeringEngine extends Engine {

	protected TurnBasedSquareBoard board;
	protected DomineeringLogic logic;
	
	public DomineeringEngine(EngineToServerConnection toServer) {
		super(toServer);
		this.board = null;
		this.logic = new DomineeringLogic();
	}

	@Override
	public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
		if (board == null) {
			throw new RuntimeException(engineName() + ": board was null when GameTerminatedMessage was received! (Perhaps no InitGameMessage was sent?)");
		}
		Log.info("game terminated.");
		board.printPretty();
		
		toServer.closeConnection();
	}

	@Override
	public void receiveInitGameMessage(InitGameMessage a) {
		this.board = new TurnBasedSquareBoard(((DomineeringInitGameMessage) a).getBoardSize(), 0, 2);
	}

	@Override
	public void receivePlayedMoveMessage(PlayedMoveMessage a) {
		if (board == null) {
			throw new RuntimeException(engineName() + ": board was null when PlayMoveMessage was received! (Perhaps no InitGameMessage was sent?)");
		}
		logic.makeMove(board, a.getMove());
	}

}
