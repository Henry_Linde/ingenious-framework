package za.ac.sun.cs.ingenious.games.mnk;

import za.ac.sun.cs.ingenious.core.model.GameFinalEvaluator;

public class MNKFinalEvaluator implements GameFinalEvaluator<MNKBoard> {

	private int getWinner(MNKBoard forState) {
		return forState.getWinner();
	}

	@Override
	public double[] getScore(MNKBoard forState) {
		double[] scores = new double[2];
		if(getWinner(forState)==-1){
			scores[0] = 0.5;
			scores[1] = 0.5;
		}else{
			scores[getWinner(forState)] = 1;
		}
		return scores;
	}
}
