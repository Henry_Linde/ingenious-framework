package za.ac.sun.cs.ingenious.games.cardGames.uno.game;

import com.esotericsoftware.minlog.Log;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Random;
import java.util.TreeMap;

import za.ac.sun.cs.ingenious.core.PerfectInformationAlternatingPlayReferee;
import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.games.cardGames.core.card.Card;
import za.ac.sun.cs.ingenious.games.cardGames.core.deck.Deck;
import za.ac.sun.cs.ingenious.games.cardGames.core.exceptions.KeyNotFoundException;
import za.ac.sun.cs.ingenious.games.cardGames.core.exceptions.LocationNotFoundException;
import za.ac.sun.cs.ingenious.games.cardGames.core.move.DrawCardMove;
import za.ac.sun.cs.ingenious.games.cardGames.core.rack.CardRack;
import za.ac.sun.cs.ingenious.games.cardGames.uno.cardFeatures.UnoSuits;
import za.ac.sun.cs.ingenious.games.cardGames.uno.cardFeatures.UnoSymbols;

public class UnoReferee extends PerfectInformationAlternatingPlayReferee<UnoGameState, UnoGameLogic, UnoFinalEvaluator> {
	
	public UnoReferee(MatchSetting match, PlayerRepresentation[] players) {
		super(match, players, new UnoGameState(new Deck<UnoSymbols, UnoSuits>(EnumSet.allOf(UnoSymbols.class), EnumSet.allOf(UnoSuits.class), 2),
				match.getNumPlayers()), new UnoGameLogic(), new UnoFinalEvaluator());
	}

	@Override
	protected void beforeGameStarts() {
		Log.info();
	}

	@Override
	protected InitGameMessage createInitGameMessage(PlayerRepresentation player) {
		int numPlayers = currentState.getNumberOfPlayers();
		CardRack<UnoSymbols, UnoSuits> rack = createInitRack(player.getID());
		UnoInitGameMessage iga = new UnoInitGameMessage(rack, player.getID(), numPlayers);
		return iga;
	}

	@Override
	protected void reactToValidMove(int player, PlayActionMessage pma) {
		StringBuilder s = new StringBuilder();
		int outputWidth = 62;
		
		if (pma.getAction() != null) {
		
			s.append("\n+-------------------------------------------------------------+\n");
			
			String tmpString = "|                           Round " + (currentState.getRoundNR()-1);
			s.append(tmpString);
			for(int i=0; i<outputWidth-tmpString.length(); i++){
				s.append(" ");
			}
			s.append("|\n");
			
			s.append("|                                                             |\n");
			
			tmpString = "| Player " + player + ": " + pma.getAction().toString();
			s.append(tmpString);
			for(int i=0; i<outputWidth-tmpString.length(); i++){
				s.append(" ");
			}
			s.append("|\n");
			
			tmpString = "| (New) top: " + currentState.getTop().toString();
			s.append(tmpString);
			for(int i=0; i<outputWidth-tmpString.length(); i++){
				s.append(" ");
			}
			s.append("|\n");
			
			StringBuilder tmpStringBuilder = new StringBuilder();
			int[] rackSizes = currentState.getRackSizes();
			tmpStringBuilder.append("| Rack sizes: [");
			for(int i=0; i<rackSizes.length; i++){
				if(i==0){tmpStringBuilder.append(rackSizes[i]);}
				else{tmpStringBuilder.append(", " + rackSizes[i]);}
			}
			tmpStringBuilder.append("]");
			s.append(tmpStringBuilder.toString());
			for(int i=0; i<outputWidth-tmpStringBuilder.length(); i++){
				s.append(" ");
			}
			s.append("|\n");
			
			s.append("+-------------------------------------------------------------+\n");
			
			Log.info(s);
			
			//Pause for 400 ms after each turn.
			try {
				Thread.sleep(400);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void reactToInvalidMove(int player, PlayActionMessage m) {
		Card<UnoSymbols, UnoSuits> card = null;
		try {
			card = drawFromDrawPile(player);
		} catch (EmptyDrawPileException e) {
			e.printStackTrace();
		}

		DrawCardMove<UnoLocations> dcm = new DrawCardMove<>(player, card, UnoLocations.DRAWPILE,
				UnoLocations.values()[player]);

		logic.makeMove(currentState, dcm);

		PlayActionMessage pma = new PlayActionMessage(dcm);
		players[player].playMove(new PlayedMoveMessage(pma));

		reactToValidMove(players[player].getID(), pma);
	}

	@Override
	protected void reactToNullMove(int player, PlayActionMessage m) {
		reactToInvalidMove(player, m);
	}

	protected CardRack<UnoSymbols, UnoSuits> createInitRack(int player) {
		CardRack<UnoSymbols, UnoSuits> rack = new CardRack<>();

		while (rack.size() < UnoGameState.INITNUMBEROFCARDSONHAND) {
			try {
				Card<UnoSymbols, UnoSuits> card = drawFromDrawPile(player);
				rack.addCard(card);
				currentState.changeLocation(card, UnoLocations.DRAWPILE, UnoLocations.values()[player]);
			} catch (EmptyDrawPileException | KeyNotFoundException | LocationNotFoundException e) {
				e.printStackTrace();
			}
		}


		//Print rack... pretty
		StringBuilder s = new StringBuilder();
		s.append("\n+----------------+\n");
		s.append("|Rack of Player " + player + "|\n");
		s.append("+----------------+\n");
		for (Card<UnoSymbols, UnoSuits> card : rack) {
			String tmpString = "|" + card.toString();
			s.append(tmpString);
			for(int i=0; i<17-tmpString.length(); i++){s.append(" ");}
			s.append("|\n");
		}
		s.append("+----------------+\n\n");
		Log.info(s);
		return rack;
	}

	protected Card<UnoSymbols, UnoSuits> drawFromDrawPile(int player) throws EmptyDrawPileException {
		TreeMap<Card<UnoSymbols, UnoSuits>, ArrayList<UnoLocations>> map = currentState.getMap();
		Random randomizer = new Random();
		Card<UnoSymbols, UnoSuits> card;

		if (currentState.getSizeOfDrawPile() == 0) {
			throw new EmptyDrawPileException("Empty DrawPile!");
		}

		while (true) {
			int rand = randomizer.nextInt(map.size());
			card = (Card<UnoSymbols, UnoSuits>) map.keySet().toArray()[rand];

			if (map.get(card).contains(UnoLocations.DRAWPILE)) {
				return card;
			}
		}
	}
	
	@Override
	protected void afterPlayersTurn(PlayerRepresentation player, Action action){
	}
}