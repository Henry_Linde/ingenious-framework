package za.ac.sun.cs.ingenious.core;

import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.core.model.GameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.model.MoveObserver;
import za.ac.sun.cs.ingenious.core.model.TurnBasedGameLogic;
import za.ac.sun.cs.ingenious.core.model.TurnBasedGameState;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;

/**
 * Abstract super class for referees for any turn based game with partially observable
 * moves. The moves sent in by players are transformed by a {@link MoveObserver} before
 * being sent to all players.
 */
public abstract class PartiallyObservableMovesReferee<S extends TurnBasedGameState, L extends TurnBasedGameLogic<S>, E extends GameFinalEvaluator<S>> extends TurnBasedReferee<S, L, E> {

	protected MoveObserver<S> obs;
	
	protected PartiallyObservableMovesReferee(MatchSetting match, PlayerRepresentation[] players, S currentState,
			L logic, E eval, MoveObserver<S> obs) {
		super(match, players, currentState, logic, eval);
		this.obs = obs;
	}
	
	@Override
	protected void distributeAcceptedMove(int playerId, PlayedMoveMessage move) {
		for (int i = 0; i < getMaxPlayers(); i++) {
			players[i].playMove(new PlayedMoveMessage(obs.fromPointOfView((Action) move.getMove(), currentState, i)));
		}
	}

}
