package za.ac.sun.cs.ingenious.core.configuration;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import za.ac.sun.cs.ingenious.core.Variant;

/**
 * Created by Chris Coetzee on 2016/07/26.
 * <p/>
 * This should be refactored to only have the proper getters and setters
 */
public class MatchBuilder implements Serializable {
    /* All these fields are package private so they can be Accessed from the MatchSettings file */

    private static final Gson DEFAULT_GSON;

    static {
        DEFAULT_GSON = new Gson();
    }

    String LobbyName;
    String gameName;

    /* default value if a field is unset, but required */
    int    numPlayers = MatchSetting.UNSET_POSITIVE_SCALAR;

    int[]              colourRatio;
    int                numColours;
    int                boardSize;
    int 			   mnk_height;
    int 			   mnk_width;
    int 			   mnk_k;

	Variant[]          variants;
    String[]           engineNames;
    boolean            managerPlaying;

    boolean perfectInformation;
    boolean alternatingPlay;

    /* extended fields */ EngineConfig[] engines;

    public MatchBuilder() {}

    /**
     * Creates a new MatchBuilder from a configuration file
     *
     * @param fullpath
     */
    public static MatchBuilder newBuilderFromMatchSettings(String fullpath) throws IOException {
        File file = new File(fullpath);
        byte[] encoded = Files.readAllBytes(Paths.get(fullpath));
        String jsonObject = new String(encoded, StandardCharsets.UTF_8);
        MatchBuilder s = DEFAULT_GSON.fromJson(jsonObject, MatchBuilder.class);
        return s;
    }

    public static MatchBuilder newBuilderFromMatchSettingsString(String matchSetting) {
        MatchBuilder s = DEFAULT_GSON.fromJson(matchSetting, MatchBuilder.class);
        return s;
    }

    public static MatchBuilder newBuilderFromMatchSettingsJson(JsonElement matchSetting) {
        MatchBuilder s = DEFAULT_GSON.fromJson(matchSetting, MatchBuilder.class);
        return s;
    }

    public boolean isPerfectInformation() {
        return perfectInformation;
    }

    public boolean isAlternatingPlay() {
        return alternatingPlay;
    }

    public void setPerfectInformation(boolean perfectInformation) {
        this.perfectInformation = perfectInformation;
    }

    public void setEngines(String[] engines) {
        engineNames = engines;

    }

    public void setEngine(String engine, int index) {
        engineNames[index] = engine;
    }

    public void setPlaying(boolean playing) {
        managerPlaying = playing;
    }

    public void setVariant(Variant var, int index) {
        variants[index] = var;
    }

    /* --- GETTERS -----------------------------------------------------------------------------*/
    public int[] getColourRatio() {
        return colourRatio;
    }

    public void setColourRatio(int[] colourRatio) {
        this.colourRatio = colourRatio;
    }

    public String[] getEngineNames() {
        return engineNames;
    }

    public void setEngineNames(String[] engineNames) {
        this.engineNames = engineNames;
    }

    public String getGameName() {
        return gameName;
    }

    public EngineConfig[] getEngines() {
        return engines;
    }

    public void setEngines(EngineConfig[] engines) {
        this.engines = engines;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getLobbyName() {
        return LobbyName;
    }

    public void setLobbyName(String name) {
        this.LobbyName = name;
    }

    public boolean isManagerPlaying() {
        return managerPlaying;
    }

    public void setManagerPlaying(boolean managerPlaying) {
        this.managerPlaying = managerPlaying;
    }

    public int getNumColours() {
        return numColours;
    }

    public void setNumColours(int numColours) {
        this.numColours = numColours;
    }

    public int getNumPlayers() {
        return numPlayers;
    }

    public void setNumPlayers(int numPlayers) {
        this.numPlayers = numPlayers;
    }

    public Variant[] getVariants() {
        return variants;
    }

    public void setVariants(Variant[] vars) {
        variants = vars;
    }

    public int getBoardSize() {
        return boardSize;
    }

    public int getMnk_height() {
		return mnk_height;
	}

    public int getMnk_width() {
		return mnk_width;
	}

    public int getMnk_k() {
		return mnk_k;
	}

    public void setBoardSize(int boardSize) {
        this.boardSize = boardSize;
    }

    public MatchSetting build() {
        Gson gson = new Gson();
        JsonElement obj = gson.toJsonTree(this);
        MatchSetting match = new MatchSetting(obj.getAsJsonObject());
        return match;
    }

}
