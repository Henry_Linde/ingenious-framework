package za.ac.sun.cs.ingenious.core.network.game.messages;

import za.ac.sun.cs.ingenious.core.network.Message;

/**
 * Super class for all messages that are used to initialize the game. Extend this to
 * supply game specific information (e.g. sending the initial game state, etc).
 * 
 * @author Stephan Tietz
 */
public class InitGameMessage extends Message {

	private static final long serialVersionUID = 1L;

}
