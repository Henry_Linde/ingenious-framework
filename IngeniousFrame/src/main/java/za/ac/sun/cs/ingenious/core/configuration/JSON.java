package za.ac.sun.cs.ingenious.core.configuration;

import com.esotericsoftware.minlog.Log;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import za.ac.sun.cs.ingenious.core.Variant;
import za.ac.sun.cs.ingenious.core.exception.BadMatchSetting;

public class JSON {

    private Gson gson;
    private PrintStream output = null;
    private File file;
    private       String location    = "";
    public static String VARIANT_EXT = ".var";
    public static String MATCH_EXT   = ".mth";

    public JSON(String fileLocation) {
        gson = new Gson();
        location = fileLocation;
    }

    public void writeMatchSettings(MatchSetting match) {
        file = new File(location + match.getLobbyName() + MATCH_EXT);
        try {
            file.createNewFile();
            output = new PrintStream(file);
            output.println(gson.toJson(match));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeVariantSettings(Variant variant) {
        file = new File(location + variant.getName() + VARIANT_EXT);
        Log.info(file.getName());
        try {
            file.createNewFile();
            output = new PrintStream(file);
            output.println(gson.toJson(variant));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Variant readVariantSettings(String name) throws IOException {
        File file = new File(name + VARIANT_EXT);
        byte[] encoded = Files.readAllBytes(Paths.get(name));
        String jsonObject = new String(encoded, StandardCharsets.UTF_8);
        return gson.fromJson(jsonObject, Variant.class);

    }

	public MatchSetting readMatchSettings(String name) throws IOException, BadMatchSetting {
        File file = new File(name);
        byte[] encoded = Files.readAllBytes(Paths.get(name));
        String jsonObject = new String(encoded, StandardCharsets.UTF_8);
        MatchBuilder s = gson.fromJson(jsonObject, MatchBuilder.class);

        MatchSetting settings = s.build();
		settings.checkConfiguration();

        return settings;
    }

}
