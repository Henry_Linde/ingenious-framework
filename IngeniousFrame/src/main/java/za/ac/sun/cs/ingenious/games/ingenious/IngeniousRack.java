package za.ac.sun.cs.ingenious.games.ingenious;

import java.util.ArrayList;
import java.util.Iterator;

import za.ac.sun.cs.ingenious.core.model.Rack;
/**
 * A rack is a list of tiles.
 * 
 * @author steven
 *
 */
public class IngeniousRack extends Rack implements Iterable<Tile> {

	private ArrayList<Tile> tiles = new ArrayList<Tile>();
	
	private final int length;
	
	public Iterator<Tile> iterator(){
		return tiles.iterator();
	}
	
	public IngeniousRack(int length){
		this.length = length;
	}
	
	public int size(){
		return tiles.size();
	}
	public int capacity(){
		return length;
	}
	public IngeniousRack(ArrayList<Tile> rack){
		this.length = rack.size();
		this.setRack(rack);
	}
	
	public IngeniousRack(Tile [] rack){
		this.length = rack.length;
		this.setRack(rack);
	}
	
	public void setRack(ArrayList<Tile> rack){
		if(rack.size()!= this.length){
			throw new IllegalArgumentException("rack lengths do not match");
		}
		for(Tile tile : rack){
			this.add(tile.copy());
		}
	}
	
	public void setRack(Tile[] rack){
		if(rack.length != this.length){
			throw new IllegalArgumentException("rack lengths do not match");
		}
		for(int i = 0; i<rack.length;i++){
			this.add(rack[i]);
		}
	}
	public IngeniousRack copy(){
		ArrayList<Tile> copy = new ArrayList<Tile>();
		for(Tile t : this.tiles ){
			copy.add(t.copy());
		}
		return new IngeniousRack(copy);
	}
	/**
	 * add tile to rack
	 * 
	 * @param addTile
	 * @return true if tile added successfully
	 */
	public boolean add(Tile addTile){
		if(tiles.size()<this.length){
			return tiles.add(addTile);
		}
		return false;
		
	}
	
	public Tile get(int index){
		if(index < 0 || index > this.length){
			throw new IndexOutOfBoundsException();
		}
		return tiles.get(index);		
	}
	
	public Tile get(Tile tile){
		return this.get(tiles.indexOf(tile));
	}
	
	/**
	 * remove tile from rack
	 * 
	 * @param removeTile
	 * @return true if tile removed successfully
	 */
	public boolean remove(Tile removeTile){
		
		return tiles.remove(removeTile);
	}
}
