package za.ac.sun.cs.ingenious.games.bomberman.gamestate;

import com.esotericsoftware.minlog.Log;

import java.awt.Point;

/**
 * Representation of a Bomberman agent
 */
public class Player {
	
	private Point position;
	private boolean alive;
	private short blastRadius = 1;
	private int numBombs = 1;
	private int nMaxBombs = 1;
	private final int id;
	
	public Player(int id) {
		this.id = id;
		alive = true;
	}
	
	protected void setPosition(Point position) {
        // TODO - check position passed is valid - see issue 180
		this.position = position;
	}
	
	protected void setAlive(boolean alive) {
		this.alive = alive;
	}
	
    /** Increases the maximum AND current number of available bombs for the player */
	protected void incMaxBombs(){
		nMaxBombs++;
        incNofBombs();
	}
	
	protected void incBlastRadius(){
		blastRadius++;
	}
	
	protected boolean decNofBombs() {
        if (numBombs > 0) {
            numBombs--;
            return true;
        }
        Log.trace("Attempt to decrease non-positive bomb count for player " + id + ". Ignoring...");
        return false;
	}
	
	protected boolean incNofBombs() {
        if (numBombs < nMaxBombs) {
    		numBombs++;
            return true;
        }
        Log.trace("Attempt to increase bomb count beyond bomb bag size for player" + id + ". Ignoring...");
        return false;
	}
	
	public Point getPosition() {
		return position;
	}
	
	public boolean isAlive() {
		return alive;
	}
	
	public short getBombTimer() {
		return (short) (Math.min(10, nMaxBombs*3+1));
	}
	
	public short getBlastRadius() {
		return blastRadius;
	}
	
	public int getNofBombs() {
		return numBombs;
	}
	
	public int getId() {
		return id;
	}

	public int getMaxNofBombs() {
		return nMaxBombs;
	}

    /** Checks to see if this player and the provided player correspond to the same Bomberman player. */
    public boolean similar(Player p) {
        if (p == null) return false;
        if (id != p.id) return false;
        if (alive != p.alive) return false;
        if (alive) {        
            // all dead players with the same id are similar...
            if (blastRadius != p.blastRadius || numBombs != p.numBombs || nMaxBombs != p.nMaxBombs) return false;
            if (position == null) {
                if (p.position != null) return false;
            } else {    
                if (!position.equals(p.position)) return false;
            }
        }
        return true;
    }

    public String toString() {
        return "Player " + id + " (" + (alive?"alive":"dead") + ") at " + position+ " with " + numBombs + " of " + nMaxBombs + " (blast radius " + blastRadius + ") remaining.";
    }
}
