package za.ac.sun.cs.ingenious.games.shadowtictactoe;

import za.ac.sun.cs.ingenious.core.model.XYAction;

/**
 * A XYAction indicating that the given coordinates are already occupied by
 * another player
 */
public class OccupiedXYAction extends XYAction {

	private static final long serialVersionUID = 1L;

	public OccupiedXYAction(int x, int y, int player) {
		super(x, y, player);
	}

}
