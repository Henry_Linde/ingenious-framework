package za.ac.sun.cs.ingenious.games.ingenious.search.mcts;

import java.util.ArrayList;

import za.ac.sun.cs.ingenious.games.ingenious.IngeniousAction;
import za.ac.sun.cs.ingenious.search.mcts.PlayoutPolicy;
import za.ac.sun.cs.ingenious.search.mcts.SearchNode;

public class DefaultIngeniousPolicy implements PlayoutPolicy{

	public static int playouts = 0;
	public static long timeSpentPlaying = 0;
	
	
	public double[] playout(SearchNode node){
		double[] res = new double[1];
		res[0] = (double)ingeinousPlayout(node, node.getCurrentPlayer());
		return res;
	}
	
	
	public static int ingeinousPlayout(SearchNode node,int player){
		SimpleIngeniousNode sNode = (SimpleIngeniousNode) node;
		
		long startTime = System.currentTimeMillis();
		int originalPlayer = player;

		ArrayList<IngeniousAction> moves = sNode.gameBoard.generateMoves(-1,sNode.rack.get(0));
		int numberOfMoves = 0;
		while(moves.size()>0 && !sNode.gameBoard.full()){
			int index = (int)(Math.random()*moves.size());
			while(!sNode.gameBoard.validMove(moves.get(index))){
				index = (int)(Math.random()*moves.size());
			}
			sNode.gameBoard.makeMove(moves.get(index));
			if(player == 1){
				player = 2;
			}else{
				player =1;
			}
			moves.remove(index);
			numberOfMoves++;
		}
	//	System.out.println("NUMBER_OF_MOVES MADE IN PLAYOUT : "+numberOfMoves);
	//	System.out.println("NUMBER_OF_MOVES MADE ON board : "+sNode.gameBoard.getMoveHistory().size());
		
		for(int i = 0; i<numberOfMoves;i++){
			sNode.gameBoard.undoMove();
		}
		playouts++;
		int result = 0;
		long endTime = System.currentTimeMillis();
		timeSpentPlaying += (endTime -startTime);
		return result;
	}
	
}
