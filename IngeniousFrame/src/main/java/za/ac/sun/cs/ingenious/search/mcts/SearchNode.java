package za.ac.sun.cs.ingenious.search.mcts;

import java.util.List;

import za.ac.sun.cs.ingenious.core.model.GameState;
import za.ac.sun.cs.ingenious.core.model.Action;

/**
 * Describes nodes in the search tree that is built during MCTS
 * 
 * @author steven
 * @author Michael Krause
 * 
 * @param <S> The type of the GameStates that the nodes in the search tree represent
 */
public interface SearchNode<S extends GameState, N extends SearchNode<S,N>> {

	/**
	 * @return The already expanded children of this node.
	 */
	public List<N> getExpandedChildren();
	
	/**
	 * @return List of moves that do not have a corresponding child in the tree yet.
	 */
	public List<Action> getMovesToExpand();
	
	/**
	 * @return The move that lead to this node.
	 */
	public Action getMove();

	/**
	 * @return The game state / information set that this node represents.
	 */
	public S getGameState();

	/**
	 * @return The amount of times that this node has been visited.
	 */
	public int getVisits();

	/**
	 * @return The total reward scored by choosing this node per player.
	 */
	public double[] getReward();

	/**
	 * @return True if this node can be expanded.
	 */
	public boolean expandable();

	/**
	 * Expand a child node for the given move. For MCTS, start a playout after calling expandAChild.
	 * @param action Move leading to the child node to be generated.
	 * @return The child node obtained by applying move to the current node.
	 */
	public N expandAChild(Action action);

	/**
	 * @return The parent of this node in the search tree. 'null' indicates that this is the root node
	 */
	public N getParent();

	/**
	 * Increases the reward per player by the corresponding entry in d and the number of visits by 1.
	 * @param d Array of values by which to increase the reward of the players.
	 */
	public void addPlayout(double[] d);

	/**
	 * @return The player that is going to make a move in this node.
	 */
	public int getCurrentPlayer();

	/**
	 * @return Depth of this node in the search tree. 0 is the depth of the tree root.
	 */
	public int getDepth();
	
}
