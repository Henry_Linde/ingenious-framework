package za.ac.sun.cs.ingenious.games.cardGames.core.deck;

import com.esotericsoftware.minlog.Log;

import java.util.ArrayList;

import za.ac.sun.cs.ingenious.games.cardGames.core.card.Card;
import za.ac.sun.cs.ingenious.games.cardGames.core.card.CardFeature;

/**
 * The Class Deck.
 *
 * @param <F1> Generic type of first feature (implements CardFeature).
 * @param <F2> Generic type of second feature (implements CardFeature).
 */
public class Deck<F1 extends CardFeature, F2 extends CardFeature>{
	
	/** The deck size. */
	private int deckSize;
	
	/** The cards. */
	private ArrayList<Card<F1, F2>> cards;

	/**
	 * Instantiates a new deck.
	 *
	 * @param feature1 the feature 1
	 * @param feature2 the feature 2
	 * @param nrOfAppearancesPerCard the nr of appearances per card
	 */
	public Deck(Iterable<F1> feature1, Iterable<F2> feature2, int nrOfAppearancesPerCard) {

		initDeck(nrOfAppearancesPerCard, feature1, feature2);
	}

	/**
	 * Instantiates a new deck.
	 *
	 * @param feature1 the feature 1
	 * @param feature2 the feature 2
	 */
	public Deck(Iterable<F1> feature1, Iterable<F2> feature2) {
		this(feature1, feature2, 1);
	}

	/**
	 * Inits the deck.
	 *
	 * @param nrOfAppearancesPerCard the nr of appearances per card
	 * @param feature1 the feature 1
	 * @param feature2 the feature 2
	 */
	private void initDeck(int nrOfAppearancesPerCard, Iterable<F1> feature1, Iterable<F2> feature2) {
		cards = new ArrayList<>();

		for (F1 f1 : feature1) {
			for (F2 f2 : feature2) {
				Card<F1, F2> newCard = new Card<F1, F2>(f1, f2);

				// Create x actual cards of one type, where x is nrOfAppearancesPerCard.
				for (int i = 0; i < nrOfAppearancesPerCard; i++) {
					cards.add(newCard);
				}
			}
		}
	}

	/**
	 * Prints the deck.
	 */
	public void printDeck() {
		int i=1;
		for (Card<F1, F2> card: cards) {
			Log.info("Card nr " + (i++) + ": " + card.toString());
		}
	}
	
	/**
	 * Gets the all cards.
	 *
	 * @return the all cards
	 */
	public ArrayList<Card<F1, F2>> getAllCards(){
		//Not a deep copy.
		return (ArrayList<Card<F1, F2>>) cards.clone();
	}

	/**
	 * Gets the size.
	 *
	 * @return the size
	 */
	public int getSize() {
		return deckSize;
	}
}
