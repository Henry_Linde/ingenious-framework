package za.ac.sun.cs.ingenious.core.commandline.gamecreator;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.esotericsoftware.minlog.Log;

import za.ac.sun.cs.ingenious.core.commandline.BaseArguments;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.network.TCPProtocoll;

/**
 * Created by Chris Coetzee on 2016/07/27.
 */
@Parameters(commandDescription = "Creates a new game on some game server")
public class GameCreatorArguments extends BaseArguments {
    static final String FLAG_PLAYERS = "-players";

    @Parameter(names = "-port", description = "GameServer port")
    private int serverPort = TCPProtocoll.PORT;

    @Parameter(names = "-hostname", description = "GameServer hostname")
    private String serverHostname = "localhost";

	@Parameter(names = "-game", description = "Name of the referee of the game to host. All referees are automatically registered with their class names. You can also supply the -C command to the server script to register a referee with any name you like.", required = true)
    private String gameName = null;

    @Parameter(names = "-lobby", description = "Name of the lobby to create", required = true)
    private String lobbyName = null;

    @Parameter(names = FLAG_PLAYERS, description = "Number of players allowed in this lobby")
    private int playerCount = MatchSetting.UNSET_POSITIVE_SCALAR;

    @Parameter(names = "-config", description = "Path to a file containing game specific configuration formatted as JSON", required = true)
    private String gameconfigPath;

    public String getGameconfigPath() {
        return gameconfigPath;
    }

    public String getServerHostname() {
        return serverHostname;
    }

    public int getServerPort() {
        return serverPort;
    }

    public String getGameName() {
        return gameName;
    }

    public String getLobbyName() {
        return lobbyName;
    }

    public int getPlayerCount() {
        return playerCount;
    }

    @Override public void validateAndPrepareArguments(JCommander commander) {

        if (playerCount != MatchSetting.UNSET_POSITIVE_SCALAR && playerCount <= 0) {
            Log.error("Players must be > 0");
            throw new IllegalArgumentException("players must be > 0");
        }
    }
}
