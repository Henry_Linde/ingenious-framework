package za.ac.sun.cs.ingenious.search.mcts;

import java.util.List;

import za.ac.sun.cs.ingenious.core.model.GameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.model.GameLogic;
import za.ac.sun.cs.ingenious.core.model.GameState;
import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.core.model.TurnBasedGameState;

/**
 * Standard implementation of a PlayoutPolicy that just plays the game
 * by letting each player pick a random action until a terminal state is reached.
 * No domain knowledge needed
 * @author steve
 * @author Michael Krause
 * @param <S> The type of TurnBasedGameState to operate the playout on
 */
public class RandomPolicy<S extends TurnBasedGameState, N extends SearchNode<S,N>> implements PlayoutPolicy<N> {

	private GameLogic<S> logic;
	private GameFinalEvaluator<S> eval;
	private boolean normalize;
	
	/**
	 * @param logic The game logic to play the game with
	 * @param eval An evaluator for terminal states of the game
	 * @param normalizeScores True, if the scores returned by eval are not normalized, i.e: each score is between 0 and 1 where 0 denotes failure and 1 complete success
	 */
	public RandomPolicy(GameLogic<S> logic, GameFinalEvaluator<S> eval, boolean normalizeScores) {
		this.logic = logic;
		this.eval = eval;
		this.normalize = normalizeScores;
	}
	
	@Override
	public double[] playout(N node) {
		S state = (S) node.getGameState().deepCopy();
		
		while(!logic.isTerminal(state)){
			
			List<Action> actions = logic.generateActions(state, state.nextMovePlayerID); //generate possible moves
			
			if(!actions.isEmpty()){ //if any moves are possible
				logic.makeMove(state, actions.get((int)(Math.random()*(double)actions.size()))); //pick a random one 
			}
		}
		
		double[] scores = eval.getScore(state);
		if (normalize) {
			double max = Double.NEGATIVE_INFINITY;
			for (double d : scores) {
				if (d>max)
					max = d;
			}
			for (int i=0; i<scores.length; i++) {
				scores[i] = scores[i]/max;
			}
		}
		return scores;
	}

}
