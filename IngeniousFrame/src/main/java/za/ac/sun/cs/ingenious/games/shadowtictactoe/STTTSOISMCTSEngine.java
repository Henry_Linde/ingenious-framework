package za.ac.sun.cs.ingenious.games.shadowtictactoe;

import za.ac.sun.cs.ingenious.core.Constants;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.search.ismcts.SOISMCTS;
import za.ac.sun.cs.ingenious.search.ismcts.SOISMCTSNode;
import za.ac.sun.cs.ingenious.search.ismcts.SOISMCTSUCTDescender;
import za.ac.sun.cs.ingenious.search.mcts.RandomPolicy;
import za.ac.sun.cs.ingenious.search.mcts.SimpleUpdater;

public class STTTSOISMCTSEngine extends STTTEngine {
	
	public STTTSOISMCTSEngine(EngineToServerConnection toServer) {
		super(toServer);
	}
	
	/**
	 * Never use spaces in engine name!
	 */
	@Override
	public String engineName() {
		return "STTTSOISMCTSEngine";		
	}
	
	/**
	 * This method is called when the server requests a move by this engine. 
	 * Don't forget to update your own gamestate(board) if you make a move, receivePlayMove is only called for other players.
	 */
	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
		SOISMCTSNode<STTTGameState> root = new SOISMCTSNode<STTTGameState>(currentState,
				logic.determinizeUnknownInformation(playerID, currentState), logic, null, null);
		return new PlayActionMessage(SOISMCTS.generateMove(root, logic,
				new RandomPolicy<STTTGameState,SOISMCTSNode<STTTGameState>>(logic, new STTTFinalEvaluator(), false),
				new SOISMCTSUCTDescender<STTTGameState,SOISMCTSNode<STTTGameState>>(logic),
				new SimpleUpdater<SOISMCTSNode<STTTGameState>>(),
				Constants.TURN_LENGTH));
	}

}
