package za.ac.sun.cs.ingenious.games.cardGames.uno.game;

public class EmptyDrawPileException extends RuntimeException{

	private static final long serialVersionUID = 327482937428L;
	
	public EmptyDrawPileException(String message){
		super(message);
	}
	
}
