package za.ac.sun.cs.ingenious.games.bomberman.gamestate.moves;

import za.ac.sun.cs.ingenious.core.model.Action;

public class TriggerBombAction implements Action{

	private static final long serialVersionUID = 1L;

	int playerID;
	
	public TriggerBombAction(int id) {

		this.playerID = id;
	}
	
	public String toString(){
		return "trigger by "+playerID;
	}
	
	public int getPlayerID() {
		return playerID;
	}
}
