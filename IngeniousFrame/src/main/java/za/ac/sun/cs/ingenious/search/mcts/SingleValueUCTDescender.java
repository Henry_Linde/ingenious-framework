package za.ac.sun.cs.ingenious.search.mcts;

import za.ac.sun.cs.ingenious.core.model.GameState;

/**
 * This Descender is suitable for two player zero sum games with alternating moves
 * Instead of two score values for the players, only one score is stored per node
 * So we only have to look at that one score.
 * This should be used together with @link {@link NegamaxUpdater}
 * @author Michael Krause
 */
public class SingleValueUCTDescender<N extends SearchNode<? extends GameState,N>> extends UCTDescender<N> {

	@Override
	public double searchValue(N node, int forPlayerID, int totalNofPlayouts) {
		return super.searchValue(node, 0, totalNofPlayouts);
	}
	
}
