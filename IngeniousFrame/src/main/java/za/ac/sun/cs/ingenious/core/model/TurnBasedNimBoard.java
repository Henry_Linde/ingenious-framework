package za.ac.sun.cs.ingenious.core.model;

import com.esotericsoftware.minlog.Log;

/**
 * Represents the state of a game that is played on a nim board.
 * 
 * @author Henry Linde
 */
public class TurnBasedNimBoard extends TurnBasedGameState {

	public byte[] board;
	private int boardSize;
	private int currPlayer;
	private int pieces;


	/**
	 * Initializes a board.
	 */
	public TurnBasedNimBoard(int boardSize, int firstPlayer, int numPlayers) {
		super(firstPlayer, numPlayers);
		this.boardSize = boardSize;
		this.currPlayer = firstPlayer;
		board = new byte[this.boardSize];
		byte val = 1;
		int sum = 0;
		for (int i = 0; i < boardSize; i++) {
			board[i] = (byte)val;
			sum += val;
			val += (byte)2;

		}
		this.pieces = sum;

	}
	
	/**
	 * Copy constructor. Duplicates the given board.
	 */
	public TurnBasedNimBoard(TurnBasedNimBoard toCopy) {
		super(toCopy.nextMovePlayerID, toCopy.numPlayers);
		this.boardSize = toCopy.getBoardSize();
		this.board = toCopy.board.clone();
		this.pieces = toCopy.pieces;
		this.currPlayer = toCopy.currPlayer;
	}

	/**
	 * @return number of matches on the board
	 */
	public int getPieces() {
		return pieces;
	}

	/**
	 * @void decrements the number of matches on the board
	 */
	public void setPieces(int x) {
		pieces -= x;
	}

	/**
	 * @return the current player to remove matches
	 */
	public int getCurrPlayer() {
		return currPlayer;
	}

	/**
	 * @return the current player to remove matches
	 */
	public void setCurrPlayer(int a) {
		this.currPlayer = a;
	}

	/**
	 * @return The size of the board. i.e. Number of rows.
	 */
	public int getBoardSize() {
		return boardSize;
	}
	
	@Override
	public GameState deepCopy() {
		return new TurnBasedNimBoard(this);
	}
	
	/**
	* Prints the board in a pretty fashion
	*/
	@Override
	public void printPretty(){
		StringBuilder s = new StringBuilder();
		s.append("\n");
		s.append("+");
		for (short i =0; i < 7; i++){
			s.append("-");
		}
		s.append("+");
		s.append("\n");

		for (short y =0; y < boardSize; y++){
			s.append("|");
			for (short x =0; x < board[y]; x++){
				s.append("x");
			}
			s.append("|");
			s.append("\n");
		}
		
		s.append("+");
		for (short i =0; i < 7; i++){
			s.append("-");
		}
		s.append("+");
		Log.info(s.toString());
	}
	
}
