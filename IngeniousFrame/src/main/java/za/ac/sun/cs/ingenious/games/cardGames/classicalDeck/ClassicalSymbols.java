package za.ac.sun.cs.ingenious.games.cardGames.classicalDeck;

import za.ac.sun.cs.ingenious.games.cardGames.core.card.CardFeature;

/**
 * The enum ClassicalSymbols.
 */
public enum ClassicalSymbols implements CardFeature{

	/** Classical symbols. 
	 */
	TWO(2),
	THREE(3),
	FOUR(4),
	FIVE(5),
	SIX(6),
	SEVEN(7),
	EIGHT(8),
	NINE(9),
	TEN(10),
	JACK(11),
	QUEEN(12),
	KING(13),
	AS(14);

	/** Value for hierarchical ordering. */
	int value;
	
	
	/**
	 * Constructor for symbols.
	 *
	 * @param Value of symbol.
	 */
	ClassicalSymbols(int value){
		this.value = value;
	}
	

	/**
	 * @see za.ac.sun.cs.ingenious.games.cardGames.core.card.CardFeature#getValue()
	 */
	@Override
	public int getValue(){
		return this.value;
	}

	/**
	 * @see za.ac.sun.cs.ingenious.games.cardGames.core.card.CardFeature#setValue(int)
	 */
	@Override
	public void setValue(int value){
		this.value = value;
	}
}
