# Card

#### The most basic unit of any card game

A Card simply consists of **any two objects** (features) which implement the **CardFeature interface**.

*Why exactly two features?* - Nearly all card types only have two features (e. g. symbol and suit).

If more than two features are required consider using a collection as one of the two features.

If only one feature is needed use '?' as second generic type.



#CardFeature Interface

All card features need a value in order to make hierarchical distinctions (Comparable).

If a game doesn't have a hierarchy regarding it's cards just give it any value. 
It will only have an impact when printing the game state since a TreeMap is used (which relies the Comparable interface) for internal representation.

For most card types using an enum (see uno.cardFeatures) is very convenient.

**Please note:** when comparing two cards the first feature is more dominant than the second feature.