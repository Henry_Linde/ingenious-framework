package za.ac.sun.cs.ingenious.core.exception;

import za.ac.sun.cs.ingenious.core.configuration.EngineConfig;

/**
 * Exception that is raised when an engine could not be located.
 * Created by Chris Coetzee on 2016/07/18.
 */
public class EngineNotFound extends EngineConfigurationError {

    public EngineNotFound(String msg) {
        super(msg);
    }

    public EngineNotFound(EngineConfig e) {
        super(String.format("Could not find engine: %s", e));
        setEngine(e);
    }

}
