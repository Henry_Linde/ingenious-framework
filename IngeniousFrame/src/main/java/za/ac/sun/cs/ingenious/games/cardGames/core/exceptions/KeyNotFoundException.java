package za.ac.sun.cs.ingenious.games.cardGames.core.exceptions;

/**
 * The KeyNotFoundException.
 * 
 * Is raised when a key in could not be found.
 */
public class KeyNotFoundException extends Exception{

	/** The serialVersionUID. */
	private static final long serialVersionUID = 327482937428L;
	
	/**
	 * Instantiates a newKeyNotFoundException.
	 *
	 * @param message Error description for super class.
	 */
	public KeyNotFoundException(String message){
		super(message);
	}
	
}
