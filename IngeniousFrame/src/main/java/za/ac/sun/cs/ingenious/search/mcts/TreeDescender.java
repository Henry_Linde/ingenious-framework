package za.ac.sun.cs.ingenious.search.mcts;

import za.ac.sun.cs.ingenious.core.model.GameState;
import za.ac.sun.cs.ingenious.core.model.Action;

/**
 * Describes how the search tree is traversed during MCTS when the best node to 
 * expand is looked for.
 * @author steve
 */
public interface TreeDescender<N extends SearchNode<? extends GameState,N>> {
	/**
	 * Looking for the best move to play according to the current search state in root
	 * This should be called after several rounds of MCTS have already been run
	 * @param root Node from which to play a move
	 * @return Best move to play from root or null if there are no expanded children
	 */
	public Action bestPlayMove(N root);
	/**
	 * Looking for the best node to expand.
	 * @param root Node from where to search
	 * @param totalNofPlayouts Number of playouts that have been run before this call
	 * @return The best node to expand according to the current search state from root
	 */
	public N bestSearchMove(N root, int totalNofPlayouts);

	/**
	 * Choose a move to expand (run this after bestSearchMove)
	 * @param parent Node to expand
	 * @return Move from parent to expand
	 */
	public Action bestExpandMove(N parent);
}