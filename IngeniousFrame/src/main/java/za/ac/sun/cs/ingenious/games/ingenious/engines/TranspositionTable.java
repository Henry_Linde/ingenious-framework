package za.ac.sun.cs.ingenious.games.ingenious.engines;

import java.util.ArrayList;
import java.util.HashMap;

public class TranspositionTable {
	private final int M;

	private final int hashLength;
	HashMap<Integer, ArrayList<Transposition>> table = new HashMap<Integer, ArrayList<Transposition>>();

	public TranspositionTable(int m, int hashLength) {
		M = m;
		this.hashLength = hashLength;
		initArrays();
	}

	private void initArrays() {
		for (int i = 0; i < Math.pow(2, hashLength); i++) {
			table.put(i, new ArrayList<Transposition>());
		}
	}

	public Transposition getTran(long key) {
		for (Transposition t : table.get(getHash(key))) {
			if (key == t.key) {
				return t;
			}
		}
		return null;
	}

	public void addTranposition(Transposition tran) {
		int hashKey = getHash(tran.key);
		//table.getInstance(hashKey).clear();
		if(table.get(hashKey).size()==0){
			table.get(hashKey).add(tran);
		}else{
			Transposition t = table.get(hashKey).get(0);
			if(t.key != tran.key){
				table.get(hashKey).remove(0);
				table.get(hashKey).add(tran);
			}else{
				if (t.flag == Transposition.EXACTVALUE){
					
				} else if (tran.flag == Transposition.EXACTVALUE && t.flag != Transposition.EXACTVALUE){
					table.get(hashKey).remove(0);
					table.get(hashKey).add(tran);
				} else if(tran.flag == Transposition.LOWERBOUND){
					if(tran.value > t.value){
						table.get(hashKey).remove(0);
						table.get(hashKey).add(tran);
					}
				} else{
					if(tran.value<t.value){
						table.get(hashKey).remove(0);
						table.get(hashKey).add(tran);
					}
				}
			}
		}
		

	}

	public String toString() {
		String str = "";
		for (ArrayList<Transposition> t : table.values()) {
			str += t.size() + "\n";
		}
		return str;
	}

	public int getHash(long key) {
		long hashKey = key & ((1 << hashLength) - 1);

		return (int) hashKey;
	}

	public static void main(String[] args) {
		TranspositionTable tranny = new TranspositionTable(5, 8);

		System.out.println(tranny.getHash(22244000000000l));
	}
}
