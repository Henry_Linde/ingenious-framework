package za.ac.sun.cs.ingenious.games.bomberman.engines;

import java.util.List;

import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.games.bomberman.BMEngine;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.BMBoard;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.moves.IdleAction;
import za.ac.sun.cs.ingenious.games.bomberman.network.BMGenActionMessage;
import za.ac.sun.cs.ingenious.games.bomberman.network.UpdateBombsMessage;

/**
 * Bomberman engine that creates a save move based on depth-first logginsearch.
 * Will not try to score or kill other players, just trying to survive for as long as possible.
 */
public class BMEngineBFS extends BMEngine {

	public BMEngineBFS(EngineToServerConnection toServer) {
		super(toServer);
	}

	/**
	 * DO NOT USE SPACES IN ENGINE NAME
	 */
	@Override
	public String engineName() {
		return "BMEngineBFS";
	}
	
	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
		BMGenActionMessage bmGenMove = (BMGenActionMessage) a; 
		return new PlayActionMessage(createSmarterMove(bmGenMove.getPlayerID(), bmGenMove.getRound()));
	}
	
	private Action createSmarterMove(int id, int round){
		System.out.println("=============== Initial planning state:");
		board.printPretty();
		Action action = recursiveCreateSmarterMove(id, 0, board, round);
		if (action != null) {
			System.out.println(0 + ": "+ action.toString());
		}
		return action;
	}
	
	private Action recursiveCreateSmarterMove(int id, int depth, BMBoard board, int round){
        // TODO (see issue 153): This code (A) is a depth-limited DFS; and (B) does not take opponent moves into account.
        // Should be replaced by a generic working BFS, or this engine should be renamed.
		System.out.println("Round:" + round +", Player " + playerID + " recursiveCreateSmarterMove, depth " + depth);
		List<Action> actions = logic.generateActions(board, id);
		System.out.println("	"+actions.size()+" possible moves generated");
		for (Action m : actions){
			System.out.println("		"+m.toString());
		}
		while (!actions.isEmpty()) {
			Action action = actions.remove((int)(Math.random()*actions.size()));
			BMBoard testBoard = (BMBoard) board.deepCopy();
			if (depth > 0) {
				logic.applyPlayedMoveMessage(new UpdateBombsMessage(-1, false), testBoard);
            }
			logic.makeMove(testBoard, action);
			if(testBoard.getPlayer(id).isAlive()) {
				if (depth == 4) {
					System.out.println("Round:" + round +", Player " + playerID + " planning: ");
					return action;
				} else {
					Action nextMove = recursiveCreateSmarterMove(id, depth+1, testBoard, round);
					if (nextMove != null) {
						System.out.println((depth+1) + ": "+ nextMove.toString()  );
						return action;
					}
				}
			} else {
				System.out.println(action.toString()+" leads to death");
			}
		}
		System.out.println("	"+" no possible move found, defaulting to idle move");
		return new IdleAction();
	}
}
