package za.ac.sun.cs.ingenious.core.exception;

/**
 * Created by Chris Coetzee on 2016/07/26.
 */
public class InvalidJarException extends Exception {

    public InvalidJarException(String msg) {
        super(msg);
    }
}
