package za.ac.sun.cs.ingenious.games.domineering.game;

import za.ac.sun.cs.ingenious.core.model.XYAction;


/**
 * An action in the Domineering game The x,y coordinates of a DomineeringAction
 * always denote the upper left tile of the domino to be played Player 1 plays
 * vertically (i.e: occupies (x,y) and (x,y+1)) and player 2 plays horizontally
 * (i.e: occupies (x,y) and (x+1,y))
 * 
 * @author Michael Krause
 */
public class DomineeringAction extends XYAction {
	
	private static final long serialVersionUID = 1L;
	
	public DomineeringAction(int x, int y, int player) {
		super(x,y,player);
	}

	public int getX1() {
		return getX();
	}
	public int getY1() {
		return getY();
	}
	public int getX2() {
		if (getPlayer() == 0) {
			return getX();
		} else {
			return (getX()+1);
		}
	}
	public int getY2() {
		if (getPlayer() == 0) {
			return (getY()+1);
		} else {
			return getY();
		}
	}
}
