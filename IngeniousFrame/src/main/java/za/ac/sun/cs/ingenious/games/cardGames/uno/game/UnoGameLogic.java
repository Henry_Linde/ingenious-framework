package za.ac.sun.cs.ingenious.games.cardGames.uno.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import za.ac.sun.cs.ingenious.core.model.GameLogic;
import za.ac.sun.cs.ingenious.core.model.Move;
import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.core.model.TurnBasedGameLogic;
import za.ac.sun.cs.ingenious.games.cardGames.core.card.Card;
import za.ac.sun.cs.ingenious.games.cardGames.core.exceptions.KeyNotFoundException;
import za.ac.sun.cs.ingenious.games.cardGames.core.exceptions.LocationNotFoundException;
import za.ac.sun.cs.ingenious.games.cardGames.core.move.DrawCardMove;
import za.ac.sun.cs.ingenious.games.cardGames.core.move.PlayCardMove;
import za.ac.sun.cs.ingenious.games.cardGames.uno.cardFeatures.UnoSuits;
import za.ac.sun.cs.ingenious.games.cardGames.uno.cardFeatures.UnoSymbols;

public class UnoGameLogic implements TurnBasedGameLogic<UnoGameState> {

	@Override
	public List<Action> generateActions(UnoGameState fromState, int forPlayerID) {
		ArrayList<PlayCardMove<UnoLocations>> moves = new ArrayList<>();
		UnoLocations playerLocation = UnoLocations.values()[forPlayerID];
		TreeMap<Card<UnoSymbols, UnoSuits>, ArrayList<UnoLocations>> map = fromState.getMap();

		for (Map.Entry<Card<UnoSymbols, UnoSuits>, ArrayList<UnoLocations>> entry : map.entrySet()) {
			Card<UnoSymbols, UnoSuits> card = entry.getKey();
			Card<UnoSymbols, UnoSuits> currentTop = fromState.getTop();
			if (currentTop != null && (card.getf1().equals(currentTop.getf1())
					|| card.getf2().equals(currentTop.getf2()))) {
				for (UnoLocations location : entry.getValue()) {
					if (location.equals(playerLocation)) {
						moves.add(new PlayCardMove<UnoLocations>(forPlayerID, entry.getKey(), playerLocation,
								UnoLocations.DISCARDPILE));
					}
				}
			} else if (currentTop == null) {
				for (UnoLocations location : entry.getValue()) {
					if (location.equals(playerLocation)) {
						moves.add(new PlayCardMove<UnoLocations>(forPlayerID, entry.getKey(), playerLocation,
								UnoLocations.DISCARDPILE));
					}
				}
			}
		}
		return (List) moves;
	}

	@Override
	public boolean validMove(UnoGameState fromState, Move move) {
		boolean validPlayer = false, sameOldLocation = false, validNewLocation = false, compatibleWithTop;
		TreeMap<Card<UnoSymbols, UnoSuits>, ArrayList<UnoLocations>> map = fromState.getMap();
		
		if(move instanceof DrawCardMove){
			return true;
		}
		
		PlayCardMove<UnoLocations> pcMove = (PlayCardMove) move;

		// Check if it's the right players turn.
		validPlayer = pcMove.getPlayer() == fromState.getCurrentPlayer();

		// Check if old location of card exists in gameState.
		for (UnoLocations loc : (ArrayList<UnoLocations>) map.get(pcMove.getCard())) {
			if (pcMove.getOldLoc().equals(loc)) {
				sameOldLocation = true;
			}
		}

		// Check if compatible with current top.
		if (fromState.getTop() == null) {
			compatibleWithTop = true;
		} else {
			compatibleWithTop = pcMove.getCard().getf1().equals(fromState.getTop().getf1())
					|| pcMove.getCard().getf2().equals(fromState.getTop().getf2());
		}

		// Check if new location is valid

		// If old location is among the first n UnoLocations (which are
		// players), where n is the number of players in a particular game
		// only DISCARDPILE is a valid new location.
		if (pcMove.getOldLoc().ordinal() < fromState.getNumberOfPlayers()) {
			validNewLocation = pcMove.getNewLoc().equals(UnoLocations.DISCARDPILE);
		}
		// If old locations is DRAWPILE only players (first n UnoLocations) are
		// valid new locations, where n is the number of players in a particular
		// game
		else if (pcMove.getOldLoc().equals(UnoLocations.DRAWPILE)) {
			validNewLocation = pcMove.getNewLoc().ordinal() < fromState.getNumberOfPlayers();
		}
		return validPlayer && sameOldLocation && validNewLocation && compatibleWithTop;
	}

	@Override
	public boolean makeMove(UnoGameState fromState, Move move) {		
		
		//validMove() takes current player into account. Therefore don't setNextPlayer() before validMove is executed!
		if (!validMove(fromState, move)) {
			fromState.incrementRoundNR();
			nextTurn(fromState, move);
			return false;
		}

		if (move instanceof DrawCardMove) {
			DrawCardMove<UnoLocations> dcMove = (DrawCardMove) move;
			try {
				fromState.changeLocation(dcMove.getCard(), UnoLocations.DRAWPILE, UnoLocations.values()[dcMove.getPlayer()]);
				fromState.incrementRackSize(dcMove.getPlayer());
			} catch (KeyNotFoundException | LocationNotFoundException e) {
				e.printStackTrace();
			}
		}

		if (move instanceof PlayCardMove) {
			PlayCardMove<UnoLocations> pcMove = (PlayCardMove) move;
			try {
				fromState.changeLocation((Card) pcMove.getCard(),
						(UnoLocations) pcMove.getOldLoc(), (UnoLocations) pcMove.getNewLoc());
				fromState.decrementRackSize(pcMove.getPlayer());
				fromState.changeTop(pcMove.getCard());
			} catch (KeyNotFoundException | LocationNotFoundException e) {
				e.printStackTrace();
			}
		}
		fromState.incrementRoundNR();
		nextTurn(fromState, move);
		return true;
	}

	@Override
	public void undoMove(UnoGameState fromState, Move move) {
		throw new UnsupportedOperationException("Method not implemented");
	}

	@Override
	public boolean isTerminal(UnoGameState state) {

		TreeMap<Card<UnoSymbols, UnoSuits>, ArrayList<UnoLocations>> map = state.getMap();
		ArrayList<UnoLocations> list = new ArrayList<>();

		for (Map.Entry<Card<UnoSymbols, UnoSuits>, ArrayList<UnoLocations>> entry : map.entrySet()) {
			for (UnoLocations location : entry.getValue()) {
				if (!list.contains(location)) {
					list.add(location);
				}
			}
		}

		// Terminate if one player doesn't hold any cards. But keep in mind the
		// 2 extra locations (drawpile, discardpile)
		// TODO: Only a temporary ugly solutions - see issue 116 - https://bitbucket.org/skroon/ingenious-framework/issues/116/make-unogamelogicisterminal-more-elegant
		return list.size() == state.getNumberOfPlayers() - 1 + 2 && state.getRoundNR() > 12;
	}
}
