# Exceptions

#### KeyNotFoundException

Will be raised if the key (which is a Card) could not be found in the TreeMap of the CardGameState

####LocationNotFoundException

Will be raised if a certain location in the value of a TreeMap-entry (which is an ArrayList of locations) could not be found.

