package za.ac.sun.cs.ingenious.search.ismcts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import za.ac.sun.cs.ingenious.core.model.GameLogic;
import za.ac.sun.cs.ingenious.core.model.GameState;
import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.games.shadowtictactoe.STTTGameState;
import za.ac.sun.cs.ingenious.search.mcts.SearchNode;
import za.ac.sun.cs.ingenious.search.mcts.SimpleNode;

public abstract class ISMCTSNode<S extends GameState, N extends ISMCTSNode<S,N>> extends SimpleNode<S,N> implements DeterminizedSearchNode<S> {

	public ISMCTSNode(S currentState, S determinizedState, GameLogic<S> logic, Action toThisMove, N parent) {
		super(currentState, logic, toThisMove, parent);
		visitsPerChildNodes = new HashMap<Action, Integer>();
		setParentDeterminizedState(determinizedState);
	}

	private Map<Action, Integer> visitsPerChildNodes;
	protected S determinizedState;

	@Override
	public S getDeterminizedState() {
		return determinizedState;
	}

	@Override
	public void setParentDeterminizedState(S determinizedState) {
		this.determinizedState = (S) determinizedState.deepCopy();
		if(parentToThisMove != null)
			logic.makeMove(this.determinizedState, parentToThisMove);
	}

	@Override
	public Map<Action, Integer> getVisitsPerChildNodes() {
		return visitsPerChildNodes;
	}
	
	@Override
	public List<Action> getMovesToExpand() {
		ArrayList<Action> l = new ArrayList<Action>();
		for (Action m : unExpandedMoves) {
			if (logic.validMove(determinizedState, m)) {
				l.add(m);
			}
		}
		return l;
	}
	
	@Override
	public void addPlayout(double[] d) {
		super.addPlayout(d);
		// For subset multi armed bandit, update the visits for each child node
		// that was available for this playout
		for (N child : expandedChildren) {
			if (logic.validMove(determinizedState, child.getMove())) {
				if(visitsPerChildNodes.get(child.getMove())==null)
					visitsPerChildNodes.put(child.getMove(),1);
				else
					visitsPerChildNodes.put(child.getMove(),visitsPerChildNodes.get(child.getMove()).intValue()+1);
			}
		}
	}

}
