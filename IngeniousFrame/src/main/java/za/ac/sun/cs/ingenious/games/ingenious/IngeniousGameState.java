package za.ac.sun.cs.ingenious.games.ingenious;

import java.util.ArrayList;

import za.ac.sun.cs.ingenious.core.model.GameState;
import za.ac.sun.cs.ingenious.games.ingenious.engines.IngeniousScoreKeeper;

public class IngeniousGameState extends GameState{

	private final IngeniousBoard gameBoard;
	private final int currentPlayer;
	private final IngeniousScoreKeeper currScore;
	private final ArrayList<ArrayList<Tile>> racks;
	private final BagSequence currentBag;
	
	public IngeniousGameState(IngeniousBoard currBoard, int nextPlayer,IngeniousScoreKeeper scorekeeper,ArrayList<ArrayList<Tile>> racks, BagSequence bag ){
		super(-1); 
		gameBoard= currBoard;
		 currentPlayer = nextPlayer;
		 currScore = scorekeeper;
		 currentBag = bag;
		 this.racks = racks;
	}

	public IngeniousBoard getGameBoard() {
		return gameBoard;
	}

	public int getCurrentPlayer() {
		return currentPlayer;
	}

	public IngeniousScoreKeeper getCurrScore() {
		return currScore;
	}

	public BagSequence getCurrentBag() {
		return currentBag;
	}

	@Override
	public void printPretty() {
		System.out.println(gameBoard);
	}
	
}
