package za.ac.sun.cs.ingenious.games.cardGames.uno.game;

import com.esotericsoftware.minlog.Log;

import java.util.Arrays;

import za.ac.sun.cs.ingenious.games.cardGames.core.card.Card;
import za.ac.sun.cs.ingenious.games.cardGames.core.cardGameState.CardGameState;
import za.ac.sun.cs.ingenious.games.cardGames.core.deck.Deck;
import za.ac.sun.cs.ingenious.games.cardGames.uno.cardFeatures.UnoSuits;
import za.ac.sun.cs.ingenious.games.cardGames.uno.cardFeatures.UnoSymbols;

public class UnoGameState extends CardGameState<UnoSymbols, UnoSuits, UnoLocations>{

	public static final int INITNUMBEROFCARDSONHAND = 7;
	
	private int numberOfPlayers;
	private Card<UnoSymbols, UnoSuits> top;
	
	private int roundNR;
	private int drawPileSize;
	private int[] rackSizes;
	
	public UnoGameState(Deck<UnoSymbols, UnoSuits> deck, int numberOfPlayers) {
		super(deck, UnoLocations.DRAWPILE, 0, numberOfPlayers);
		this.numberOfPlayers = numberOfPlayers;
		roundNR = 1;
		top = null;
		drawPileSize = deck.getSize() - (numberOfPlayers*7);
		rackSizes = new int[numberOfPlayers];
		Arrays.fill(rackSizes, INITNUMBEROFCARDSONHAND);
	}
	
	@Override
	public void printPretty(){
		StringBuilder s = new StringBuilder();
	
		s.append("\n#Uno - Round " + roundNR + " #\n");
		s.append("Number of players: " + this.numberOfPlayers + "\n");
		s.append("Rack sizes: [");
		for(int i=0; i<rackSizes.length; i++){
			if(i==0){s.append(rackSizes[i]);}
			else{s.append(", " + rackSizes[i]);}
		}
		s.append("] \n");
		s.append("It's Player" + this.getCurrentPlayer() + "'s turn\n");
		s.append("Top of the discard pile:");
		if(top==null){s.append("empty\n");}
		else{s.append(top.toString() + "\n\n");}
		Log.info(s);
		
		super.printPretty();
	}
	
	public void incrementRoundNR(){
		roundNR++;
	}
	
	public void decrementDrawPile(){
		drawPileSize--;
	}
	
	public int getNumberOfPlayers() {
		return numberOfPlayers;
	}
	
	public int getRoundNR(){
		return roundNR;
	}
	
	public int getSizeOfDrawPile(){
		return drawPileSize;
	}
	
	//Deep Copy
	public int[] getRackSizes() {
		int[] copy = Arrays.copyOf(rackSizes, rackSizes.length);
		return copy;
	}
	
	public void changeTop(Card<UnoSymbols, UnoSuits> newTop){
		this.top = newTop;
	}
	
	public Card<UnoSymbols, UnoSuits> getTop(){
		return top;
	}
	
	public void incrementRackSize(int player){
		this.rackSizes[player]++;
	}
	
	public void decrementRackSize(int player){
		this.rackSizes[player]--;
	}
}
