	package za.ac.sun.cs.ingenious.games.nim;

import com.esotericsoftware.minlog.Log;

import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.model.TurnBasedNimBoard;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;

/**
 * This is an abstract superclass for all the different engines to be implemented for Nim.
 * Engines are basically the strategies of the players in a game.
 */
public abstract class NIMEngine extends Engine {

	protected TurnBasedNimBoard currentState;
	protected NIMLogic logic;	
	
	/**
	 * The required constructor for the TCP back-end to work
	 * 
	 * @param toServer Object representing the connection to the server. We just need to
	 *            pass this on, the rest is all handled at the level of the superclass.
	 */
	public NIMEngine(EngineToServerConnection toServer) {
		super(toServer);
		this.logic = new NIMLogic();
		this.currentState = new TurnBasedNimBoard(4,0,2);
	}
	
	/**
	 * ============================================================================================================
	 * 													Required Methods
	 *  ============================================================================================================
	 */
	
	/**
	 * Put any code here that needs to be called to initialize a game (e.g. setup board, etc).
	 * In case of Nim nothing needs to be initialized, the board always looks the same.
	 */
	@Override
	public void receiveInitGameMessage(InitGameMessage a) {
	
	}
	
	/**
	 * Called after a player made a move. Put logic here to update your local gamestate accordingly.
	 * @param a The message sent by the server informing this engine that a move has been played
	 */
	@Override
	public void receivePlayedMoveMessage(PlayedMoveMessage a) {
		logic.makeMove(currentState, a.getMove());
	}
	
	/**
	 * Put any code here that shall be executed when a game terminated message is sent by server, e.g. print the winner and the board.
	 */
	@Override
	public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
		NIMFinalEvaluator eval = new NIMFinalEvaluator();
        Log.info("Game has terminated!");
        Log.info("Final scores:");
        double[] score = eval.getScore(currentState);
        for (int i = 0; i < score.length; i++) {
            Log.info("Player "+i+": "+score[i]);
        }
		Log.info("Final state:");
		currentState.printPretty();
	}
	
}
