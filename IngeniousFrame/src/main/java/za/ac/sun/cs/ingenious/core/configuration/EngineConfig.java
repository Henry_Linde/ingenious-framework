package za.ac.sun.cs.ingenious.core.configuration;

import java.io.Serializable;

/**
 * EngineConfig Configuration parameters
 * Created by Chris Coetzee on 2016/07/18.
 */
public class EngineConfig implements Serializable {
    private static final long serialVersionUID = -3467684858273753L;

    /* The simple name for the engine */
    private String name;

    /* The full class identifier, should be unique */
    private String className;

    public EngineConfig(String name, String className) {
        this.name = name;
        this.className = className;
    }

    public String getName() {
        return name;
    }

    public String getClassName() {
        return className;
    }

    @Override public String toString() {
        final StringBuilder sb = new StringBuilder("EngineConfig{");
        sb.append("className='").append(className).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
