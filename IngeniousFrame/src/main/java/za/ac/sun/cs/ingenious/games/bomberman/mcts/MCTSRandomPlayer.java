package za.ac.sun.cs.ingenious.games.bomberman.mcts;

import java.util.List;

import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.BMBoard;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.BMLogic;
import za.ac.sun.cs.ingenious.games.bomberman.network.UpdateBombsMessage;

public class MCTSRandomPlayer implements PlayerRepresentation {

	private int id;
	private BMBoard board;
	private BMLogic logic = BMLogic.defaultBMLogic;
	private Action firstMove;

	/**
	 * Used during random playouts, simulates an opposing player.
	 * @param id
	 * @param board
	 */
	public MCTSRandomPlayer(int id, BMBoard board) {
		this.id = id;
		this.board = board;
	}
	
	public void setFirstMove(Action firstMove) {
		this.firstMove = firstMove;
	}
	
	@Override
	public int getID() {
		return id;
	}

	@Override
	public void initGame(InitGameMessage a) {
	}

	@Override
	public void playMove(PlayedMoveMessage a) {
	}

	@Override
	public PlayActionMessage genAction(GenActionMessage a) {
		if(firstMove!=null){
			PlayActionMessage an = new PlayActionMessage(firstMove);
			firstMove = null;
			return an;
		}
		return new PlayActionMessage(recursiveCreateSmarterMove(id, 0, board, 0));
	}
	
	public Action recursiveCreateSmarterMove(int id, int depth, BMBoard board, int round){
		List<Action> actions = logic.generateActions(board, id);
		while(!actions.isEmpty()){
			Action action = actions.remove((int)(Math.random()*actions.size()));
			BMBoard testBoard = (BMBoard) board.deepCopy();
			if(depth > 0) {
				logic.applyPlayedMoveMessage(new UpdateBombsMessage(-1, false), testBoard);
			}
			logic.makeMove(testBoard, action);
			if (testBoard.getPlayer(id).isAlive()){
				if (depth == 4) {
					return action;
				} else {
					Action nextMove = recursiveCreateSmarterMove(id, depth+1, testBoard, round);
					if (nextMove != null) {
						return action ;
					}
				}
			}
		}
		return null;
	}

	@Override
	public void terminateGame(GameTerminatedMessage a) {
		

	}

}
