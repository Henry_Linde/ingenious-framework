package za.ac.sun.cs.ingenious.games.nim;

import za.ac.sun.cs.ingenious.core.model.TurnBasedNimBoard;
import za.ac.sun.cs.ingenious.search.minimax.GameEvaluator;

/**
 * The final evaluator determines the score at the end of the game. Every game must
 * implement the GameFinalEvaluator interface.
 */
public class NIMFinalEvaluator implements GameEvaluator<TurnBasedNimBoard>{

	/**
	 * @param forState Some terminal state
	 * @return The ID of the player who won in that state, or -1 if it is a draw.
	 */
	public int getWinner(TurnBasedNimBoard forState) {
		if (forState.getPieces() == 1) {
			return (forState.nextMovePlayerID+1) % forState.getNumPlayers();
		} else {
			return -1;
		}
	}

	/**
	 * Determine Scores; 1 for a win and 0 for a loss
	 */
	@Override
	public double[] getScore(TurnBasedNimBoard forState) {
		double[] scores = new double[2];
		if(getWinner(forState)==-1){
			scores[0] = 0.5;
			scores[1] = 0.5;
		}else{
			scores[getWinner(forState)] = 1;
		}
		return scores;
	}

}
