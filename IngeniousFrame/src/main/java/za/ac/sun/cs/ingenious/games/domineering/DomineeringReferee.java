package za.ac.sun.cs.ingenious.games.domineering;

import za.ac.sun.cs.ingenious.core.PerfectInformationAlternatingPlayReferee;
import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.core.model.TurnBasedSquareBoard;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.games.domineering.game.DomineeringLogic;

public class DomineeringReferee extends PerfectInformationAlternatingPlayReferee<TurnBasedSquareBoard,DomineeringLogic,DomineeringFinalEvaluator> {
	
	public DomineeringReferee(MatchSetting match, PlayerRepresentation[] players) {
		super(match, players, new TurnBasedSquareBoard(match.getBoardSize(),0,2), new DomineeringLogic(),new DomineeringFinalEvaluator());
	}
	
	@Override
	protected InitGameMessage createInitGameMessage(PlayerRepresentation player) {
		return new DomineeringInitGameMessage(currentState.getBoardSize());
	}
	
	@Override
	protected void afterPlayersTurn(PlayerRepresentation player, Action action) {
		super.afterPlayersTurn(player, action);
		
		int lastPlayer = player.getID();
		int nextPlayer = lastPlayer==0?1:0;
		currentState.nextMovePlayerID = nextPlayer;		
	}
}
