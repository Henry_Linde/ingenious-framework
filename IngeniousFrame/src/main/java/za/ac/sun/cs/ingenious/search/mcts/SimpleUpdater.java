package za.ac.sun.cs.ingenious.search.mcts;

import za.ac.sun.cs.ingenious.core.model.GameState;

/**
 * Standard implementation of the TreeUpdater. Just adds the given normalized
 * results to each node in the tree that was visited for that playout
 * @author Michael Krause
 */
public class SimpleUpdater<N extends SearchNode<? extends GameState,N>> implements TreeUpdater<N> {

	public void backupValue(N node, double[] normalizedResults) {
		N iterator = node;
		while (iterator != null) {
			iterator.addPlayout(normalizedResults);
			iterator = iterator.getParent();
		}
	}
}
