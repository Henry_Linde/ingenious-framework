package za.ac.sun.cs.ingenious.games.bomberman.engines;

import java.util.List;

import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.games.bomberman.BMEngine;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.BMBoard;
import za.ac.sun.cs.ingenious.games.bomberman.network.BMGenActionMessage;

/**
 * BMEngine that plays a random legal move. It does not take care of scoring or even defending itself.
 */
public class BMEngineRandom extends BMEngine {

	public BMEngineRandom(EngineToServerConnection toServer) {
		super(toServer);
	}

	public BMEngineRandom(BMBoard board, char playerID, int engineNr) {
		super(board, playerID, engineNr);
	}
	
	@Override
	public String engineName() {
		return "BMEngineRandom";
	}

	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
		BMGenActionMessage bmGenMove = (BMGenActionMessage) a; 
		return new PlayActionMessage(createRandomMove(bmGenMove.getPlayerID()));
	}
	
	/**
	 * This method simply picks a random move from all available moves.
	 * @param id The id of the current player.
	 * @return a move, if none is possible null.
	 */
	private Action createRandomMove(int id) {
		assert id == playerID : "Move requested for player that does not correspond to this engine.";
		List<Action> actions = logic.generateActions(board, id);
        assert (!actions.isEmpty()); // Idle move is always legal.
        return actions.get((int) (Math.random() * actions.size()));
	}
}
