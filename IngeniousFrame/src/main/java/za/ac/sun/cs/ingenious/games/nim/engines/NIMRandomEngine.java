package za.ac.sun.cs.ingenious.games.nim.engines;

import java.util.List;
import java.util.Random;

import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.core.model.XYAction;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.games.nim.NIMEngine;

/**
 * This engine just chooses a random, valid action when it has to act. 
 */
public class NIMRandomEngine extends NIMEngine {

	public NIMRandomEngine(EngineToServerConnection toServer) {
		super(toServer);
	}
	
	@Override
	public String engineName() {
		return "NIMRandomEngine";		
	}
	
	/**
	 * This method is called when the server requests an action by this engine.
	 */
	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
		List<Action> actions = logic.generateActions(currentState, this.playerID);
		if(!actions.isEmpty()){
			Random rand = new Random();
			return new PlayActionMessage((XYAction)actions.get(rand.nextInt(actions.size())));
		} else
			return new PlayActionMessage(null); // Return a null action when none is possible
	}

}
