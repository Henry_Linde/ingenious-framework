package za.ac.sun.cs.ingenious.core.network.lobby.messages;

import za.ac.sun.cs.ingenious.core.network.Message;


/**
 * Send this message to the server to getInstance a list of lobbies and some info about them back. Answer is a {@link LobbyOverviewReply}
 * @author Stephan Tietz
 *
 */
public class LobbyOverviewRequest extends Message {

	private static final long serialVersionUID = 1L;

}
