package za.ac.sun.cs.ingenious.core.network.lobby.messages;

import za.ac.sun.cs.ingenious.core.network.Message;

/**
 * Used as part of the handshake between client and server when the client joins a lobby.
 */
public class SendNameMessage extends Message {

	private static final long serialVersionUID = 1L;
	private final String playerName;
	
	public SendNameMessage(String playerName) {
		this.playerName = playerName;
	}
	
	public String getPlayerName() {
		return playerName;
	}
	
}
