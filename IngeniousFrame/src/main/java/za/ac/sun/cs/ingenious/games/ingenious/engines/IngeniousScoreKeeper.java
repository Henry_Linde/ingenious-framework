package za.ac.sun.cs.ingenious.games.ingenious.engines;

import za.ac.sun.cs.ingenious.core.model.Coord;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousBoard;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousMinMaxBoardInterface;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousAction;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousRack;
import za.ac.sun.cs.ingenious.games.ingenious.Tile;

public class IngeniousScoreKeeper {

	private final int numberOfPlayers;
	private final int numberOfColours;
	private Score[] playerScores;

	public IngeniousScoreKeeper(int numberOfPlayers, int numberOfColours) {
		this.numberOfColours = numberOfColours;
		this.numberOfPlayers = numberOfPlayers;
		this.playerScores = new Score[numberOfPlayers];

		for (int i = 0; i < playerScores.length; i++) {
			playerScores[i] = new Score(numberOfColours);
		}
	}

	public IngeniousScoreKeeper copy() {
		IngeniousScoreKeeper copy = new IngeniousScoreKeeper(this.numberOfPlayers,
				this.numberOfColours);
		for (int i = 0; i < copy.playerScores.length; i++) {
			int currentScore[] = this.getScore(i).getScore();
			for (int j = 0; j < copy.numberOfColours; j++) {
				copy.getScore(i).addScore(j, currentScore[j]);
			}
		}
		return copy;
	}

	public int getNumPlayers() {
		return numberOfPlayers;
	}

	public int getLeader() {

		int leader = 0;
		boolean draw = false;
		for (int i = 1; i < numberOfPlayers; i++) {
			if(playerScores[leader].compare(playerScores[leader], playerScores[i]) < 0){
				leader = i;
				draw = false;
			}else if(playerScores[leader].compare(playerScores[leader], playerScores[i]) == 0){
				draw = true;
			}
		}
		if(draw){
			return -1;
		}
		return leader;
	}

	public int[] updateScore(int playerId, IngeniousMinMaxBoardInterface<IngeniousAction, IngeniousRack> gameBoard) {

		int[] update = calculateScore(gameBoard);
		for (int i = 0; i < numberOfColours; i++) {
			playerScores[playerId].addScore(i, update[i]);
		}

		return playerScores[playerId].getScore();
	}

	public int[] undoScore(int playerId, IngeniousMinMaxBoardInterface<IngeniousAction, IngeniousRack> gameBoard) {

		int[] update = calculateScore(gameBoard);
		for (int i = 0; i < numberOfColours; i++) {
			playerScores[playerId].addScore(i, -update[i]);
		}

		return playerScores[playerId].getScore();
	}

	public Score getScore(int playerId) {
		return playerScores[playerId];
	}

	public int[] calculateScore(IngeniousMinMaxBoardInterface<IngeniousAction, IngeniousRack> gameBoard) {

		int topScore = 0;
		int botScore = 0;

		int[] score = new int[numberOfColours];
		Tile tile = ((IngeniousAction) gameBoard.lastMove()).getTile();
		for (Coord coord : Tile.coordinateSystem) {
			botScore += scoreBottomTileDirection(coord, gameBoard);
			topScore += scoreTopTileDirection(coord, gameBoard);
		}

		score[tile.getBottomColour()] += botScore;
		score[tile.getTopColour()] += topScore;

		return score;
	}

	private int scoreBottomTileDirection(Coord direction,
			IngeniousMinMaxBoardInterface<IngeniousAction, IngeniousRack> gameBoard) {

		boolean connected = true;
		IngeniousAction lastMove = (IngeniousAction) gameBoard.lastMove();
		int tileBottomColour = lastMove.getTile().getBottomColour();
		int score = 0;

		Coord directionFromBottom = lastMove.getTile()
				.getTopCoord(lastMove.getPosition())
				.sub(lastMove.getPosition());

		/*
		 * Check the in the direction specified from the bottom position
		 */
		Coord checkCoordinate = lastMove.getPosition().add(direction);
		if (!directionFromBottom.equals(direction)) {
			while (connected) {
				if (gameBoard.getHex(checkCoordinate) == tileBottomColour) {
					score++;
					checkCoordinate = checkCoordinate.add(direction);
				} else {
					connected = false;
				}
			}
		}

		return score;
	}

	private int scoreTopTileDirection(Coord direction,
			IngeniousMinMaxBoardInterface<IngeniousAction, IngeniousRack> gameBoard) {

		boolean connected = true;
		IngeniousAction lastMove = (IngeniousAction) gameBoard.lastMove();
		int tileTopColour = lastMove.getTile().getTopColour();
		int score = 0;

		Coord directionFromBottom = lastMove.getTile()
				.getTopCoord(lastMove.getPosition())
				.sub(lastMove.getPosition());
		Coord directionFromTop = directionFromBottom.negate();

		/*
		 * Check the in the direction specified from the bottom position
		 */
		Coord checkCoordinate = lastMove.getTile()
				.getTopCoord(lastMove.getPosition()).add(direction);

		if (!directionFromTop.equals(direction)) {
			while (connected) {
				if (gameBoard.getHex(checkCoordinate) == tileTopColour) {
					score++;
					checkCoordinate = checkCoordinate.add(direction);
				} else {
					connected = false;
				}
			}
		}

		return score;
	}

	public static void main(String[] args) {
		IngeniousBoard board = new IngeniousBoard(7,6);
		Tile tile = new Tile(1, 2);
		tile.setRotation(4, 6);
		Coord coord = new Coord(0, 0);
		IngeniousAction move = new IngeniousAction(tile, coord);
		board.makeMove(move);
		IngeniousScoreKeeper score = new IngeniousScoreKeeper(1, 6);

		int result[] = score.calculateScore(board);
		for (int i = 0; i < result.length; i++) {
			System.out.println(i + " :" + result[i]);
		}

		coord = new Coord(0, 2);
		move = new IngeniousAction(tile, coord);
		board.makeMove(move);

		IngeniousScoreKeeper cScore = score.copy();
		result = score.calculateScore(board);
		for (int i = 0; i < result.length; i++) {
			System.out.println(i + " :" + result[i]);
		}

		coord = new Coord(0, 1);
		move = new IngeniousAction(tile, coord);
		board.makeMove(move);
		result = score.calculateScore(board);
		System.out.println(board);
		for (int i = 0; i < result.length; i++) {
			System.out.println(i + " :" + result[i]);
		}

	}
}