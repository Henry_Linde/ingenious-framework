package za.ac.sun.cs.ingenious.core.commandline;

import com.esotericsoftware.minlog.Log;

import java.util.List;
import java.util.Scanner;

/**
 * Created by Chris Coetzee on 2016/07/27.
 */
public class CommandlineUtils {

    public static <T> T readChoice(String message, List<T> options, List<?> messages) {
        Log.info(message);

        if (options.size() != messages.size()) {
            throw new IllegalArgumentException("Must have same number of options as messages");
        }

        Scanner kb = new Scanner(System.in);
        int choice;
        while (true) {
            for (int i = 0; i < options.size(); i++) {
                Log.info("[" + (i+1) + "] " + messages.get(i));
            }
            try {
                Log.info("Input: ");
                choice = kb.nextInt();
                if (choice < 1 || choice > options.size()) {
                    Log.info("No such option, please choose again:");
                    continue;
                }
                break;
            } catch (Exception e) {
                /* throw away whatever was entered on that line */
                kb.nextLine();
            }

        }
        kb.close();
        return options.get(choice - 1);
    }

}
