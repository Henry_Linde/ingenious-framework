package za.ac.sun.cs.ingenious.games.ingenious.engines;

import java.util.Arrays;
import java.util.Comparator;

public class Score implements Comparator<Score> {
	
	private int[] score;
	private int numberOfColours;
	
	public Score(int numberOfColours){
		this.numberOfColours = numberOfColours;
		this.score = new int[numberOfColours];	
	}
	
	public int[] getScore(){
		return score;
	}
	
	public int compare(Score arg0, Score arg1) {
		if(arg0.getScore().length != arg1.getScore().length){
			return -1;
		}
		
		int[] score0 = arg0.getScore().clone();
		int[] score1 = arg1.getScore().clone();
		Arrays.sort(score0);
		Arrays.sort(score1);
		
		for(int i =0; i<score0.length;i++){
			if(score0[i]<score1[i]){
				return -1;
			}else if(score0[i]>score1[i]){
				return 1;
			}
		}
		
		return 0;
	}
	
	public int getMinimum(){
		int min = Integer.MAX_VALUE;
		for(int i : score){
			if(i<min){
				min = i;
			}
		}
		return min;
	}
	
	public void incrementScore(int colour){
		if(score[colour]<18){
			score[colour]++;
		}
	}
	
	public void addScore(int colour, int addition){
		score[colour] += addition;
		if(score[colour]>18){
			score[colour] = 18;
		}
	}
	
	public boolean outrightWin(int maxScore){
		boolean hasWon = true;
		for(int i =0; i<numberOfColours;i++){
			if(score[i]!=maxScore){
				hasWon = false;
			}
		}
		return hasWon;
	}
	
	public int getColourScore(int colour){
		return score[colour];
	}

	public static void main(String [] args){
		Score one = new Score(6);
		Score two = new Score(6);
		
		two.addScore(0,4);
		one.addScore(0,4);
		two.addScore(1,0);
		
		System.out.println(one.compare(one, two));
	}
}