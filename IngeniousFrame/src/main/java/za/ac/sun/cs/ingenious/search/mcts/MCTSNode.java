package za.ac.sun.cs.ingenious.search.mcts;

import za.ac.sun.cs.ingenious.core.model.GameLogic;
import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.core.model.GameState;

/**
 * A SimpleNode implementation for most games
 * @author Michael Krause
 * @param <S> The type of GameState to operate on
 */
public class MCTSNode<S extends GameState> extends SimpleNode<S,MCTSNode<S>> {

	public MCTSNode(S parentState, GameLogic<S> logic, Action toThisNode, MCTSNode<S> parent) {
		super(parentState, logic, toThisNode, parent);
	}

	@Override
	public MCTSNode<S> expandAChild(Action action) {
		MCTSNode<S> newNode = new MCTSNode<S>(this.currentState, this.logic, action, this);
		expandedChildren.add(newNode);
		unExpandedMoves.remove(action);
		return newNode;
	}

}
