package za.ac.sun.cs.ingenious.core;

import com.esotericsoftware.minlog.Log;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

import za.ac.sun.cs.ingenious.core.network.lobby.ClientHandler;
import za.ac.sun.cs.ingenious.core.network.lobby.LobbyManager;

/**
 * GameServer listens to new connections and stores all open lobbies and connected
 * clients.
 *
 * @author steven
 */
public class GameServer implements Runnable {
    private static final String DEFAULT_HOSTNAME = "0.0.0.0";

    /* How many connections can be waiting for acceptance ? */
    private static final int          DEFAULT_BACKLOG = 50;
    private              ServerSocket serverSocket    = null;
    private              Socket       client          = null;
    private          int          port;
    private          boolean      socketRunning;
    private volatile boolean      interrupt;
    private          LobbyManager lobbyMan;

    /**
     * Create new GameServer using the port specified
     *
     * @param port - specifies the port on which the facilitator will listen
     * @throws IOException  thrown if port is not available
     */
    public GameServer(int port) throws IOException {
        this(DEFAULT_HOSTNAME, port);
    }

    public int getConnectedPort() {
        return port;
    }

    public GameServer(String hostname, int port) throws IOException {
        this.port = port;
        this.lobbyMan = new LobbyManager();
        try {
            this.serverSocket = new ServerSocket(port, DEFAULT_BACKLOG, InetAddress.getByName(
                    hostname));
            this.port = serverSocket.getLocalPort();
            socketRunning = true;
            Log.info("GameServer launched on port " + port);
        } catch (IOException e) {
            Log.error("Port " + port + " not available: GameServer failed.");
            throw e;
        }
    }

    /**
     * Listen for connecting managers.
     */
    @Override
	public void run() {
        while (!interrupt) {
            try {
                client = this.serverSocket.accept();

                Log.info("Accepted new connection from " + client.getInetAddress());
                Thread newStream = new Thread(new ClientHandler(client, lobbyMan));
                /* FIXME we should keep track of this thread */
                newStream.start();

            } catch (Exception e) {
                Log.error("Failed to connect new client. Will wait for further connections.");
            }
        }
    }

    public void close() {
        try {
            interrupt = true;
            serverSocket.close();
        } catch (SocketException e) {
            Log.info("Socket exception due to closing socket.");
        } catch (IOException e) {
        	Log.error("Error when closing GameServer socket", e);
        }
        socketRunning = false;
    }

    public LobbyManager getLobbyMan() {
        return lobbyMan;
    }

    public boolean isSocketRunning() {
        return socketRunning;
    }
}