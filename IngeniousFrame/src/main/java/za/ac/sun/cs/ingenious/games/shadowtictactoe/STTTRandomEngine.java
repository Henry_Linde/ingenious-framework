package za.ac.sun.cs.ingenious.games.shadowtictactoe;

import java.util.List;
import java.util.Random;

import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.core.model.XYAction;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;

public class STTTRandomEngine extends STTTEngine {

	public STTTRandomEngine(EngineToServerConnection toServer) {
		super(toServer);
	}
	
	/**
	 * Never use spaces in engine name!
	 */
	@Override
	public String engineName() {
		return "STTTRandomEngine";		
	}
	
	/**
	 * This method is called when the server requests a move by this engine. 
	 * Don't forget to update your own gamestate(board) if you make a move, receivePlayMove is only called for other players.
	 */
	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
		// This engine just chooses a random, valid move
		List<Action> actions = logic.generateActions(currentState, this.playerID);
		if(!actions.isEmpty()){
			Random rand = new Random();
			return new PlayActionMessage((XYAction)actions.get(rand.nextInt(actions.size())));
		} else
			return new PlayActionMessage(null);
	}

}
