package za.ac.sun.cs.ingenious.search.pimc;

import com.rits.cloning.Cloner;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import za.ac.sun.cs.ingenious.core.model.GameState;
import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.search.InformationSetDeterminizer;
import za.ac.sun.cs.ingenious.search.mcts.MCTS;
import za.ac.sun.cs.ingenious.search.mcts.PlayoutPolicy;
import za.ac.sun.cs.ingenious.search.mcts.SearchNode;
import za.ac.sun.cs.ingenious.search.mcts.TreeDescender;
import za.ac.sun.cs.ingenious.search.mcts.TreeUpdater;

public final class PIMC {
	private PIMC(){}

	/**
	 * IMPORTANT: This method requires the Moves used in the game to be comparable (i.e. equals and
	 * hashMap must have working implementations)
	 */
	public static <S extends GameState, N extends SearchNode<S,N>> Action generateMove(NodeCreator<S, N> creator,
			int forPlayerID, InformationSetDeterminizer<S> det, S informationSet, PlayoutPolicy<N> policy,
			TreeDescender<N> descender , TreeUpdater<N> updater, 
			long turnLength, long numDeterminizationsToTry) {

		Map<Action, Integer> visitsPerMove = new HashMap<Action, Integer>();
		
		long timePerDeterminization = turnLength/numDeterminizationsToTry;
		long timeInit = System.currentTimeMillis();
		long endTime = timeInit + turnLength;
		
		int numDeterminizationsTried = 0;
		while(System.currentTimeMillis() < endTime){
			numDeterminizationsTried++;
			N root = creator.createNode(det.determinizeUnknownInformation(forPlayerID, informationSet));
			MCTS.generateMove(root, policy, descender, updater, timePerDeterminization, false);
			for (N child : root.getExpandedChildren()) {
				if (visitsPerMove.get(child.getMove())!=null)
					visitsPerMove.put(child.getMove(), visitsPerMove.get(child.getMove()).intValue()+child.getVisits());
				else
					visitsPerMove.put(child.getMove(), child.getVisits());
			}
		}
		
		int mostVisits = Integer.MIN_VALUE;
		Action bestMove = null;
		System.out.println("Number of determinizations tried: " + numDeterminizationsTried);
		System.out.println("Possible moves:");
		for (Entry<Action,Integer> e : visitsPerMove.entrySet()) {
			System.out.println(e.getKey().toString() + ", visits: "+ e.getValue());
			if (e.getValue() > mostVisits) {
				mostVisits = e.getValue();
				bestMove = e.getKey();
			}
		}
		System.out.println("Best move: "+ bestMove.toString());
		
		return bestMove;

	}
}
