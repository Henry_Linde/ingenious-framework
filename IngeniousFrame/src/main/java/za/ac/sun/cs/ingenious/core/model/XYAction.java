package za.ac.sun.cs.ingenious.core.model;

/**
 * An action that consists of x and y coordinates and the player that acted.
 * 
 * @author Michael Krause
 */
public class XYAction implements Action {

	private static final long serialVersionUID = 1L;
	
	private int x, y;
	private int player;
	
	public XYAction(int x, int y, int player) {
		this.x = x;
		this.y = y;
		this.player = player;
	}
	
	public int getPlayer() {
		return player;
	}
	
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	
	@Override
	public String toString() {
		return "("+x+"|"+y+") --> "+player; 
	};	
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof XYAction)) {
			return false;
		} else {
			XYAction otherMove = (XYAction) obj;
			return (this.x == otherMove.x) && (this.y == otherMove.y) && (this.player == otherMove.player);
		}
	}

	@Override
	public int hashCode() {
		// Some magic. This is not a good hash function.
		return (((51 + this.x) * 51 + this.y) * 51 + this.player);
	}
}
