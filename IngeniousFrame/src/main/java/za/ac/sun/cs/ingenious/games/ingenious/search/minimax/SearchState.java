package za.ac.sun.cs.ingenious.games.ingenious.search.minimax;

import java.util.ArrayList;

import za.ac.sun.cs.ingenious.games.ingenious.IngeniousMinMaxBoardInterface;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousAction;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousRack;
import za.ac.sun.cs.ingenious.games.ingenious.engines.IngeniousScoreKeeper;

abstract public class SearchState {
	
	private IngeniousMinMaxBoardInterface<IngeniousAction, IngeniousRack> board;
	private IngeniousScoreKeeper scores;
	private ArrayList<IngeniousRack> racks;
	private int currentPlayer = 0;
	
	public SearchState(IngeniousMinMaxBoardInterface<IngeniousAction, IngeniousRack> board, IngeniousScoreKeeper scores,  ArrayList<IngeniousRack> racks, int player){
		this.setBoard(board);
		this.setScores(scores.copy());
		this.setRacks(new ArrayList<IngeniousRack>());
		this.setCurrentPlayer(player);
		
		for(IngeniousRack r : racks){
			this.getRacks().add(r.copy());
		}
	}
	
	public ArrayList<IngeniousAction> generateMoves(){

		// TODO commented out to avoid compile errors, sorry...
//		return getBoard().generateMoves(-1, getRacks().get(getCurrentPlayer()));
		return null;
	}

	public IngeniousMinMaxBoardInterface<IngeniousAction, IngeniousRack> getBoard() {
		return board;
	}

	public void setBoard(IngeniousMinMaxBoardInterface<IngeniousAction, IngeniousRack> board) {
		this.board = board;
	}

	public int getCurrentPlayer() {
		return currentPlayer;
	}

	public void setCurrentPlayer(int currentPlayer) {
		this.currentPlayer = currentPlayer;
	}

	public ArrayList<IngeniousRack> getRacks() {
		return racks;
	}

	public void setRacks(ArrayList<IngeniousRack> racks) {
		this.racks = racks;
	}

	public IngeniousScoreKeeper getScores() {
		return scores;
	}

	public void setScores(IngeniousScoreKeeper scores) {
		this.scores = scores;
	}	
	
}
