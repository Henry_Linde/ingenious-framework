package za.ac.sun.cs.ingenious.games.shadowtictactoe;

import za.ac.sun.cs.ingenious.core.PerfectInformationAlternatingPlayReferee;
import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.core.model.TurnBasedSquareBoard;
import za.ac.sun.cs.ingenious.core.model.UnobservedMove;
import za.ac.sun.cs.ingenious.core.model.XYAction;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.games.tictactoe.TTTFinalEvaluator;
import za.ac.sun.cs.ingenious.games.tictactoe.TTTLogic;

public class STTTReferee extends PerfectInformationAlternatingPlayReferee<TurnBasedSquareBoard, TTTLogic, TTTFinalEvaluator> {

	/**
	 * The required constructor for the Controller Factory.
	 * @param match contains the match settings from the MatchSettingsFile
	 * @param players an array of player representations (either local or TCP)
	 */
	public STTTReferee(MatchSetting match, PlayerRepresentation[] players) {
		super(match, players, new TurnBasedSquareBoard(3,0,2), new TTTLogic(), new TTTFinalEvaluator());
	}
	
	@Override
	protected void distributeAcceptedMove(int playerId, PlayedMoveMessage move) {
		players[playerId].playMove(move); // tell player who made the move about it
		int nextPlayer = (playerId+1) % matchSettings.getNumPlayers();
		players[nextPlayer].playMove(new PlayedMoveMessage(new UnobservedMove(playerId))); // tell other player that there was a move, but not which one
	}
	
	@Override
	protected void reactToInvalidMove(int player, PlayActionMessage m) {
		XYAction xym = (XYAction)m.getAction();
		players[player].playMove(new PlayedMoveMessage(new OccupiedXYAction(xym.getX(), xym.getY(), xym.getPlayer()))); // tell player who made the move that the move is already taken
		int nextPlayer = (player+1) % matchSettings.getNumPlayers();
		players[nextPlayer].playMove(new PlayedMoveMessage(new UnobservedMove(player))); // tell other player that there was a move, but not which one
	}
}
