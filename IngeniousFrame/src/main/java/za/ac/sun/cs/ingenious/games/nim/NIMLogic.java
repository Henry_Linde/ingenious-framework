package za.ac.sun.cs.ingenious.games.nim;

import java.util.ArrayList;
import java.util.List;

import za.ac.sun.cs.ingenious.core.model.Action;
import za.ac.sun.cs.ingenious.core.model.GameLogic;
import za.ac.sun.cs.ingenious.core.model.Move;
import za.ac.sun.cs.ingenious.core.model.TurnBasedGameLogic;
import za.ac.sun.cs.ingenious.core.model.TurnBasedNimBoard;
import za.ac.sun.cs.ingenious.core.model.XYAction;

/**
 * 
 * @author Henry Linde
 */
public class NIMLogic implements TurnBasedGameLogic<TurnBasedNimBoard> {

	@Override
	public boolean validMove(TurnBasedNimBoard fromState, Move move) {
		XYAction xyMove = (XYAction) move;
		if (fromState.board[xyMove.getX()] >= xyMove.getY()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	* @param fromState The current board state
	* @param move Move to be applied to the current board state
	* This function applies the move received to the current game state.
	*/
	@Override
	public boolean makeMove(TurnBasedNimBoard fromState, Move move) {
		XYAction xyMove = (XYAction) move;
		System.out.println(xyMove);
		fromState.board[xyMove.getX()] = (byte)(fromState.board[xyMove.getX()] - xyMove.getY());
		fromState.setPieces(xyMove.getY());
		nextTurn(fromState, move);
		return true;
	}

	/**
	* Undo move not supported.
	*/
	@Override
	public void undoMove(TurnBasedNimBoard fromState, Move move) {
		throw new UnsupportedOperationException();
	}

	/**
	* @param fromState The current state of the game
	* @param forPlayedID The current player to make a move.
	* This function generates all possible moves to be made from the current game state.
	*/

	@Override
	public List<Action> generateActions(TurnBasedNimBoard fromState, int forPlayerID) {
		ArrayList<Action> actions = new ArrayList<>();
		for(byte i = 0; i < fromState.getBoardSize(); i++){
			for (byte j = 1; j < fromState.board[i]+1; j++) {
				actions.add(new XYAction(i, j, forPlayerID));
			}
		}
		return actions;
	}

	/**
	* @param state The current state of the game
	* This function determines if the current game state is terminal i.e. The game has ended.
	*/
	@Override
	public boolean isTerminal(TurnBasedNimBoard state) {
		if (state.getPieces() == 1) {
			return true;
		} else {
			return false;
		}
	}
}
