package za.ac.sun.cs.ingenious.games.ingenious.search.minimax;

import java.util.ArrayList;

import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousEngine;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousAction;
import za.ac.sun.cs.ingenious.games.ingenious.Tile;
import za.ac.sun.cs.ingenious.games.ingenious.engines.FullInformationState;
import za.ac.sun.cs.ingenious.games.ingenious.engines.IMoveController;
import za.ac.sun.cs.ingenious.games.ingenious.engines.IngeniousEvaluator;

/**
 * A abstract logginsearch.minimax implementation can be extended for different logginsearch.minimax
 * enhancements. Extensions should be as domain independant as possible.
 * 
 * @author Steven Labrum
 *
 */
abstract public class MiniMax implements IMoveController {

	public IngeniousEvaluator evaluator;
	protected int playerId;

	/*
	 * opponentId is only applicable to 2 player logginsearch.minimax.
	 */
	protected int opponentId;

	protected int numberOfColours;
	public int searchDepth;

	/**
	 * getState should return a state that can be used by the logginsearch.minimax algorithm
	 * implemented. EngineConfig components should be copied rather than referenced.
	 * 
	 * getState is called at the startAsync of the logginsearch and updated throughout the
	 * logginsearch.
	 * 
	 * @return null
	 */
	public SearchState getState(IngeniousEngine engine) {

		// TODO commented out to avoid compile errors, sorry...
//		FullInformationState state = new FullInformationState((IngeniousMinMaxBoardInterface)engine.gameBoard,
//				engine.scoreKeeper.copy(), engine.racks, engine.expectedBag,
//				engine.getPlayerID());
//
//		return state;
		return null;
	}

	public void setLocalVariables(IngeniousEngine engine) {

		this.playerId = engine.getPlayerID();
		this.opponentId = 1 - this.playerId;
		this.numberOfColours = engine.matchSetting.getNumColours();
	}

	public IngeniousAction generateMove(IngeniousEngine engine, IngeniousEvaluator evaluator) {

		this.evaluator = evaluator;

		/*
		 * The gameState contains all information that changes at different
		 * ply's.
		 */
		SearchState gameState = getState(engine);

		/*
		 * set the localVariables which are used throughout the logginsearch without
		 * being changed.
		 */
		this.evaluator = evaluator;
		setLocalVariables(engine);



		ArrayList<IngeniousAction> moves = gameState.generateMoves();

		IngeniousAction bestMove = moves.get(0);
		double bestValue = Double.MIN_VALUE+1;
		double alpha = Double.MIN_VALUE+1;
		double beta = Double.MAX_VALUE;
		
		for (IngeniousAction child : moveOrder(moves,gameState)) {
			
			this.preSearchStateAdjustment(gameState);
			makeMove(child, gameState);
			double value = MinMove(gameState,alpha,beta, 0);
			unmakeMove(child, gameState);

			this.postSearchStateAdjustment(gameState);

			if (bestValue < value) {
				bestValue = value;
				bestMove = child;
			}
			
			alpha = Math.max(alpha, bestValue);
			if(bestValue >=beta){
				break;
			}

		}

		return bestMove;
	}

	/**
	 * Make move includes all affects to the state of the game not just the
	 * placement of a tile on the board
	 */
	public void makeMove(IngeniousAction child, SearchState gameState) {

		try {
			FullInformationState state = (FullInformationState) gameState;

			// TODO commented out to avoid compile errors, sorry...
//			state.getBoard().makeMove(child);
			state.getRacks().get(state.getCurrentPlayer()).remove(child.getTile());

			Tile bagTile = state.getSearchBag().get(state.getBagIndex());
			state.getRacks().get(state.getCurrentPlayer()).add(bagTile);

			state.getScores().updateScore(playerId, state.getBoard());
			state.setBagIndex(state.getBagIndex() + 1);
			state.setCurrentPlayer(1 - state.getCurrentPlayer());
		} catch (ClassCastException e) {
			throw new RuntimeException(
					"SearchState not compatible with  search.minimax enhancements");
		}

	}

	public void unmakeMove(IngeniousAction child, SearchState gameState) {
		try {
			FullInformationState state = (FullInformationState) gameState;

			state.setCurrentPlayer(1 - state.getCurrentPlayer());
			state.setBagIndex(state.getBagIndex() - 1);

			// TODO commented out to avoid compile errors, sorry...
//			state.getBoard().undoMove();
			state.getRacks().get(state.getCurrentPlayer()).add(child.getTile());

			Tile bagTile = state.getSearchBag().get(state.getBagIndex());
			state.getRacks().get(state.getCurrentPlayer()).remove(bagTile);

			state.getScores().undoScore(state.getCurrentPlayer(), state.getBoard());
		} catch (ClassCastException e) {
			throw new RuntimeException(
					"SearchState not compatible with  search.minimax enhancements");
		}
	}

	
	public void postSearchStateAdjustment(SearchState state) {

	}

	public void preSearchStateAdjustment(SearchState state) {

	}

	public ArrayList<IngeniousAction> moveOrder(ArrayList<IngeniousAction> moves,SearchState state) {
		/*
		 * default move ordering is random
		 */
		return moves;
	}

	public double MaxMove(SearchState state,double alpha,double beta, int depth) {

		if (state.getBoard().full() || this.searchDepth == depth) {
			return evaluator.evaluate(state.getScores(), state.getBoard(), state.getRacks(),this.playerId);
		}

		double bestValue =  Double.MIN_VALUE+1;

		ArrayList<IngeniousAction> moves = state.generateMoves();
		for (IngeniousAction child : moveOrder(moves,state)) {
			this.preSearchStateAdjustment(state);

			makeMove(child, state);
			double value = MinMove(state,alpha,beta, depth +1);
			unmakeMove(child, state);

			this.postSearchStateAdjustment(state);

			if (bestValue < value) {
				bestValue = value;
			}
			
			alpha = Math.max(alpha, bestValue);
			if(bestValue >=beta){
				break;
			}
		}

		return bestValue;
	}

	public double MinMove(SearchState state,double alpha,double beta, int depth) {

		if (state.getBoard().full() || this.searchDepth == depth) {
			return evaluator.evaluate(state.getScores(), state.getBoard(), state.getRacks(),this.playerId);
		}

		double bestValue =  Double.MAX_VALUE;

		ArrayList<IngeniousAction> moves = state.generateMoves();

		for (IngeniousAction child : moveOrder(moves,state)) {
			this.preSearchStateAdjustment(state);

			makeMove(child, state);
			double value = MaxMove(state,alpha,beta, depth + 1);
			unmakeMove(child, state);

			this.postSearchStateAdjustment(state);

			if (bestValue > value) {
				bestValue = value;
			}
			
			beta = Math.min(beta, bestValue);
			if(bestValue <=alpha){
				break;
			}

		}

		return bestValue;
	}
}
