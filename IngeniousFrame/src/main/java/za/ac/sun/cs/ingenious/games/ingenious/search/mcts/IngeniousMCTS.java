package za.ac.sun.cs.ingenious.games.ingenious.search.mcts;

import za.ac.sun.cs.ingenious.games.ingenious.IngeniousBoard;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousEngine;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousAction;
import za.ac.sun.cs.ingenious.games.ingenious.engines.IMoveController;
import za.ac.sun.cs.ingenious.games.ingenious.engines.IngeniousEvaluator;
import za.ac.sun.cs.ingenious.search.mcts.MCTS;
import za.ac.sun.cs.ingenious.search.mcts.TreeDescender;
import za.ac.sun.cs.ingenious.search.mcts.TreeUpdater;

public class IngeniousMCTS implements IMoveController {

	IngeniousBoard gameBoard;
	
	TreeUpdater updater;
	TreeDescender descender;
	SimpleIngeniousNode root;
	
	public IngeniousAction generateMove(IngeniousEngine engine, IngeniousEvaluator evaluator) {
		// TODO commented out to avoid compile errors, sorry...
//		gameBoard = (IngeniousBoard)engine.gameBoard;
//		long timeInit = System.currentTimeMillis();
//		long endTime = timeInit + Constants.TURN_LENGTH;
//		descender = new UCTDescender();
//		updater = new UCTUpdater();
//
//		root = new SimpleIngeniousNode(gameBoard, null,(short) engine.playerID,engine.racks, engine.expectedBag, null);
//		int numberOfPlayouts = 0;
//		while(System.currentTimeMillis() < endTime){
//			numberOfPlayouts++;
//			SimpleNode bestNode  = (SimpleNode) descender.bestSearchMove(root, numberOfPlayouts);
//			SimpleNode newNode = (SimpleNode) bestNode.expandAChild();
//			int result=0;
//			
//			result = DefaultIngeniousPolicy.ingeinousPlayout(newNode, (short) newNode.getCurrentPlayer());
//			
//			updater.backupValue(newNode, result);
//		}		
//		System.out.println("NUMBER OF PLAYOUTS : "+numberOfPlayouts);
//		return (IngeniousMove)descender.bestPlayMove(root);
		return null;
	}

}
