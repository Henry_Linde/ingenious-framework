#Deck

Basically just an ArrayList of cards.

Gets two generic types which implement the CardFeature interface.
Also gets two Iterables of these generic types as parameters for the constructor.

The class Deck automatically creates a certain number cards (according to the optional parameter of the constructor - default: 1) for each combination of the two Iterables.

This class doesn't cover decks where the number of cards of one type differs from another card type. (e. g. 1 x queen of spades, 2 x king of hearts).