package za.ac.sun.cs.ingenious.games.mnk;

import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;

public class MNKInitGameMessage extends InitGameMessage{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int h;
	private int w;
	private int k;
	
	public MNKInitGameMessage(int h, int w, int k){
		this.h = h;
		this.w = w;
		this.k = k;
	}
	
	public int getHeight(){
		return this.h;
	}
	
	public int getWidth(){
		return this.w;
	}
	
	public int getK(){
		return this.k;
	}
}
