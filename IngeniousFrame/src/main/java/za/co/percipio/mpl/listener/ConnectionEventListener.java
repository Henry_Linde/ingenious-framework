package za.co.percipio.mpl.listener;

import za.co.percipio.mpl.Message;
import za.co.percipio.mpl.connection.Connection;

/**
 *
 * Callback interface that should be implemented by any classes
 * that wishes to handle events that occur during messaging.
 *
 * @author Chris Coetzee <chriscz93@gmail.com>
 */
public interface ConnectionEventListener {
    /**
     * Called when a message was received on the provided transport
     * @param connection on which the message was received
     * @param message the message that was received
     */
    public void onMessageReceived(Connection connection, Message message);
    /**
     * Called when a  MessageTransport is connected to one on the
     * other end.
     */
    public void onSocketConnection(Connection connection);
    /**
     * Called when the transport is disconnected.
     *
     * @param connection the transport that
     */
    public void onSocketDisconnection(Connection connection);

    /**
     * Called when an error occurs during the use of the transport.
     *
     * @param c
     */
    public void onConnectionError(Connection c);
    /**
     * Called when an error occurs during the parsing of an incoming message.
     * @param connection on which the error occurred.
     * @param t type of the error
     */
    public void onIncomingMessageParsingError(Connection connection, Throwable t);
}
