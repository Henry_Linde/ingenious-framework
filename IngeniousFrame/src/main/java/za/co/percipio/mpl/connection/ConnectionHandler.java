package za.co.percipio.mpl.connection;

import com.esotericsoftware.minlog.Log;

import java.io.IOException;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import za.co.percipio.mpl.exception.ConnectionException;
import za.co.percipio.mpl.exception.MessageSerializationException;
import za.co.percipio.mpl.listener.ConnectionEventListener;

public class ConnectionHandler implements Runnable {

    private List<SocketChannel> pendingChannels;

    private          HashMap<Connection, Connection> clientConnections;
    private          Thread                          thread;
    private          ConnectionEventListener         listener;
    private          Selector                        selector;
    private volatile boolean                         active;

    private Object initializationLock = new Object();

    /**
     * Creates a ConnectionHandler, if the a selector cannot be created an exception is thrown
     *
     * @throws RuntimeException
     **/
    public ConnectionHandler(ConnectionEventListener listener) {
        pendingChannels = new LinkedList<SocketChannel>();
        clientConnections = new HashMap<Connection, Connection>();

        thread = new Thread(this, "ConnectionHandler Thread");
        this.listener = listener;
        try {
            selector = Selector.open();

        } catch (IOException e) {
            Log.error(this.getClass().getName(),"[ConnectionHandler] Couldn't open selector!!", e);
            throw new RuntimeException(e);
        }
    }

    public void start() {
        if (!active) {
            synchronized (initializationLock) {
                if (!active) {
                    active = true;
                    thread.start();
                } else {
                    Log.warn(this.getClass().getName(),"Handler already active");
                }
            }
        }
    }

    public void stop() {
        if (active) {
            synchronized (initializationLock) {
                if (active) {
                    selector.wakeup();
                    active = false;
                    selector.wakeup();
                }
            }
        }
    }

    public void manageSocketChannel(SocketChannel socket) {
        // XXX Should have been initialized by now!!!!
        try {
            synchronized (pendingChannels) {
                socket.configureBlocking(false);
                pendingChannels.add(socket);//
                if (selector != null) {
                    selector.wakeup();
                }
            }
        } catch (IOException e) {
            Log.error(this.getClass().getName(),"[SocketHandler] Could not manage socket");
        }
    }

    @Override
    public void run() {
        while (active) {
            try {
                registerNewConnections();
                selector.select();
                Set<SelectionKey> selected = selector.selectedKeys();
                Iterator<SelectionKey> iter = selected.iterator();
                while (iter.hasNext()) {
                    SelectionKey key = iter.next();
                    iter.remove();
                    if (!key.isValid()) {
                        continue;
                    }
                    Connection c = (Connection) key.attachment();
                    if (key.isReadable()) {
                        try {
                            c.read(listener);
                        } catch (ConnectionException e) {
                            Log.error(this.getClass().getName(),"Connection error occured", e);
                            key.cancel();
                            clientConnections.remove(c);
                            continue;
                        }
                    }
                    if (key.isWritable()) {
                        try {
                            c.write();
                        } catch (MessageSerializationException e) {
                            e.printStackTrace();
                            System.exit(1);
                        } catch (ConnectionException e) {
                            Log.error(this.getClass().getName(),"Connection error occured", e);
                            key.cancel();
                            clientConnections.remove(c);
                            continue;
                        }
                    }
                }
            } catch (IOException e) {
                if (!active) {
                    /* Thread stopped before error */
                    Log.debug(this.getClass().getName(),"Thread already stopped");
                } else {
                    Log.error(this.getClass().getName(),"Selector threw an exception", e);
                }
            } catch (Exception e) {
                Log.debug(this.getClass().getName(),"Other exception Occured", e);
            }
        }
        /* When done executing */
        internalCleanup();
    }

    private void internalCleanup() {
        try {
            selector.close();
        } catch (IOException e) {
            Log.error(this.getClass().getName(),"[ConnectionHandler] Error while closing selector");
        }
        selector = null;
        for (Connection c : clientConnections.keySet()) {
            /* Call the disconnect function of all the */
            c.disconnectInternal();
        }
        pendingChannels.clear();
        clientConnections.clear();
    }

    private void registerNewConnections() {
        if (pendingChannels.isEmpty())
            return;
        ArrayList<SocketChannel> nextChannels;
        synchronized (pendingChannels) {
            nextChannels = new ArrayList<SocketChannel>(pendingChannels);
            pendingChannels.clear();
        }
        for (SocketChannel s : nextChannels) {
            try {
                SelectionKey key = s.register(selector, SelectionKey.OP_READ);
                Connection c = new Connection(this, key);
                Connection oldConnection = clientConnections.put(c, c);
                if (oldConnection != null && oldConnection.checkIsConnected()) {
                    /* This shouldn't ever happen, since we don't reuse connections */
                    oldConnection.disconnect();
                }
                listener.onSocketConnection(c);
            } catch (ClosedChannelException e) {
                Log.error(this.getClass().getName(),"Could not register socket with selector");
            }
        }
    }

    public ConnectionEventListener getConnectionEventListener() {
        return listener;
    }
}
