package za.co.percipio.mpl;

import com.esotericsoftware.minlog.Log;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

import za.co.percipio.mpl.connection.ConnectionHandler;
import za.co.percipio.mpl.listener.ConnectionEventListener;

public class MessageClient implements Runnable {

    private Thread thread;

    private String hostname;

    private int                     port;
    private Selector                selector;
    private SocketChannel           channel;
    private ConnectionEventListener listener;

    private          boolean connectionErrorOccured;
    private volatile boolean isActive;
    private Object initializationLock = new Object();

    private ConnectionHandler handler;

    public MessageClient(String hostname, int port, ConnectionEventListener listener) {
        thread = new Thread(this);
        handler = new ConnectionHandler(listener);
        this.listener = listener;
        this.hostname = hostname;
        this.port = port;
    }

    private void initializeConnection() {
        try {
            selector = Selector.open();
            channel = SocketChannel.open();
            channel.configureBlocking(false);
        } catch (IOException e) {
            Log.error(this.getClass().getName(),"Error occured while connecting", e);
            internalOnErrorOccured();
        }
    }

    ;

    private void connectToServer() {
        boolean connected = false;
        try {
            channel.connect(new InetSocketAddress(hostname, port));
            channel.register(selector, SelectionKey.OP_CONNECT);
            while (!connected) {
                selector.select();
                Set<SelectionKey> selected = selector.selectedKeys();
                Iterator<SelectionKey> iter = selected.iterator();

                while (iter.hasNext()) {
                    SelectionKey key = iter.next();
                    SocketChannel socket = ((SocketChannel) key.channel());
                    iter.remove();
                    if (!key.isValid()) {
                        continue;
                    }
                    if (key.isConnectable()) {
                        if (socket.isConnectionPending()) {
                            socket.finishConnect();
                            connected = true;
                        }
                        break;
                    }
                }
            }
        } catch (IOException e) {
            Log.error(this.getClass().getName(),"Error occured while connecting", e);
            if (!isActive) {
                /* This was disconnected externally, don't send out a notification */
                Log.trace(this.getClass().getName(),"Was disconnected from an external source.");
            } else {
                internalOnErrorOccured();
            }
            return;
        } catch (Exception e) {
            if (!isActive) {
                /* This was disconnected externally, don't send out a notification */
                Log.trace(this.getClass().getName(),"Was disconnected from an external source.", e);
            } else {
                Log.error(this.getClass().getName(),"This should never have happened!", e);
                internalOnErrorOccured();
            }
        }

    }

    @Override public void run() {
        initializeConnection();
        if (connectionErrorOccured) return;
        connectToServer();
        if (connectionErrorOccured) return;
        handler.manageSocketChannel(channel);
        while (isActive) {
            try {
                Thread.sleep(5 * 60000);
            } catch (InterruptedException e) {
            }
        }
        disconnectInternal();
    }

    public void start() {
        if (!isActive) {
            synchronized (initializationLock) {
                if (!isActive) {
                    isActive = true;
                    handler.start();
                    thread.start();
                }
            }
        }
    }

    public void stop() {
        /* Stop the handler and clean up*/
        if (isActive) {
            synchronized (initializationLock) {
                if (isActive) {
                    isActive = false;
                    thread.interrupt();
                }
            }
        }
    }

    private void disconnectInternal() {
        handler.stop();
        if (selector != null) {
            try {
                selector.close();
            } catch (IOException e) {
                Log.debug(this.getClass().getName(),"[MessageClient] Exception while closing selector");
            }
            selector = null;
        }
    }

    private void internalOnErrorOccured() {
        connectionErrorOccured = true;
        listener.onConnectionError(null);
        disconnectInternal();
    }

}
