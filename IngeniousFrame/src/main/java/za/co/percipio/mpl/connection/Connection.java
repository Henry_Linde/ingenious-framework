package za.co.percipio.mpl.connection;

import com.esotericsoftware.minlog.Log;

import java.io.IOException;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import za.co.percipio.mpl.Message;
import za.co.percipio.mpl.codec.JavaDeserializerCodec;
import za.co.percipio.mpl.codec.JavaSerializerCodec;
import za.co.percipio.mpl.exception.ConnectionException;
import za.co.percipio.mpl.exception.MessageDeserializationException;
import za.co.percipio.mpl.exception.MessageSerializationException;
import za.co.percipio.mpl.listener.ConnectionEventListener;

/**
 * Representation of a connection between two hosts
 *
 * @author Chris Coetzee
 *
 *
 */
public class Connection {

    private static final ByteBuffer[] EMPTY_BUFFER_ARRAY = new ByteBuffer[0];
    private ConnectionHandler       parent;
    private ConnectionEventListener currentListener;

    private SocketChannel channel;
    private SelectionKey  key;
    private AtomicBoolean hasSetWriteReady;

    private List<Message>    queuedMessages;
    private List<ByteBuffer> pendingOutputBytes;
    private List<ByteBuffer> pendingInputBytes;

    private ByteBuffer nextObjectSizeBuffer;
    private int        nextObjectSize;
    private boolean    hasReadNextSize;

    private          JavaDeserializerCodec reader;
    private          JavaSerializerCodec   writer;
    private volatile boolean               connected;
    private volatile boolean               hasCleaned;

    /**
     * Create a new Connection using the given handler as parent / manager of this connection and the given
     * SelectionKey to represent the connection.
     *
     * @param parent
     * @param key
     */
    public Connection(ConnectionHandler parent, SelectionKey key) {
        queuedMessages = new LinkedList<Message>();
        hasSetWriteReady = new AtomicBoolean();
        pendingOutputBytes = new LinkedList<ByteBuffer>();
        pendingInputBytes = new LinkedList<ByteBuffer>();
        nextObjectSizeBuffer = ByteBuffer.allocate(4);

        this.parent = parent;
        this.key = key;
        this.channel = (SocketChannel) key.channel();
        currentListener = parent.getConnectionEventListener();

        key.attach(this);
        reader = new JavaDeserializerCodec();
        writer = new JavaSerializerCodec();
        possiblyUpdateConnectionState();
    }

    /**
     * Add a message for sending over ths connection
     *
     * @param m
     *            the message to send
     */
    public void queueMessage(Message m) {
        Log.trace(this.getClass().getName(),"Sending Message: " + m);
        synchronized (queuedMessages) {
            queuedMessages.add(m);
            possiblySetWriteReady();
            possiblyUpdateConnectionState();
        }
    }

    /**
     * Mark the channel as being write ready
     */
    private void possiblySetWriteReady() {
        if (hasSetWriteReady.compareAndSet(false, true)) {
            try {
                key.interestOps(SelectionKey.OP_WRITE | SelectionKey.OP_READ);
            } catch (CancelledKeyException e) {
                Log.debug(this.getClass().getName(),
                        "[Connection] Key has been cancelled while setting `OP_WRITE`, continuing anyway");
            }
            key.selector().wakeup();
        }
    }

    /**
     * Unmark the channel as write ready. This is done to prevent unnecessary waste of CPU cycles when we
     * don't have any data to write.
     */
    private void possiblyUnsetWriteReady() {
        synchronized (queuedMessages) {
            if (pendingOutputBytes.isEmpty() && queuedMessages.isEmpty()
                    && hasSetWriteReady.compareAndSet(true, false)) {
                try {
                    key.interestOps(SelectionKey.OP_READ);
                } catch (CancelledKeyException e) {
                    Log.debug(this.getClass().getName(),
                            "[Connection] Key has been cancelled while setting `OP_READ`, continuing anyway");
                }
                key.selector().wakeup();
            }
        }
    }

    /**
     * Method used by the handler of this connection to perform reading from the channel
     *
     * @param listener
     */
    void read(ConnectionEventListener listener) throws ConnectionException {
        if (!checkIsConnected())
            throw new ConnectionException("Connection already disconnected");

        currentListener = parent.getConnectionEventListener();
        readNewMessages();
        processMessageByteBuffers();
    }

    /**
     * Attempts to read messages from the socketChannel
     */
    private void readNewMessages() throws ConnectionException {
        while (key.isReadable()) {
            ByteBuffer last = null;
            if (!pendingInputBytes.isEmpty()) {
                last = pendingInputBytes.get(pendingInputBytes.size() - 1);
            }
            if (last != null && last.remaining() > 0) {
                /* The buffer still has some data pending */
                try {
                    int readCount = channel.read(last);
                    if (readCount < 0) throw new IOException("Could not read from channel");
                    if (readCount == 0) return;
                } catch (IOException e) {
                    Log.debug(this.getClass().getName(),"Could not read from channel!");
                    onConnectionError();
                    disconnectInternal();
                    throw new ConnectionException(e);
                }
            } else {
                try {
                    readNextObjectSize();
                    if (hasReadNextSize) {
                        /* Great, we know the size of the next object! */
                        ByteBuffer buffer = ByteBuffer.allocate(nextObjectSize);
                        pendingInputBytes.add(buffer);
                        continue;
                    } else
                        /* Couldn't fill up the size, leaving it until later! */
                        return;
                } catch (IOException e) {
                    Log.debug(this.getClass().getName(),"Could not read from channel!");
                    onConnectionError();
                    disconnectInternal();
                    /* TODO Cleanup and return */
                    throw new ConnectionException(e);
                }
            }
        }
    }

    /**
     * Processes the pendingByteBuffers
     */
    private void processMessageByteBuffers() {
        int available = pendingInputBytes.size();

        if (available > 0) {
            int readUntil = available;
            if (pendingInputBytes.get(available - 1).hasRemaining()) {
                /* We still need to fill the last message up, so we don't process it yet */
                readUntil--;
            }
            for (int i = 0; i < readUntil; i++) {
                Message m;
                ByteBuffer buffer = pendingInputBytes.remove(0);
                buffer.rewind();
                try {
                    m = (Message) deserialize(buffer);
                } catch (MessageSerializationException e) {
                    Log.error(this.getClass().getName(),"Read a bad message!");
                    onMessageParsingError(e);
                    continue;
                }
                onHandleNewMessage(m);
            }
            /* Done deserializing */
        }
    }

    private void readNextObjectSize() throws IOException {
        hasReadNextSize = false;
        int readBytes = channel.read(nextObjectSizeBuffer);
        if (readBytes < 0) throw new IOException("Could not read from channel");
        if (!nextObjectSizeBuffer.hasRemaining()) {
            /* We have read the size! */
            nextObjectSizeBuffer.rewind();
            nextObjectSize = nextObjectSizeBuffer.getInt();
            nextObjectSizeBuffer.rewind();
            hasReadNextSize = true;
        }
    }

    /**
     * Method used by the handler of this connection, when data is to be written to the client
     *
     * @throws MessageSerializationException
     * @throws ConnectionException
     */
    void write() throws MessageSerializationException, ConnectionException {
        if (!checkIsConnected())
            throw new ConnectionException("Connection already disconnected");
        currentListener = parent.getConnectionEventListener();
        if (!key.isValid()) {
            /* TODO call the listener callback if we have not already done so! */
            Log.debug(this.getClass().getName(),"Found invalid key:" + key);
            return;
        }
        if (key.isWritable()) {
            while (!pendingOutputBytes.isEmpty() || !queuedMessages.isEmpty()) {
                if (pendingOutputBytes.isEmpty()) {
                    try {
                        serializeMessages();
                    } catch (MessageSerializationException e) {
                        disconnectInternal();
                        throw e;
                    }
                }
                ByteBuffer buf = pendingOutputBytes.get(0);
                try {
                    channel.write(buf);
                    if (buf.remaining() > 0) {
                        /* Could not write anymore */
                        synchronized (queuedMessages) {
                            possiblySetWriteReady();
                        }
                        return;
                    } else {
                        pendingOutputBytes.remove(0);
                    }
                } catch (IOException e) {
                    Log.debug(this.getClass().getName(),"Error while writing a message", e);
                    onConnectionError();
                    disconnectInternal();
                    throw new ConnectionException(e);
                }
            }
            possiblyUnsetWriteReady();
        }
    }

    /**
     * Serializes a message to an object
     *
     * @param m
     *            message object to serialize
     * @return the serialized object
     * @throws IOException
     */
    private ByteBuffer[] serialize(Message m) throws MessageSerializationException {
        try {
            return writer.serializeObject(m);
        } catch (Exception e) {
            Log.debug(this.getClass().getName(),"Could not serialize message.", e);
            throw new MessageSerializationException(m, e);
        }
    }

    private Object deserialize(ByteBuffer b) throws MessageDeserializationException {
        /* This should return null if the serialization failed! */
        try {
            return reader.readObject(b);
        } catch (Exception e) {
            Log.debug(this.getClass().getName(),"Could not deserialize message");
            throw new MessageDeserializationException(e);
        }
    }

    private void serializeMessages() throws MessageSerializationException {
        if (!queuedMessages.isEmpty()) {
            synchronized (queuedMessages) {
                Iterator<Message> messages = queuedMessages.iterator();
                while (messages.hasNext()) {
                    Message m = messages.next();

                    ByteBuffer[] serialized = serialize(m);
                    for (ByteBuffer b : serialized) {
                        pendingOutputBytes.add(b);
                    }
                    messages.remove();
                }
            }
        }
    }

    private void onConnectionError() {
        try {
            currentListener.onConnectionError(this);
        } catch (Exception e) {
            Log.error(this.getClass().getName(),"Client code threw an exception", e);
        }
    }

    private void onHandleNewMessage(Message message) {
        try {
            currentListener.onMessageReceived(this, message);
        } catch (Exception e) {
            Log.error(this.getClass().getName(),"Client code threw an exception", e);
        }
    }

    private void onMessageParsingError(Throwable e) {
        try {
            currentListener.onIncomingMessageParsingError(this, e);
        } catch (Exception ex) {
            Log.error(this.getClass().getName(),"Client code threw an exception", ex);
        }
    }

    public boolean isConnectionValid() {
        return key.isValid();
    }

    private void possiblyUpdateConnectionState() {
        if (!channel.isOpen()) {
            disconnect();
            return;
        } else if (!key.isValid()) {
            disconnect();
            return;
        }
        connected = true;
    }

    /**
     * Disconnects this connection ensures that all internal structures have been cleared and that the
     * connection has been closed.
     *
     * <br/>
     * <b>*NOTE*</b> This should only be invoked from the handler thread.
     */
    void disconnectInternal() {
        disconnect();
        clean();
    }

    public boolean checkIsConnected() {
        possiblyUpdateConnectionState();
        return connected;
    }

    public void disconnect() {
        if (connected) {
            connected = false;
            synchronized (queuedMessages) {
                /* Wake the selector up for writing, so it can check up on this connection and realize that it was closed */
                possiblySetWriteReady();
            }
            /* We call this regardless of whether the socket was closed internally or from the outside */
            currentListener.onSocketDisconnection(this);
        }
    }

    public InetAddress address() {
        return channel.socket().getInetAddress();
    }

    private void clean() {
        if (!hasCleaned) {
            hasCleaned = true;
            pendingInputBytes.clear();
            pendingOutputBytes.clear();
            nextObjectSize = -1;
            nextObjectSizeBuffer = null;
            hasReadNextSize = false;
            parent = null;
        }
        // We don't null current connection listener
        // currentConnectionListener
        // because it may still be used to notify the caller of the error
    }

    public boolean isSameHostAs(Connection c) {
        if (c == null)
            return false;
        return (address().equals(c.address()));
    }
}
