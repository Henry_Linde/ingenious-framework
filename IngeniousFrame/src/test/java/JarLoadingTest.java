import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import za.ac.sun.cs.ingenious.core.exception.InvalidJarException;
import za.ac.sun.cs.ingenious.core.util.JarLoader;

/**
 * Created by Chris Coetzee on 2016/07/26.
 */
public class JarLoadingTest {

    public static JarLoader getTestJarLoader() {
        JarLoader loader = new JarLoader();
        try {
            loader.addJarToSearchPath("src/test/java/classloadertest.jar");
        } catch (InvalidJarException e) {
            e.printStackTrace();
            assert false;
        }
        return loader;
    }

    @Test public void testFindSubClassesSerializable() {
        List<String> expectedClassnames = Arrays.asList("com.foo.bar.Andromeda");
        JarLoader loader = getTestJarLoader();
        List<String> classnames = loader.findSubClasses(Serializable.class);
        assertEqualsUnordered(expectedClassnames, classnames);

    }

    @Test public void testFindSubClassesThread() {
        List<String> expectedClassnames = Arrays.asList("com.foo.bar.baz.CrabNebula");
        JarLoader loader = getTestJarLoader();
        List<String> classnames = loader.findSubClasses(Thread.class);
        assertEqualsUnordered(expectedClassnames, classnames);
    }

    @Test public void testFindSubClassesRunnable() {
        List<String> expectedClassnames = Arrays.asList("com.foo.bar.baz.CrabNebula",
                                                        "com.foo.bar.baz.MilkyWay");
        JarLoader loader = getTestJarLoader();
        List<String> classnames = loader.findSubClasses(Runnable.class);
        assertEqualsUnordered(expectedClassnames, classnames);
    }

    public static void assertEqualsUnordered(List<?> a, List<?> b) {
        assertEquals(new HashSet<>(a), new HashSet<>(b));
    }
}
