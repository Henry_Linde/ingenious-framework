import java.util.List;

import za.ac.sun.cs.ingenious.core.model.Move;
import za.ac.sun.cs.ingenious.core.model.TurnBasedSquareBoard;
import za.ac.sun.cs.ingenious.core.model.XYAction;
import za.ac.sun.cs.ingenious.games.shadowtictactoe.STTTGameState;
import za.ac.sun.cs.ingenious.games.shadowtictactoe.STTTLogic;

public class STTTDeterminizer {
	
	private static void printboard(byte[] b) {
		System.out.println(b[0] + " " + b[1] + " " + b[2]);
		System.out.println(b[3] + " " + b[4] + " " + b[5]);
		System.out.println(b[6] + " " + b[7] + " " + b[8]);
		System.out.println();
	}

	public static void main(String[] args) {
		int player = 0;
		STTTGameState s = new STTTGameState();
		TurnBasedSquareBoard b = s.playerBoards[player];
		b.board[1]=1;
		b.board[4]=2;
		s.printPretty();
		List<Move> bla = s.moveHistories.get(player);
		bla.add(new XYAction(1,0,0));
		bla.add(new XYAction(1,1,1));
		STTTLogic d = new STTTLogic();
		for (int i =0;i<100; i++) {
			d.determinizeUnknownInformation(player, s).printPretty();
		}
	}

}
