#!/bin/bash
# Here we start two clients bob and alice
# Note that you can exclude the hostname and port if you're connecting to localhost.
# Also note that hostname can be an IP address as in the case of the second client
java -jar ../build/libs/IngeniousFrame-all-0.0.2.jar client -username "alice" -engine "za.ac.sun.cs.ingenious.games.bomberman.engines.BMEngineBFS" -game "bomberman" -hostname 127.0.0.1 -port 61234 > alice.log &
java -jar ../build/libs/IngeniousFrame-all-0.0.2.jar client -username "bob" -engine "za.ac.sun.cs.ingenious.games.bomberman.engines.BMEngineRandom" -game "bomberman" -hostname localhost -port 61234 > bob.log &
java -jar ../build/libs/IngeniousFrame-all-0.0.2.jar client -username "carol" -engine "za.ac.sun.cs.ingenious.games.bomberman.engines.BMEngineMCTS" -game "bomberman" -hostname 127.0.0.1 -port 61234 > carol.log &
java -jar ../build/libs/IngeniousFrame-all-0.0.2.jar client -username "dave" -engine "za.ac.sun.cs.ingenious.games.bomberman.engines.BMEngineRandom" -game "bomberman" -hostname 127.0.0.1 -port 61234 > dave.log &

