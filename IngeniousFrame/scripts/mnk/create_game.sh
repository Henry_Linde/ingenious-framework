#!/bin/bash

# creates a new tictactoe game with the default configuration
java -jar ../../build/libs/IngeniousFrame-all-0.0.2.jar create -config "MNK.json" -game "mnk" -lobby "mylobby" -players 2
