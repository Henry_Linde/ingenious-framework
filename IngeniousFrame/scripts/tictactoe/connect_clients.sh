#!/bin/bash
# Here we start two clients bob and alice
# Note that you can exclude the hostname and port if you're connecting to localhost.
# Also note that hostname can be an IP address as in the case of the second client
java -jar ../../build/libs/IngeniousFrame-all-0.0.2.jar client -username "bob" -engine "za.ac.sun.cs.ingenious.games.tictactoe.engines.TTTMCTSEngine" -game "tictactoe" -hostname localhost -port 61234 &
java -jar ../../build/libs/IngeniousFrame-all-0.0.2.jar client -username "alice" -engine "za.ac.sun.cs.ingenious.games.tictactoe.engines.TTTRandomEngine" -game "tictactoe" -hostname 127.0.0.1 -port 61234 &
