#!/bin/bash

# creates a new tictactoe game with the default configuration
java -jar ../../build/libs/IngeniousFrame-all-0.0.2.jar create -config "LOA.json" -game "LOA" -lobby "mylobby" -players 2
