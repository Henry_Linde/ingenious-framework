#!/bin/bash

# creates a new bomberman game with the default configuration
java -jar ../build/libs/IngeniousFrame-all-0.0.2.jar create -config "BMDefault.json" -game "bomberman" -lobby "mylobby" -players 4
